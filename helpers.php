<?php

/**
 * Template Include
 */
if (!function_exists('tpf_get_template_part')) {
    function tpf_get_template_part($filepath = null, $args = [])
    {
        if ($filepath == null) {
            return;
        }

        $filepath = $filepath . '.php';

        if (file_exists(plugin_dir_path(__FILE__) . $filepath)) {
            ob_start();
            $template = include plugin_dir_path(__FILE__) . $filepath;
            do_action('after_' . $filepath);
            $data = ob_get_clean();
            echo $data;
        }
    }
}
