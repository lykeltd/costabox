var stock_flutes = [];

let details = "";
let emidcheck = true;
let onprogress = true;
let custom_box = "";
let jqxhr_calculate_quote;

jQuery(document).ready(function () {
    var price_break_value = "";
    var available_box_types = "";
    var available_box_i = 0; // Keep track of the number of requests to get available box types, so that we only process the latest
    var use_stock = 0;

    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });


    if(!(document.getElementById("die_cut_chop").value > 0)) {
        jQuery(".die_cut").slideUp();
    }

    if (params.qid > 0 || params.readd > 0) {
        setTimeout(function () {
            jQuery([document.documentElement, document.body]).animate({
                scrollTop: jQuery("#add_to_cart").offset().top
            }, 2000);


        }, 1500)
    }

    jQuery(document).keypress(
        function (event) {
            if (event.which == '13') {
                event.preventDefault();
            }
        });

    jQuery("select.has_images").imagepicker({
        show_label: true,
        selected: box_type_selected,
    });

    jQuery(".qbcb_measurements input").on("click", function (e) {
        if (jQuery("#box_type").val() == 0) return;

        if (!confirm("Changing this measurement will change the box types available. Are you sure this is what you want to do?")) {
            jQuery("#qty").focus();
        }
    });

    var timer = null;
    jQuery(".qbcb_measurements input").on('keyup', function () {

        var width = jQuery("#box_width").val();
        var length = jQuery("#box_length").val();
        var height = jQuery("#box_height").val();

        jQuery.post("/wp-content/plugins/costabox/fast_api.php", {
            "qbcb_request": "boxes_by_size",
            "width": width,
            "length": length,
            "height": height,
            "use_stock": 1
        }, function (resp) {

            console.log('Ajax Post -> Boxes By Size');
            console.log(resp);

            if (resp.length > 0) {
                jQuery("#stock_message").show();
            } else {
                jQuery("#stock_message").hide();
                use_stock = 0;
            }

            get_available_box_types();
        });
    });

    jQuery(".qbcb_measurements input#box_length").on('keyup', function () {
        jQuery("#box_width").attr("max", jQuery(this).val());
    });

    jQuery("#box_width,#box_height").on('focusout', function () {
        var length = jQuery("#box_length").val();
        var width = jQuery("#box_width").val();

        if (!(jQuery("#box_height").val() > 0)) return;

        if (parseInt(width) > parseInt(length)) {
            jQuery("#box_width").val(length);
            jQuery("#box_width").attr("max", length);
            jQuery("#box_length").val(width);
            generate_quote();
        }
    });

    //jQuery('#box_type').on('change', get_dimension_restrictions);
    get_dimension_restrictions();

    jQuery('#box_colour').on('change', filter_board_colour);

    setTimeout(function () {
        filter_board_colour();
        if (typeof jQuery("input[name=quote_id]").val() == "undefined") hide_all_panels();
    }, 2);

    filter_board_colour();

    jQuery('#box_flute').on('change', change_board_grade);
    change_board_grade();


    jQuery("input").on(' keyup', function () {

        if (jQuery(this)[0]['id'] == "quote_ref" || jQuery(this)[0]['id'] == "custom_quote_email") {
            console.log("form will not reload");
        } else {
            generate_quote();
        }

    });
    jQuery("select").on('change', generate_quote);
    generate_quote();
    jQuery("input[type=checkbox]").on('change', generate_quote);
    jQuery("#box_production").on("change", function () {
        if (jQuery(this).val() == "supplier") {
            jQuery("#box_supplier_breaks, #box_supplier_details").slideDown();
            jQuery("#box_printing").hide();
        } else {
            jQuery("#box_supplier_breaks, #box_supplier_details").hide();
            jQuery("#box_printing").show();
        }
    });

    jQuery("#supplier_finish").on("change", function () {
        if (jQuery(this).val() > 0) {
            jQuery("#artwork_upload").slideDown();
            jQuery("#box_printing").val("printed");
        } else {
            jQuery("#box_printing").val("plain");
            jQuery("#artwork_upload").hide();
        }
    });

    jQuery("#box_printing").on("change", function () {
        if (jQuery(this).val() == "printed") {
            jQuery("#artwork_upload").slideDown();
        } else {
            jQuery("#artwork_upload").hide();
        }
    })

    jQuery(".qbcb_radio_button").on('click', function () {
        jQuery(".qbcb_radio_button").removeClass("selected");
        jQuery(this).addClass("selected");
        if (jQuery(this).text() == "Yes")
            use_stock = 1;
        else
            use_stock = 0;

        get_available_box_types();
        toggle_stock_flutes();
        generate_quote();
    });

    jQuery("#qty").on("keyup", function () {
        jQuery("#price_panel").hide();
        jQuery("#qty-spinner").attr('hidden', false);
    });

    function toggle_stock_flutes() {
        if (use_stock) {
            jQuery.post("/wp-content/plugins/costabox/fast_api.php", {"qbcb_request": "stock_flutes"}, function (resp) {

                console.log('Ajax Post -> Toggle Stock flutes Function');
                console.log(resp);

                stock_flutes = resp.split(",");
                filter_board_colour(stock_flutes);

            });
        } else {
            stock_flutes = [];
            filter_board_colour(stock_flutes);
        }
    }

    function hide_all_panels() {
        //jQuery('.box_type_wrapper, .box_color_wrapper, .box_thickness_wrapper, .box_qty_wrapper, .box_additional_costs, .box_production_method').hide();
    }

    function get_available_box_types() {

        var width = jQuery("#box_width").val();
        var length = jQuery("#box_length").val();
        var height = jQuery("#box_height").val();
        available_box_i += 1;

        var this_box_i = available_box_i;

        jQuery.post("/wp-content/plugins/costabox/fast_api.php", {
            "qbcb_request": "boxes_by_size",
            "width": width,
            "length": length,
            "height": height,
            "use_stock": use_stock
        }, function (resp) {

            console.log('Ajax Post -> Get Available Box Function');
            console.log(resp);

            var allowed_boxes = resp.split(",");

            available_box_types = allowed_boxes;

            set_allowed_box_types(this_box_i);

            jQuery("#box_type").val(0);
            jQuery("#qty").val(0);
            jQuery("#price_panel, #add_to_cart").hide();
            jQuery("#price_break_table").html("");

        });

    }

    function set_allowed_box_types(i) {

        var opt_no = -1;
        var available_box = false;

        // Because AJAX is asynchronous, we need to make sure we only process the latest data

        if (i < available_box_i) return;

        jQuery("#box_type > option").each(function () {
            opt_no = opt_no + 1;

            if (jQuery(this).val() != 999 && jQuery.inArray(jQuery(this).val(), available_box_types) === -1) {
                jQuery("#box_type").next("ul").find("li:nth-child(" + opt_no + ")").hide();
                jQuery("#box_type").next("ul").find("li:nth-child(" + opt_no + ") div").removeClass("selected");
            } else {
                jQuery("#box_type").next("ul").find("li:nth-child(" + opt_no + ")").show();
                available_box = true;
            }
        });

        if (available_box > 0) {
            jQuery('.box_type_wrapper').slideDown();
            jQuery(".no_box_type_wrapper").hide();

            if (!jQuery("#box_type").next("ul").find("div.selected").length) {
                jQuery(' #price_panel, #add_to_cart, #save_box').hide();
            }
        } else {
            hide_all_panels();
            jQuery(".no_box_type_wrapper").show();
            jQuery('.box_type_wrapper, #price_panel, #add_to_cart, #save_box').hide();
            generate_quote();
        }

    }

    function box_type_selected() {
        var select_name = (jQuery(this).attr("id"));

        if (select_name == "box_type") {
            jQuery('.box_color_wrapper, .box_thickness_wrapper, .box_qty_wrapper, .box_additional_costs, .box_production_method').slideDown();
        }

        if (jQuery(this).val() == 999) {
            jQuery('#box_production option:first').attr('disabled', "disabled");
            jQuery("#box_production option:last").attr("selected", "selected");
            jQuery("#box_supplier_breaks, #box_supplier_details").slideDown();
            jQuery("#bespoke_desc").show();
        } else {
            jQuery('#box_production option:first').removeAttr('disabled');
            jQuery("#bespoke_desc").hide();
        }

        var option_text = (jQuery("#box_type option[value='" + jQuery(this).val() + "']").text());

        if (option_text.includes("DIE-CUT")) {
            jQuery(".die_cut").slideDown();
        } else {
            jQuery(".die_cut").slideUp();
        }
    }

    function get_dimension_restrictions() {

        var box_type = jQuery("#box_type").val();

        jQuery.post("/wp-content/plugins/costabox/fast_api.php", {
            "qbcb_request": "dimension_request",
            "box_type": box_type,
            "dimension": "length"
        }, function (resp) {

            console.log('Ajax Post -> Get Dimension Restrictions: "Length" Function');
            console.log(resp);

            if (resp == '500') {
                set_dimension_restrictions('length', 0, null);
                return;
            }

            var data = jQuery.parseJSON(resp);
            set_dimension_restrictions('length', data['min'], data['max']);

        });

        jQuery.post("/wp-content/plugins/costabox/fast_api.php", {
            "qbcb_request": "dimension_request",
            "box_type": box_type,
            "dimension": "breadth"
        }, function (resp) {

            console.log('Ajax Post -> Get Dimension Restrictions: "Breadth" Function');
            console.log(resp);

            if (resp == '500') {
                set_dimension_restrictions('width', 0, null);
                return;
            }

            var data = jQuery.parseJSON(resp);
            set_dimension_restrictions('width', data['min'], data['max']);

        });

        jQuery.post("/wp-content/plugins/costabox/fast_api.php", {
            "qbcb_request": "dimension_request",
            "box_type": box_type,
            "dimension": "height"
        }, function (resp) {

            console.log('Ajax Post -> Get Dimension Restrictions: "Height" Function');
            console.log(resp);

            if (resp == '500') {
                set_dimension_restrictions('height', 0, null);
                return;
            }

            var data = jQuery.parseJSON(resp);
            set_dimension_restrictions('height', data['min'], data['max']);

        });

    }

    function set_dimension_restrictions(dimension, min, max) {

        if (max == 0) {
            jQuery('div.box_' + dimension + ' input').val(0);
            jQuery('div.box_' + dimension).hide();
        } else {
            jQuery('div.box_' + dimension).show();
            if (max != null) {
                jQuery('div.box_' + dimension + ' input').prop('max', max);
            } else {
                jQuery('div.box_' + dimension + ' input').prop('max', '');
            }
        }

        if (min == null) {
            jQuery('div.box_' + dimension + ' input').prop('min', '');
        } else if (min == 0 || (max != null && min > max)) {
            jQuery('div.box_' + dimension + ' input').prop('min', '0');
        } else {
            jQuery('div.box_' + dimension + ' input').prop('min', min);
        }

        jQuery('div.qbcb_measurements div').removeClass('first');
        jQuery('div.qbcb_measurements div:visible').first().addClass('first');

    }

    function generate_quote() {

        var box_code = jQuery('#box_type option:selected').data('box-code');

        console.log('Box Code: ' + box_code);

        var box_type = jQuery("#box_type").val();
        var box_colour = jQuery("#box_colour").val();
        var box_flute = jQuery("#box_flute").val();
        var box_grade = jQuery("#board_grade").val();
        var box_height = jQuery("#box_height").val();
        var box_length = jQuery("#box_length").val();
        var box_width = jQuery("#box_width").val();
        var user_id = jQuery("#this_user").val();
        var supplier_prices = get_supplier_break_points();
        var qty = jQuery("#qty").val();

        var selected_user = jQuery("#user").val();

        var box_printing = jQuery("#box_printing").val();
        var handhole_box = jQuery("#handhole_box").is(":checked") ? "true" : "false";

        let user = document.getElementById('user') ? document.getElementById('user').value : null
        if(typeof user === 'undefined' || user === null || user === ''){
            user = document.getElementById('this_user').value;
        }

        let formData = new FormData()
        formData.append('box_type', document.getElementById('box_type').value)
        formData.append('flute', document.getElementById('box_flute').value)
        formData.append('grade', document.getElementById('board_grade').value)
        formData.append('height', document.getElementById('box_height').value)
        formData.append('length', document.getElementById('box_length').value)
        formData.append('width', document.getElementById('box_width').value)
        formData.append('user', user)
        formData.append('stock_prices', use_stock)
        formData.append('qty', document.getElementById('qty').value)

        if(document.getElementById('box_printing')) {
            formData.append('box_printing', document.getElementById('box_printing').value)
            formData.append('handhole_box', document.getElementById('handhole_box').checked)
        }

        if(buying_boxes()) {
            formData.append('supplier_prices', get_supplier_break_points())
            formData.append('supplier_name', document.getElementById('supplier_name').value)
            formData.append('supplier_ref', document.getElementById('supplier_ref').value)
            formData.append('supplier_document', document.getElementById('supplier_document').files[0])
        }

        if(box_code == 'DC'){
            formData.append('chop', document.getElementById('die_cut_chop').value)
            formData.append('deckle', document.getElementById('die_cut_deckle').value)
            formData.append('qty_up', document.getElementById('die_cut_up').value)
            formData.append('description', document.getElementById('die_cut_code').value)
            formData.append('die_cut_artwork', document.getElementById('die_cut_attach').files[0])
        }

        if(document.getElementById("artwork") && document.getElementById('artwork').value.length > 0) {
            formData.append('artwork', document.getElementById('artwork').files[0])
        }


        if (typeof selected_user != "undefined") user_id = selected_user;

        if (jQuery(".no_box_type_wrapper").is(":visible") || box_length == 0 || box_width == 0 || qty < 1) {
            jQuery("#price_panel, #add_to_cart, #save_box").hide();
            jQuery("#price_break_table").html("");
            return;
        }

        if (!buying_boxes()) supplier_prices = "";

        if (typeof box_type == "undefined" || box_type == 0 || typeof box_colour == "undefined" || typeof box_flute == "undefined" || typeof box_grade == "undefined" || typeof box_height == "undefined" || typeof box_length == "undefined" || typeof box_width == "undefined" || qty.length == 0) {
            jQuery("#price_panel, #add_to_cart, #save_box").hide();
            return;
        }

        let data = {
            "box_type": box_type,
            "flute": box_flute,
            "grade": box_grade,
            "qty": qty,
            "length": box_length,
            "width": box_width,
            "height": box_height,
            "user": user_id,
            "supplier_prices": supplier_prices,
            "stock_prices": use_stock,
            "box_printing": box_printing,
            "handhole_box": handhole_box,
        };

        if(document.getElementById('supplier_name')) {
            data.supplier_name = document.getElementById('supplier_name').value
            data.supplier_ref = document.getElementById('supplier_ref').value
            data.supplier_document = document.getElementById('supplier_document').files[0]
        }

        if (jQuery(".die_cut").first().is(":visible") || box_code == 'DC') {

            data.chop = jQuery("#die_cut_chop").val();
            data.deckle = jQuery("#die_cut_deckle").val();
            data.qty_up = jQuery("#die_cut_up").val();

        }


        jqxhr_calculate_quote = jQuery.ajax({
            type: 'POST',
            url: '/wp-json/costabox/calculate_quote',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                jQuery("#price_panel").hide();
                jQuery("#qty-spinner").attr('hidden', false);

                if (jqxhr_calculate_quote != null) jqxhr_calculate_quote.abort();

                jQuery('.costabox').addClass('processing').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                });

            },
            success: function (resp) {
                if (box_printing == 'plain') jQuery(".printing_cost_display").hide();
                if (box_printing == 'printed') jQuery(".printing_cost_display").show();
                if (handhole_box == "true") jQuery(".handhole_cost_display").show();
                if (handhole_box == "false") jQuery(".handhole_cost_display").hide();

                details = resp;

                console.log(resp);

                custom_box = details.object;
                document.getElementById('box_object').value = JSON.stringify(details.object);

                jQuery("#ppb").text('£' + floorFigure(details['unit'], 2));
                jQuery("#total_price").text("£" + details['total']);
                jQuery("#quote_price").val(details['total']);
                jQuery("#total_weight").val(details['weight']);
                jQuery("#dispatch_date").text(details['dispatch']);
                jQuery("#stock_board").val(use_stock);
                price_break_qty_total = details['price_breaks'];
                price_break_value = details['unit_price_container'];
                jQuery("#price_break_table").html(details['price_breaks_html']);
                //jQuery("#price_break_html").val(details['price_breaks']);
                jQuery("#price_break_html").val(details['price_break_updated']);
                jQuery("#qty-spinner").attr('hidden', true);
                jQuery("#price_panel").show();

                jQuery("#cpc").text('£' + details['printing_cost_total']);
                jQuery("#chc").text('£' + details['handhole_cost']);

                for (var d in details) {

                    jQuery("#diagnostics ." + d + " strong").text(details[d]);

                }

                show_add_to_cart();

                if (details['moq'] > 0) {
                    jQuery("#moq_notice").show();
                    jQuery("#box_moq").text(details['moq']);
                    if (parseInt(details['moq']) > qty && !(qty == 1 && typeof jQuery("#user").val() != "undefined"))
                        jQuery("#price_panel, #add_to_cart, #save_box").hide();
                    else
                        jQuery("#price_panel, #add_to_cart, #save_box").show();
                } else {
                    jQuery("#moq_notice").hide();
                }

                jQuery("#price_panel").show();
                jQuery("#qty-spinner").attr('hidden', true);
                jQuery('form.costabox ').removeClass('processing').unblock();
            },
            error: function () {
                console.log('Failed Generating Quote!');
                jQuery("#moq_notice").hide();
                jQuery("#price_panel").hide();
                jQuery("#qty-spinner").attr('hidden', true);
                jQuery("#qty-spinner").attr('hidden', true);

                jQuery('form.costabox ').removeClass('processing').unblock();
            },
            statusCode: {
                404: function () {
                    alert("Error Request!");
                }
            }
        }).always(function () {
            //alert( "complete" );
        });

        jqxhr_calculate_quote.done(function (e) {

            if (e['status'] !== undefined && e['status'] === 0) return;
            // console.log(jQuery('div#price_break_table').html());
            if (jQuery('div#price_break_table').html() == '') return;
            var total_price_display = parseFloat((jQuery('strong#total_price').html()).replace(/[^\d\.\,\s]|,+/g, ''));


            var unit = (details['unit']).replace(/,/g, '');
            checker_total = parseFloat(parseFloat(unit) * parseFloat(qty));
            console.log(total_price_display);
            console.log(checker_total);
            if (!approxeq(parseFloat(total_price_display), parseFloat(checker_total))) {
                // console.log("Not same");

                // if(jqxhr_calculate_quote.readyState > 3)generate_quote();


            }
        });
        jqxhr_calculate_quote.always(function (e) {
            // alert( "second complete" );


            if (e['status'] !== undefined && e['status'] === 0) return;
            var obj = jQuery.parseJSON(e);

            if (typeof obj == 'object') {
                // It is JSON
                console.log("yes");
                // console.log(e);
                // if(price_breaks_html['price_breaks_html'].jQuery('div#price_break_table').html() == '')return;
                var total_price_display = parseFloat((obj['total']).replace(/[^\d\.\,\s]|,+/g, ''));


                var unit = (obj['unit']).replace(/[^\d\.\,\s]|,+/g, '');
                checker_total = parseFloat(parseFloat(unit) * parseFloat(qty));
                /* console.log(total_price_display);
                console.log(checker_total); */
                if (!approxeq(parseFloat(total_price_display), parseFloat(checker_total))) {
                    // console.log("Not same always");
                    // console.log(jqxhr_calculate_quote.readyState);
                    // console.log("Not same always");
                    // if(jqxhr_calculate_quote.readyState > 3)generate_quote();


                }

            }


        });

    }

    function add_to_cart() {

        let postUrl = '/wp-json/costabox/add_to_cart'
        let data = {
            'box': custom_box,
            'reference': document.getElementById("quote_ref").value,
        }

        if(document.getElementById("forme_cost"))
            data.forme_cost = document.getElementById("forme_cost").value
        if(document.getElementById("forme_code"))
            data.forme_code = document.getElementById("forme_code").value

        if(document.getElementById("plate_cost"))
            data.plate_cost = document.getElementById("plate_cost").value
        if(document.getElementById("plate_code"))
            data.plate_code = document.getElementById("plate_code").value


        fetch(postUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function (resp) {
            window.location.href = resp.redirect_url;
        })

    }

    // document.getElementById('add_to_cart').addEventListener('click', add_to_cart);

    function save_box() {

        let postUrl = '/wp-json/costabox/save_box'
        let data = {
            'box': custom_box,
        }

        fetch(postUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function (resp) {
          alert("Box Saved Successfully");
        })

    }

    document.getElementById('save_box').addEventListener('click', save_box);


    // Monitor calculation update


    cosheartbeatprice();

    function cosheartbeatprice() {
        // console.log(jqxhr_calculate_quote);
        setTimeout(function () {
            // console.log('Hello My Infinite Loop Execution');

            // Again
            cosheartbeatprice();

            // Every 3 sec
        }, 10000);

        if (jQuery('div#price_break_table').html() == '' &&
            jqxhr_calculate_quote == undefined) return;

        total_price_display = parseFloat((jQuery('strong#total_price').html()).replace(/[^\d\.\,\s]|,+/g, ''));

        if (jqxhr_calculate_quote.readyState > 3 && details['unit'] !== undefined) {
            var unit = (details['unit']).replace(/,/g, '');
            checker_total = parseFloat(parseFloat(unit) * parseFloat(jQuery('input#qty').val()));
            // console.log(total_price_display);
            // console.log(checker_total);
            if (!approxeq(parseFloat(total_price_display), parseFloat(checker_total)) || emidcheck) {
                // console.log("Not same");
                emidcheck = false;

                // if(jqxhr_calculate_quote.readyState > 3)generate_quote();


            }
        }

    };
    jQuery('input').change(function () {
        emidcheck = true;
        eachmovemonitor = 0;
    })

    approxeq = function (v1, v2, epsilon) {
        if (epsilon == null) {
            epsilon = 1.50;
        }
        return Math.abs(v1 - v2) < epsilon;
    };

    // Custom Modal Toggle

    jQuery('#send_quote').on('click', function (e) {
        jQuery('#myModal').css('display', 'block');

    });

    jQuery('button.close').on('click', function (e) {

        jQuery('#myModal').css('display', 'none');

    });

    // jQuery('.modal').on('click', function(e){

    // 	jQuery(this).css('display', 'none');

    // });

    window.onclick = function (event) {

        if (event.target == document.getElementById("myModal")) {

            document.getElementById("myModal").style.display = "none";

        }

    }

    jQuery('#send_quote_custom').on('click', function (e) {

        let form_data = new FormData();
        let quote_price_total = parseFloat(jQuery('#quote_price').val().replace(/\,/g, '')).toFixed(2);

        // form_data.append('quote_price', jQuery('#quote_price').val());
        form_data.append('quote_price', quote_price_total);
        form_data.append('box_code', jQuery('#box_type option:selected').data('box-code'));
        form_data.append('box_qty', jQuery('#qty').val());
        form_data.append('box_length', jQuery('#box_length').val());
        form_data.append('box_width', jQuery('#box_width').val());
        form_data.append('box_height', jQuery('#box_height').val());
        form_data.append('box_colour', jQuery('#box_colour').val());
        form_data.append('board_grade', jQuery('#board_grade').val());
        form_data.append('box_flute', jQuery('#box_flute').val());
        form_data.append('email', jQuery('#custom_quote_email').val());
        form_data.append('additional_text', jQuery('#additional-text').val() != undefined ? jQuery('#additional-text').val() : 'No Additional Message');
        form_data.append('price_per_box', jQuery('#ppb').text().substring(1));
        form_data.append('price_break', price_break_qty_total);
        form_data.append('price_break_unit_price', price_break_value);
        form_data.append('action', 'custom_send_quote');

        jQuery.ajax({

            type: 'POST',
            url: ajax_object.ajax_url,
            data: form_data,
            contentType: false,
            processData: false,
            beforeSend: function () {

                jQuery('#custom-send-spinner').removeClass('uk-hidden');

            },
            success: function (response) {

                console.log(response);
                jQuery('#myModal').css('display', 'none');
                jQuery('#custom-send-spinner').addClass('uk-hidden');
                jQuery('#alert-success-quote').removeClass('uk-hidden');

            },
            error: function (err) {

                console.log(err);
                jQuery('#custom-send-alert').removeClass('uk-hidden')

            }

        })

    });

});

function floorFigure(figure, decimals) {
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};

jQuery.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

function maxLengthCheck(object) {
    console.log(object);
    if (!jQuery(object).hasAttr('maxlength')) return;
    if (jQuery(object).val() > jQuery(object).attr('maxlength'))
        object.value = object.value.slice(0, object.maxLength)
}

function show_add_to_cart() {

    var selected_user = jQuery("#user").val();

    if (buying_boxes()) {

        jQuery("#save_box").hide();

        if (jQuery("#supplier_document").val().length == 0 || jQuery("#supplier_name").val().length == 0 || jQuery("#supplier_ref").val().length == 0) {
            jQuery("#add_to_cart").hide();
            return;
        }

    }

    if (typeof selected_user != "undefined" && selected_user != jQuery("#this_user").val()) {
        jQuery("#add_to_cart, #save_box").hide();
        return;
    }

    jQuery("#add_to_cart, #save_box").show();

}

function filter_board_colour(allowed_ids = []) {

    allowed_ids = stock_flutes;

    var white_board = false;
    if (allowed_ids.length == 0)
        white_board = true;

    // Get board colour selected
    var colour = jQuery("#box_colour").val();

    var opt_no = 0;


    jQuery("#box_flute > option").each(function () {
        opt_color = jQuery(this).attr("data-color");
        opt_no = opt_no + 1

        if (opt_color == 'undefined') opt_color = 'brown';

        if (opt_color.toLowerCase() == 'white') {
            if ((allowed_ids.length > 0 && jQuery.inArray(jQuery(this).val(), allowed_ids) >= 0)) {
                white_board = true;
            }
        }

        if ((allowed_ids.length > 0 && jQuery.inArray(jQuery(this).val(), allowed_ids) == -1) | opt_color.toLowerCase() != colour.toLowerCase()) {
            jQuery(this).attr("disabled", "disabled");
        } else {
            jQuery(this).removeAttr("disabled");
        }

    });

    if (!white_board) {
        jQuery("#box_colour option[value='white']").removeAttr("selected");
        jQuery("#box_colour option[value='brown']").attr("selected", "selected");
        jQuery("#box_colour").imagepicker({
            show_label: true,
        });

        colour = 'brown'; // Make sure on brown flutes are displayed

        jQuery("#box_flute > option").each(function () {
            opt_color = jQuery(this).attr("data-color");
            opt_no = opt_no + 1

            if (opt_color == 'undefined') opt_color = 'brown';

            if ((allowed_ids.length > 0 && jQuery.inArray(jQuery(this).val(), allowed_ids) == -1) | opt_color.toLowerCase() != colour.toLowerCase()) {
                jQuery(this).attr("disabled", "disabled");
            } else {
                jQuery(this).removeAttr("disabled");
            }

        });

        jQuery("#box_colour").next('ul').find("li:nth-child(2)").hide();
    } else {
        jQuery("#box_colour").next('ul').find("li:nth-child(2)").show();
    }

    if (jQuery("#box_flute > option:selected").attr("disabled") == "disabled") {
        jQuery("#box_flute > option:selected").removeAttr("selected");
        jQuery("#box_flute > option:not([disabled])").first().attr("selected", "selected");
    }

    if (jQuery("#box_flute").hasClass("has_images")) {
        jQuery("#box_flute").imagepicker({
            show_label: true,
        });

        opt_no = 0;

        jQuery("#box_flute > option").each(function () {
            opt_color = jQuery(this).attr("data-color");
            opt_no = opt_no + 1

            if (opt_color == 'undefined') opt_color = 'brown';

            if (opt_color.toLowerCase() != colour.toLowerCase() || jQuery(this).is(":disabled")) {
                jQuery("#box_flute").next("ul").find("li:nth-child(" + opt_no + ")").hide();
            } else {
                jQuery("#box_flute").next("ul").find("li:nth-child(" + opt_no + ")").show();
            }

        });
    }

    show_board_description();
    change_board_grade();

}

function change_board_grade() {

    if (!jQuery("#box_flute").hasClass("has_images")) return;

    var selected_flute = (jQuery("#box_flute option:selected").text())

    if (selected_flute.startsWith("Heavy"))
        jQuery("#board_grade").val(3);
    else if (selected_flute.startsWith("Extra"))
        jQuery("#board_grade").val(4);
    else
        jQuery("#board_grade").val(1);

}


function show_board_description() {

    var bfo = 0;
    jQuery("#box_flute option").each(function () {
        bfo += 1;

        jQuery("#box_flute + ul.thumbnails li:nth-child(" + bfo + ") div").append("<p>" + jQuery(this).attr("data-desc") + "</p>");
    });
}

function buying_boxes() {

    if (document.getElementById("box_production")) {

        return document.getElementById("box_production").value == "supplier";

    }

    return false;

}

function get_supplier_break_points() {

    if(document.getElementsByName("supplier_price_qty_0").length == 0){
        return "[]";
    }

    let break_points = {};

    for (let i = 0; i < 5; i++) {

        let qty = document.getElementsByName("supplier_price_qty_" + i)[0].value;
        let cost = document.getElementsByName("supplier_price_cost_" + i)[0].value;

        break_points[qty] = (cost * qty).toFixed(2);

    }
    return JSON.stringify(break_points);

}


function enterpress(event) {
    // 13 is the keycode for "enter"
    if (event.keyCode == 13 && event.shiftKey) {
        //document.getElementById("enterStatus").innerHTML = "Triggered enter+shift";
        linebreaker()
    }
    if (event.keyCode == 13 && !event.shiftKey) {
        //document.getElementById("enterStatus").innerHTML = "Triggered enter";
        linebreaker()
    }
}

function linebreaker() {
    jQuery('textarea').val(jQuery('textarea').val() + "\n")
}
