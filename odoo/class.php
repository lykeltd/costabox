<?php

/**
 * This file contains common functions for communicating with Odoo
 */

class Costabox_Odoo {

    private string $JSON_DIR = "";
    private string $PYTHON_PATH = "";
    private string $DOWNLOAD_URL;

    public function __construct() {
        $this->JSON_DIR = plugin_dir_path(__FILE__) . "json/";
        $this->PYTHON_PATH = plugin_dir_path(__FILE__) . "odoo_requests.py";
        $this->DOWNLOAD_URL = home_url() . "/instant_box_quote?download=";
    }

    function createQuoteJSON(array $contactDetails): string
    {

        // Get the user cart object
        $session = WC()->session;

        $cartId = $session->get('qbcb_cart_id');

        if(empty($cartId)) {
            $cartId = uniqid();
            $session->set('qbcb_cart_id', $cartId);
        }

        $session->save_data();

        $link = "<a href='" . $this->DOWNLOAD_URL . $cartId . "'>Download PDF</a>";

        $contact_name = $contactDetails['first_name'] . " " . $contactDetails['last_name'];

        if(is_user_logged_in()){
            $company = get_user_meta(get_current_user_id(), 'billing_company', true);

            if(!empty($company)){
                $contact_name .= "(" .  $company . ")";
            }
        }

        $data = array(
            "name" => "Quote - {$contact_name} - " . date("d-m-Y H:i"),
            "description" => "You can download the PDF file from the following link: " . $link,
            "partner_id" => $contactDetails['email'],
            "contact_name" => $contactDetails['first_name'] . " " . $contactDetails['last_name'],
            "phone" => $contactDetails['phone'],
            "type" => "opportunity",
            "activity_user_id" => 1,
            "date_deadline" => date("Y-m-d", strtotime("+1 week")),
        );

        // Assign the opportunity to the salesperson, if applicable
        $odoo_id = Costabox_User::get_odoo_id();
        if($odoo_id){
            $data["user_id"] = $odoo_id;
        }

        file_put_contents($this->JSON_DIR . $cartId . ".json", json_encode($data));

        // Return the name of the JSON file
        return $cartId . ".json";

    }

    public function createLead(array $contactDetails): int
    {
        $odoo_lead_id = WC()->session->get('qbcb_lead_id');

        if(!empty($odoo_lead_id)){
            $this->addLeadComment($odoo_lead_id);
            return $odoo_lead_id;
        }

        $jsonFile = $this->createQuoteJSON($contactDetails);
        $command = "python3 " . $this->PYTHON_PATH . " " . $jsonFile . " create_lead";
        $output = shell_exec($command);

        $id = intval($output);

        if($id > 0){
            WC()->session->set('qbcb_lead_id', $id);
            WC()->session->save_data();
        }

   //     unlink($this->JSON_DIR . $jsonFile);

        return $id;
    }

    function createLeadCommentJSON($leadId): string {

        // Get the user cart object
        $session = WC()->session;

        $cartId = $session->get('qbcb_cart_id');

        $fileName = $cartId . date("Hi") . ".json";

        $data = array(
            "model" => "crm.lead",
            "res_id" => $leadId,
            "body" => "Customer downloaded updated quote PDF",
        );

        file_put_contents($this->JSON_DIR . $fileName, json_encode($data));

        return $fileName;
    }

    function addLeadComment($leadId): void
    {
        $jsonFile = $this->createLeadCommentJSON($leadId);
        $command = "python3 " . $this->PYTHON_PATH . " " . $jsonFile . " create_lead_note";
        $output = shell_exec($command);

        unlink($this->JSON_DIR . $jsonFile);
    }

    public function deleteLead($leadId): void
    {
        $command = "python3 " . $this->PYTHON_PATH . " " . $leadId . " win_lead";
        shell_exec($command);
    }

    public function createContactFormLead(array $form_data): void
    {
        $jsonFile = $this->createContactFormJSON($form_data);
        $command = "python3 " . $this->PYTHON_PATH . " " . $jsonFile . " create_ticket";
        $output = shell_exec($command);

        unlink($this->JSON_DIR . $jsonFile);
    }

    function createContactFormJSON(array $form_data): string
    {

        $mapped_fields = [
            "street" => "address_1",
            "street2" => "address_2",
            "city" => "city",
            "zip" => "postcode",
        ];

        $description = "";
        foreach ($form_data as $key => $value) {
            if($key == "message" || $key == "subject") {
                $description .= "<strong>" . ucwords(str_replace("_", "", $key)) . "</strong>: " . $value . "<br>";
            }
        }


        $description .= "<br><strong>Address</strong><br>";

        foreach ($mapped_fields as $value) {
            if(isset($form_data[$value]) && !empty($form_data[$value])){
                $description .= $form_data[$value] . "<br>";
            }
        }

        $data = array(
            "name" => "New Contact Form Submission - " . date("d-m-Y H:i"),
            "description" => $description,
            "partner_id" => $form_data['email'],
            "partner_email" => $form_data['email'],
            "partner_name" => $form_data['first_name'] . " " . $form_data['last_name'],
            "partner_phone" => $form_data['phone'],
            "team_id" => 2,
        );

        $fileName = uniqid() . ".json";

        file_put_contents($this->JSON_DIR . $fileName, json_encode($data));

        // Return the name of the JSON file
        return $fileName;
    }

}

