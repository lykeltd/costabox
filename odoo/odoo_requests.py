
import xmlrpc.client
import time
import os
import json
import sys

class WatchForOdoo:

    # Constructor
    def __init__(self, filename, action):
        if action == 'win_lead':
            self.win_lead(filename)
            return

        filename = (os.path.dirname(os.path.realpath(__file__)) + "/json/" + filename)

        data = self.read_file(filename)
        if data is None:
            return

        if action == 'create_lead':
            self.create_lead(data)
        elif action == 'create_lead_note':
            self.create_note_on_lead(data)
        elif action == 'create_ticket':
            self.create_ticket(data)

        return

    # Parse JSON file
    def read_file(self,file):

        if not os.path.isfile(file):
            return None

        toReturn = None

        try:
            with open(file, 'r') as f:
                toReturn = json.load(f)

#             os.remove(file)
        except Exception as error:
            print(error)

        return toReturn

    # Get Odoo connection
    def get_odoo_connection(self):
#         return self.get_odoo_test_connection()
        url = 'https://quickbox.odoo.com'

        return xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

    def get_odoo_test_connection(self):
        url = 'https://emiprogithub-prj-quickbox-staging-7439953.dev.odoo.com'

        return xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

    # Get Odoo credentials
    def get_odoo_credentials(self):
#         return self.get_odoo_test_credentials()
        db = 'emiprogithub-prj-quickbox1-master-6664241'
        uid = 2
        password = '2a227909d7c07bf3c412d8364e27685a9cf72176'

        return (db, uid, password)

    def get_odoo_test_credentials(self):
        db = 'emiprogithub-prj-quickbox-staging-7439953'
        uid = 2
        password = '65758026075e71bd67346ed905e9b075a35b1679'

        return (db, uid, password)

    # Create lead in Odoo
    def create_lead(self, data):
        odoo = self.get_odoo_connection()
        (db, uid, password) = self.get_odoo_credentials()

        if not isinstance(data['partner_id'], int):
            # Try to get the partner id
            partner_id = odoo.execute_kw(db, uid, password, 'res.partner', 'search', [[['email', '=', data['partner_id']]]])

            if len(partner_id) == 0:
                data['email_from'] = data['partner_id']
                del data['partner_id']
            else:
                data['partner_id'] = partner_id[0]

        id = odoo.execute_kw(db, uid, password, 'crm.lead', 'create', [data])
        print(id)

    # Create helpdesk ticket in Odoo
    def create_ticket(self, data):
        odoo = self.get_odoo_connection()
        (db, uid, password) = self.get_odoo_credentials()

        if not isinstance(data['partner_id'], int):
            # Try to get the partner id
            partner_id = odoo.execute_kw(db, uid, password, 'res.partner', 'search', [[['email', '=', data['partner_id']]]])

            if len(partner_id) == 0:
                data['partner_email'] = data['partner_id']
                del data['partner_id']
            else:
                data['partner_id'] = partner_id[0]

        id = odoo.execute_kw(db, uid, password, 'helpdesk.ticket', 'create', [data])
        print(id)

    # Create note on lead in Odoo
    def create_note_on_lead(self, data):
        odoo = self.get_odoo_connection()
        (db, uid, password) = self.get_odoo_credentials()

        id = odoo.execute_kw(db, uid, password, 'mail.message', 'create', [data])
        print(id)

    # Win lead in Odoo
    def win_lead(self, data):
        odoo = self.get_odoo_connection()
        (db, uid, password) = self.get_odoo_credentials()

        id = odoo.execute_kw(db, uid, password, 'crm.lead', 'write', [[int(data)], {"probability": 100, "stage_id": 4}])
        print(id)



if __name__ == "__main__":
    WatchForOdoo(sys.argv[1], sys.argv[2])