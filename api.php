<?php

/*
* This file registers REST routes and corresponding API functions
*/

function costabox_api_box_types_by_size( $width, $length, $height ){

	global $wpdb;

	$table = $wpdb->prefix . "qbcb_box_type_restrictions";
	$width_ids = array();
	$length_ids = array();
	$height_ids = array();

	// Get the box types that allow for the specified width
	$sql = "SELECT box_type_id FROM {$table} WHERE (min <= {$width} OR min IS NULL) AND (max >= {$width} OR max IS NULL) AND dimension = 'breadth'";
	if(empty($width)) $sql = "SELECT box_type_id FROM {$table} WHERE max = 0 AND dimension = 'breadth'";

	$results = $wpdb->get_results($sql);

	if(empty($results)) return "";

	foreach($results as $row)
		$width_ids[] = $row->box_type_id;

	// Get the box types that allow for the specified length
	$sql = "SELECT box_type_id FROM {$table} WHERE (min <= {$length} OR min IS NULL) AND (max >= {$length} OR max IS NULL) AND dimension = 'length'";
	if(empty($length)) $sql = "SELECT box_type_id FROM {$table} WHERE max = 0 AND dimension = 'length'";

	$results = $wpdb->get_results($sql);

	if(empty($results)) return "";

	foreach($results as $row)
		$length_ids[] = $row->box_type_id;

	// Get the box types that allow for the specified height
	$sql = "SELECT box_type_id FROM {$table} WHERE (min <= {$height} OR min IS NULL) AND (max >= {$height} OR max IS NULL) AND dimension = 'height'";
	if(empty($height)) $sql = "SELECT box_type_id FROM {$table} WHERE max = 0 AND dimension = 'height'";

	$results = $wpdb->get_results($sql);

	if(count($results) < 1) return "";

	foreach($results as $row)
		$height_ids[] = $row->box_type_id;

	// Find the common IDs that allow these dimensions
	$box_types = array_intersect($width_ids, $length_ids, $height_ids);

	return $box_types;

}

function costabox_can_use_stock($width, $height, $length, $box_type_id){

	$calculated_box_width = 0;
	$calculated_box_length = 0;
	$calculated_lid_width = 0;
	$calculated_lid_length = 0;
	// The following function will populate the above variables by reference
	try{
		$unit_area = costabox_area_required($box_type_id, 14, $height, $length, $width, $calculated_box_width, $calculated_box_length, $calculated_lid_width, $calculated_lid_length);
	}catch(Exception $e){
		return false;
	}

	$blank_length =  costabox_get_setting('stock_board_chop');
	$blank_width =  costabox_get_setting('stock_board_deckle');

	$qty = costabox_get_qty_per_blank( 14, $calculated_box_length, $calculated_box_width, null, $blank_length, $blank_width);

	if($qty < 1) return false;

	return true;

}

function costabox_endpoint_box_types_by_size( WP_REST_Request $request ){

	$width = $request->get_param("width");
	$length = $request->get_param("length");
	$height = $request->get_param("height");
	$use_stock = $request->get_param("use_stock");

	$box_types = costabox_api_box_types_by_size($width, $length, $height);

	$eligible_box_types = array();

	if(is_array($box_types)){
		foreach($box_types as $bt){
			if($use_stock == 1 && !costabox_can_use_stock($width, $height, $length, $bt))
				continue;

			$eligible_box_types[] = $bt;
		}
	}

	return implode(",", $eligible_box_types);

}


function costabox_endpoint_stock_flutes( WP_REST_Request $request ){

	$stock_flutes = Costabox_Board::getStockFlutes();

	$stock_flutes = array_unique($stock_flutes);

	return implode(",", $stock_flutes);

}

function costabox_api_dimension_restrictions( $box_type, $dimension){

	// Get the minimum and maximum value for the specified dimension and box type
	global $wpdb;

	$sql = "SELECT min, max FROM {$wpdb->prefix}qbcb_box_type_restrictions WHERE box_type_id = {$box_type} AND dimension = '{$dimension}'";
	$restrictions = $wpdb->get_row($sql);

	if(is_null($restrictions)){
		return "500"; // Return an error if no restrictions are found
	}

	$data = array(
		"min" => $restrictions->min,
		"max" => $restrictions->max,
	);

	return json_encode($data);

}

function costabox_endpoint_dimension_restrictions( WP_REST_Request $request ){

	$dimension = $request->get_param("dimension");
	$box_type = $request->get_param("box_type");

	return costabox_api_dimension_restrictions($box_type, $dimension);

}

function costabox_endpoint_calculate_quote( WP_REST_Request $request ){

    require_once 'classes/Costabox_Box.php';

	$box_type_id = $request->get_param("box_type");
	$flute_id = $request->get_param("flute");
	$grade_id = $request->get_param("grade");
	$qty = $request->get_param("qty");
	$length = $request->get_param("length");
	$width = $request->get_param("width");
	$height = $request->get_param("height");
	$user = $request->get_param('user');
    $requestFiles = $request->get_file_params();
    $stock_prices = false;

    if($qty == 0) return 1;


    $box_ref = $request->get_param('quote_ref');
    $custom_box = new Costabox_Box($box_type_id, $flute_id, $grade_id, $qty, $height, $width, $length, $box_ref ?? "");
    $custom_box->setCustomer($user);


	$supplier_prices = $request->get_param('supplier_prices');
	if($supplier_prices){
        // Convert the supplier prices to an array (From JSON)
        $supplier_prices = json_decode($supplier_prices, true);
        $custom_box->setSupplierPrices($supplier_prices, $request->get_param('supplier_name'), $request->get_param('supplier_ref'), $requestFiles['supplier_document'] ?: null);
    }

	if($request->get_param('stock_prices') == 1){
        $custom_box->useStock();
        $stock_prices = true;
    }

	$chop = $request->get_param("chop") ?? 0;
	$qty_up = $request->get_param("qty_up") ?? 0;
	$deckle = $request->get_param("deckle") ?? 0;

    if($chop + $deckle + $qty_up > 0){
        $custom_box->setDiecutDimensions($chop, $deckle, $qty_up);
        $custom_box->setBoxDescription($request->get_param("description") ?: "");

        if(isset($requestFiles['die_cut_artwork'])){
            $custom_box->attachArtwork($requestFiles['die_cut_artwork'], true);
        }
    }

    if(isset($requestFiles['artwork'])){
        $custom_box->attachArtwork($requestFiles['artwork'], false);
    }

    if($request->get_param("box_printing") == "printed"){
        $custom_box->setPrinted(true);
    }

    if($request->get_param("handhole_box") == 'true'){
        $custom_box->setHandholes(true);
    }


	$unit_price_container = [];

    $custom_box->calculate();

    $total = $custom_box->getTotalPrice();
	$unit = ($total / $qty);

	$dispatch_date = $custom_box->getDispatchDate();
    $additional_info = $custom_box->getAdditonalInformation();

	// Generate price break table
    $price_break_table = "";
	$ctr = 0;
	$prev_unit = 0;
	$price_break_updated = [];
	
	$table_rows = "";

    if(!$stock_prices) {

        foreach ($custom_box->getPricingArray() as $break_qty => $break_price) {

            if ($break_qty == 0) $break_qty = 1;

            if (!$break_price || $break_qty < $additional_info['moq']) continue;

            $unit_price = round($break_price / $break_qty, 9);
            if ($prev_unit == $unit_price) continue;

            if ($ctr > 2) break;

            $prev_unit = $unit_price;
            $break_price_total = number_format(($unit_price * $break_qty), 2);

            $price_break_updated[$break_qty] = $break_price_total;

            $unit_price_display = round($unit_price, 2);
            $unit_price_container[] = $unit_price_display;
            $table_rows .= "<tr><td>{$break_qty}</td><td>£{$unit_price_display}</td><td>£{$break_price_total}</td></tr>";
            $ctr++;
        }

        //$price_break_table = "<table><tr><th>Quantity:</th>" . $price_break_table_qty . "</tr><tr><th>Unit Price:</th>" . $price_break_table_unit . "</tr><tr><th>Total:</th>" . $price_break_table_total . "</tr></table>";

        $price_break_table = "<table><tr><th>Quantity</th><th>Unit Price (excl. VAT)</th><th>Total</th><tr>{$table_rows}</table>";
    }


//	print_r($additional_info);
	return new WP_REST_Response(array(
        "object" => $custom_box,
		"total" => number_format($total,2),
		"unit" => number_format($unit, 9),
		"dispatch" => $dispatch_date,
		"weight" => number_format($custom_box->getWeight(), 6),
		"price_breaks_html" => $price_break_table,
		"price_breaks" => json_encode($additional_info['breaks']),
		"rsc" => $additional_info['rsc'],
		"moq" => $additional_info['moq'],
        "board_cost" => $additional_info['board_cost'],
        "labour_cost" => $additional_info["labour_cost"],
        "labour_time" => $additional_info["labour_time"],
        "total_area" => $additional_info["total_area"],
        "chop" => $additional_info["box_chop"],
        "deckle" => $additional_info["box_deckle"],
		"printing_cost" => $custom_box->getPrintingCost(),
        "handhole_cost" => $custom_box->getHandholeCost() / $custom_box->getQuantity(),
        "printing_cost_total" => $custom_box->getPrintingCost(),
		"unit_price_container" => $unit_price_container,
		"price_break_updated" => json_encode($price_break_updated),
        "margin" => $additional_info['margin'] ?? 50,
	));

}

function costabox_api_register_endpoints(){

	register_rest_route('/costabox', '/get_dimension_restrictions', array(
    	'methods' => 'POST',
    	'callback' => 'costabox_endpoint_dimension_restrictions',
   	));

	register_rest_route('/costabox', '/box_types_by_size', array(
    	'methods' => 'POST',
    	'callback' => 'costabox_endpoint_box_types_by_size',
    ));

	register_rest_route('/costabox', '/stock_flutes', array(
    	'methods' => 'POST',
    	'callback' => 'costabox_endpoint_stock_flutes',
   	));

	register_rest_route('/costabox', '/calculate_quote', array(
    	'methods' => 'POST',
    	'callback' => 'costabox_endpoint_calculate_quote',
   	));

}

add_action("rest_api_init", "costabox_api_register_endpoints");