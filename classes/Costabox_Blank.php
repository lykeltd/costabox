 <?php

class Costabox_Blank implements DVDoug\BoxPacker\Item {

	private $chop = 0;
	private $deckle = 0;
	private $depth = 0;
    private $weight = 0;

	public function __construct( $width, $length, $depth = 6, $weight = 1 ){

		$this->chop = $width;
		$this->deckle = $length;
		$this->depth = $depth;
        $this->weight = $weight;

	}

	/**
     * Item SKU etc.
     *
     * @return string
     */
    public function getDescription(){
    	return "Blank";
    }

    /**
     * Item width in mm.
     *
     * @return int
     */
    public function getWidth(){
    	return $this->chop;
    }

    /**
     * Item length in mm.
     *
     * @return int
     */
    public function getLength(){
    	return $this->deckle;
    }

    /**
     * Item depth in mm.
     *
     * @return int
     */
    public function getDepth(){
    	return $this->depth;
    }

    /**
     * Item weight in g.
     *
     * @return int
     */
    public function getWeight(){
    	return $this->weight * 1000;
    }

    /**
     * Item volume in mm^3.
     *
     * @return int
     */
    public function getVolume(){
        if(!(is_numeric($this->getWidth()) || is_numeric($this->getLength()) || is_numeric($this->getDepth()))){
            return 0;
        }
    	return $this->getWidth() * $this->getLength() * $this->getDepth();
    }

    /**
     * Does this item need to be kept flat / packed "this way up"?
     *
     * @return bool
     */
    public function getKeepFlat(){
    	return true;
    }

}