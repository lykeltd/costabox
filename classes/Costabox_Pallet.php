<?php

class Costabox_Pallet implements DVDoug\BoxPacker\Box {
	 /**
     * Reference for box type (e.g. SKU or description).
     *
     * @return string
     */

	 private $oversized = 1;

	 private $x = 1;
	 private $y = 1;

	 public function __construct($x = 1, $y = 1)
     {
         $this->x = $x;
         $this->y = $y;
     }

    public function getReference() {
    	return ($this->getOversized() > 1 ? "Oversized Pallet" :"Pallet");
    }

    /**
     * Outer width in mm.
     *
     * @return int
     */
    public function getOuterWidth(){
    	return $this->x * 1300;
    }

    /**
     * Outer length in mm.
     *
     * @return int
     */
    public function getOuterLength(){
        return $this->y * 1300;
    }

    /**
     * Outer depth in mm.
     *
     * @return int
     */
    public function getOuterDepth(){
    	return 2200;
    }

    /**
     * Empty weight in g.
     *
     * @return int
     */
    public function getEmptyWeight(){
    	return 1;
    }

    /**
     * Inner width in mm.
     *
     * @return int
     */
    public function getInnerWidth(){
        return $this->x * 1220;
    }

    /**
     * Inner length in mm.
     *
     * @return int
     */
    public function getInnerLength(){
        return $this->y * 1220;
    }

    /**
     * Inner depth in mm.
     *
     * @return int
     */
    public function getInnerDepth(){
    	return 2200;
    }

    /**
     * Total inner volume of packing in mm^3.
     *
     * @return int
     */
    public function getInnerVolume(){
    	return $this->getInnerWidth() * $this->getInnerLength() * $this->getInnerDepth();
    }

    /**
     * Max weight the packaging can hold in g.
     *
     * @return int
     */
    public function getMaxWeight(){
    	return 10000000;
    }

    /**
     * @return int|mixed
     */
    public function getOversized()
    {
        return $this->x * $this->y;
    }

    public static function getHeight(): int
    {
        return 2190;
    }
}