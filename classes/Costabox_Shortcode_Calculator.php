<?php

include_once 'Costabox_Shortcode_Form.php';
include_once 'Costabox_User.php';
require_once 'Costabox_Box.php';

class Costabox_Shortcode_Calculator extends Costabox_Shortcode_Form
{

    private array $fields = array();
    private ?WP_User $user = null;
    private bool $is_staff = false;
    private bool $is_platinum = false;
    private string $timestamp = "";
    private string $placeholderImage = "";
    private string $brownImage = "";
    private string $whiteImage = "";
    public function __construct()
    {
        $this->fields = [
            "customer_id" => [],
            "box_type_id" => [],
            "box_colour" => [],
            "box_flute" => [],
            "flute_edit" => [],
            "board_grade" => [],
            "length" => [],
            "width" => [],
            "height" => [],
            "qty" => [],
            "box_ref" => [],
            "plate_code" => [],
            "forme_code" => [],
            "box_printing" => [],
            "handhole_box" => [],
        ];

        $root_user = Costabox_User::get_user();
        $this->user = wp_get_current_user() ?: null;
        $this->is_staff = $this->user && $root_user && Costabox_User::is_staff($root_user->ID);
        $this->is_platinum = $this->user && Costabox_User::in_group("Platinum", $this->user->ID);
        $this->timestamp = date("Y-m-d H:i:s");
        $this->placeholderImage = plugin_dir_url(dirname(__FILE__) . "/costabox.php") . "/img/select_placeholder.png";
        $this->brownImage = plugin_dir_url(dirname(__FILE__)) . "/img/brown.png";
        $this->whiteImage = plugin_dir_url(dirname(__FILE__)) . "/img/white.png";

        parent::__construct("calculator", $this->fields, callback: array($this, 'output'));

        add_shortcode("costabox_calculator", array($this, "output")); // Legacy shortcode
    }

    private function enqueue_scripts(): void
    {

        wp_enqueue_script("image_picker", plugin_dir_url(dirname(__FILE__)) . "/js/image-picker.min.js", array('jquery'), in_footer: true);
        wp_enqueue_style("image_picker", plugin_dir_url(dirname(__FILE__)) . "/css/image-picker.css");

        wp_enqueue_script("qbcb_calculator", plugin_dir_url(dirname(__FILE__)) . "/js/calculator.js", array('jquery', "image_picker"), $this->timestamp, in_footer: true);
        wp_enqueue_style("qbcb_calculator", plugin_dir_url(dirname(__FILE__)) . "/css/calculator.css", [], $this->timestamp);
        wp_localize_script( 'qbcb_calculator', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    }

    public function output(): string
    {
        $this->actions();
        $this->enqueue_scripts();

        if(isset($_GET['qid'])) {
            $values = $this->get_quote_values($_GET['qid']);
        }elseif(isset($_GET['readd'])){
            $values = $this->get_cart_values($_GET['readd']);
        }else{
            $values = $this->get_form_values();
        }

        $html = "<form method='POST' class='costabox' enctype='multipart/form-data'>"; // Open form

        if(isset($values['quote_id'])) $html .= "<input type='hidden' name='quote_id' value='" . $values['quote_id'] . "'>";

        if($this->user) {
            $html .= "<input type='hidden' id='this_user' name='this_user' value='" . $this->user->ID . "'>";
            $html .= "<input type='hidden' name='user' value='" . $this->user->ID . "'>";
        }else{
            $html .= "<input type='hidden' id='this_user' name='this_user' value='0'>";
            $html .= "<input type='hidden' name='user' value='0'>";
        }

        $html .= "<h3><span class='highlight'>Step 1</span>Dimensions</h3>";
        $html .= "<p>We recommend measuring the outside dimensions of the items you need to pack and adding a couple of millimetre tolerance. These will be the <strong>internal</strong> dimensions of your boxes.</p>";

        $html .= "<div class='qbcb_measurements'>";
        $html .= "<div class='one-third first box_length'><label for='box_length'>Length (mm)</label><input type='number' name='box_length' id='box_length' min='0' value='" . $values['length'] . "' ></div>";
        $html .= "<div class='one-third box_width '><label for='box_width'>Width (mm)</label><input type='number' name='box_width' id='box_width' min='0' value='" . $values['width'] . "' ><em style='font-size:80%;margin-top:-20px; display: block'>Please ensure this is not greater than the length</em></div>";
        $html .= "<div class='one-third box_height '><label for='box_height'>Height (mm)</label><input type='number' name='box_height' id='box_height' min='0' value='" . $values['height'] . "'' ></div>";
        $html .= "</div>";

        $html .= "<div class='box_type_wrapper'>";

        $lead_time_diff = costabox_get_setting("lead_time") - costabox_get_setting("stock_lead_time");

        $html .= "<div id='stock_message' style='display: none;'><strong>Need your boxes in a hurry?</strong><br>We're currently experiencing a delay on board deliveries, however we can produce some boxes out of board we have in stock for these dimensions. It will cost you slightly more but they will be delivered {$lead_time_diff} days faster. Would you like to use stock board?<p><a class='qbcb_radio_button' href='#!' style='clear: left;'>Yes</a><a class='qbcb_radio_button selected' href='#!'>No</a></p></div>";

        $html .= "<hr class='is-style-wide'>";

        $html .= "<h3><span class='highlight'>Step 2</span>Select The Box Type</h3>";

        // Create a template for the box type select, so that if an option is disabled it can be deleted in the one visible to the user
        $box_type_template = "<select id='box_type_template' style='display: none;'>";

        $html .= "<select name='box_type' id='box_type' class='has_images'> "; // Open box type select

        $html .= "<option value='0'>Default</option>";

        foreach(Costabox_Box::getAllTypes(true) as $box_type_id => $box_type){
            if(!$this->is_staff && $box_type['code'] == "DIE-CUT"){
                continue;
            }

            $box_code = $box_type['code'] == "DIE-CUT" ? "DC" : $box_type['code'];

            $img_src = $box_type['image'] ? wp_get_attachment_image_url($box_type['image'], 'full'): $this->placeholderImage;
            $img = "data-img-src='{$img_src}'";
            $selected = $values['box_type_id'] == $box_type_id ? "selected" : "";

            $box_type_template .= "<option {$img} value='{$box_type_id}' data-box-code='{$box_code}'>" . $box_type['code'] . ' ' . $box_type['name'] . "</option>";
            $html .= "<option {$img} value='{$box_type_id}' data-box-code='{$box_code}' {$selected}>" . $box_type['code'] . ' ' . $box_type['name'] . "</option>";
        }

        $box_type_template .= "</select>";

        $html .= "</select>" . $box_type_template; // Close box type select

        $html .= "<div class='die_cut' style='width: 25%; display: inline-block;'>";
        $html .= "<label for='die_cut_code'>Enter your box code/reference:</label>";
        $html .= "<input type='text' name='die_cut_code' id='die_cut_code' placeholder='Bespoke Box Code'>";
        $html .= "</div>";

        $html .= "<div class='die_cut' style='width: 25%; display: inline-block;'>";
        $html .= "<label for='die_cut_chop'>Enter your chop:</label>";
        $html .= "<input type='text' name='die_cut_chop' id='die_cut_chop' value='{$values['die_cut_chop']}'>";
        $html .= "</div>";

        $html .= "<div class='die_cut' style='width: 25%; display: inline-block;'>";
        $html .= "<label for='die_cut_deckle'>Enter your deckle:</label>";
        $html .= "<input type='text' name='die_cut_deckle' id='die_cut_deckle' value='{$values['die_cut_deckle']}'>";
        $html .= "</div>";

        $html .= "<div class='die_cut' style='width: 25%; display: inline-block;'>";
        $html .= "<label for='die_cut_up'>Enter your UP quantity:</label>";
        $html .= "<input type='text' name='die_cut_up' id='die_cut_up' value='{$values['die_cut_up']}'>";
        $html .= "</div>";

        $html .= "<div class='die_cut' style='width: 50%; display: inline-block;'>";
        $html .= "<label for='die_cut_attach'>Attach files</label>";
        $html .= "<input type='file' name='die_cut_attach' id='die_cut_attach'>";
        $html .= "</div>";

        $html .= "</div>";

        $html .= "<div class='no_box_type_wrapper' style='margin-top: 20px; display: none'><hr class='is-style-wide'><h3><span class='highlight'>Step 2</span>Select The Box Type</h3>There are no box types available that are suitable those dimensions. Please <a href='" . home_url("contact") . "'>get in touch with the sales team</a> to discuss how this can be overcome.</div>";

        $html .= "<div class='box_color_wrapper'>";

        $html .= "<hr class='is-style-wide'>";

        $html .= "<h3><span class='highlight'>Step 3</span>Select The Cardboard Colour</h3><p>If you'd like print on the box, we recommend choosing white.</p>";

        $html .= "<select name='box_colour' id='box_colour' class='has_images'>"; // Open box colour select

        $html .= "<option value='brown' " . ($values['box_colour'] == 'brown' ? "selected" : "") . " data-img-src='" . $this->brownImage . "'>Brown</option>";
        $html .= "<option value='white' " . ($values['box_colour'] == 'white' ? "selected" : "") . " data-img-src='" . $this->whiteImage . "'>White</option>";

        $html .= "</select>"; // Close box colour select

        $html .= "</div>";

        $html .= "<div class='box_thickness_wrapper'>";

        $html .= "<hr class='is-style-wide'>";

        $html .= "<h3><span class='highlight'>Step 4</span>Select The Cardboard Thickness</h3>";

        $html .= "<select name='box_flute' id='box_flute' class='" . ($this->is_staff || $this->is_platinum ? '' : 'has_images') . "' >"; // Open box thickness select

        foreach (Costabox_Board::getAllFlutes(false, true, ($this->is_staff || $this->is_platinum)) as $flute_id => $flute) {
            $selected = $values['box_flute'] == $flute_id ? "selected" : "";
            $html .= "<option data-color='{$flute['colour']}' value='{$flute_id}' data-desc='{$flute['description']}' {$selected} data-img-src='{$flute['image']}' >{$flute['flute']}</option>";
        }

        $html .= "</select>"; // Close box thickness select

        $grades = Costabox_Board::getAllGrades(($this->is_staff || $this->is_platinum));

        if($this->is_staff || $this->is_platinum){
            $html .= "<select name='board_grade' id='board_grade' class=''> "; // Open board grade select

            foreach($grades as $val => $grade){
                $html .= "<option value='{$val}' " . ($values['grade'] == $val ? "selected" : "") . " >{$grade}</option>";
            }

            $html .= "</select>"; // Close board grade select
        } else {

            $grades = array_flip($grades);

            $html .= "<input type='hidden' name='board_grade' id='board_grade' value='" . (empty($values['grade']) ? $grades['200'] : $values['grade']) . "'>";

            $html .= "Be sure to read our <a target='_blank' href='https://quickbox.co/buying-guide/'>Buying Guide</a> for further guidance .Need alternative or specific boards? <a href='/contact' target='_blank'>Get in touch with us today.</a><br><br>";


        }

        $html .= "</div>";

        if($this->is_staff){

            $html .= "<div class='box_production_method'>
                    <h3>How will these boxes be produced?</h3>
                    <select name='box_production' id='box_production'>
                        <option value='production'>In-house</option>                        
                        <option value='supplier'>Supplier</option>
                    </select>
        ";
            $html .= "<div class='box_production_method'>
					
                    <select name='box_printing' id='box_printing'>
                        <option value='printed'>Printed</option>                        
                        <option selected value='plain'>Plain</option>
                    </select></div>
        ";
            $html .= "<div id='box_supplier_breaks' style='display: none;'>";

            $html .= "<label for='supplier_finish'>Finish</label><select name='supplier_finish' id='supplier_finish'>
	    	<option value='0'>Plain</option>
	    	<option value='1'>1 Colour</option>
	    	<option value='2'>2 Colour</option>
	    	<option value='3'>3 Colour</option>
	    </select>";


            $qty_cols = "";
            $price_cols = "";
            $col_val = 1;

            for ($i = 0; $i  < 5; $i++){

                if($i > 0) $col_val = "";

                $qty_cols .= "<td><input type='number' name='supplier_price_qty_{$i}' step='0.01'  min='1' value='{$col_val}'/></td>";
                $price_cols .= "<td><input type='number' name='supplier_price_cost_{$i}' step='0.01' min='0' /></td>";

            }

            $html .= "<label>Pricing</label><br><table><tr><th>Qty</th>{$qty_cols}</tr><tr><th>Price</th>{$price_cols}</tr></table>";

            $html .= "</div>";

            $html .= "<div style='display: none' id='artwork_upload'><label for='artwork'>Upload Artwork <em>(Optional)</em></label><input style='width: 100%; padding: 10px; padding-bottom: 15px;' type='file' name='artwork' id='artwork'></div>";

            $html .= "<div id='box_supplier_details' style='display: none;'>";

            $html .= "<div class='flex_column av_one_half  flex_column_div first'><label for='supplier_name'>Supplier name</label><input type='text' name='supplier_name' id='supplier_name' value='' ></div>";

            $html .= "<div class='flex_column av_one_half  flex_column_div '><label for='supplier_ref'>Supplier Reference</label><input type='text' name='supplier_ref' id='supplier_ref' value='' ></div>";

            $html .= "<label for='supplier_document'>Supplier Quote</label><input style='width: 100%; padding: 10px; padding-bottom: 15px;' type='file' name='supplier_document' id='supplier_document'>";

            $html .= "</div>";


            $html .= "<div class='box_production_method'>
				 <label for='handhole_box' style='display: inline-block;    position: relative;    top: -2px;
'> Add Handhole Cost </label>
                   <input type='checkbox' id='handhole_box' name='handhole_box' style='height: 11px;' >
        </div>";


            $html .= "<div class='box_additional_costs' style='display: none;'><h3>Additional Costs</h3>";

            $html .= "<div class='flex_column av_one_half  flex_column_div first' style='float: left; padding-right: 20px;'><label for='plate_cost'>Artwork Setup</label><input type='number' name='plate_cost' id='plate_cost' step='0.01' min='0' ></div>";
            $html .= "<div class='flex_column av_one_half  flex_column_div first'><label for='plate_code'>Printing Plate Code</label><input type='text' name='plate_code' id='plate_code' ></div>";

            $html .= "<div class='flex_column av_one_half  flex_column_div ' style='float: left; padding-right: 20px'><label for='forme_cost'>Forme</label><input type='number' name='forme_cost' id='forme_cost' step='0.01' min='0' ></div>";
            $html .= "<div class='flex_column av_one_half  flex_column_div '><label for='forme_code'>Forme Code</label><input type='text' name='forme_code' id='forme_code' ></div>";

            $html .= "</div>";

        }

        $html .= "<div class='box_qty_wrapper'>";

        $html .= "<hr class='is-style-wide'>";

        $html .= "<h3><span class='highlight'>Step 5</span>Quantity</h3><p>Prices may vary according to the area of board required. Here is the break down:</p>";

        $html .= "<div id='price_break_table' class='qbcb'></div><input type='hidden' name='price_break_html' id='price_break_html' >";

        $html .= "<p id='moq_notice' style='display: none;'>Due to the board grade and box size, <strong>our minimum order quantity for this box is <span id='box_moq'>0</span></strong></p>";

        $html .= "<div class='one-third first'><label for='qty'>Quantity</label><input type='number' min='0' maxlength ='9' oninput='maxLengthCheck(this)' onchange='maxLengthCheck(this)' name='qty' id='qty' value='" . $values['qty'] . "' >&ensp;<div id='qty-spinner' uk-spinner hidden></div></div>";

        $html .= "<div style='clear: both'></div>";

        $html .= "</div>";

        if($this->is_staff) {
            $html .= "<div id='diagnostics'>";

            $html .= "<p class='board_cost'>Board Cost: £<strong></strong></p>";
            $html .= "<p class='labour_cost'>Labour Cost: £<strong></strong></p>";
            $html .= "<p class='labour_time'>Labour Time: <strong></strong>s</p>";
            $html .= "<p class='total_area'>Total Area: <strong></strong>m<sup>2</sup></p>";
            $html .= "<p class='chop'>Chop: <strong></strong>mm</p>";
            $html .= "<p class='deckle'>Deckle: <strong></strong>mm</p>";
            $html .= "<p class='margin'>Margin: <strong></strong></p>";

            $html .= "</div>";
        }

        $html .= "<div id='price_panel' >"; // Open price_panel container

        if ($this->is_staff) {
            $html .= "<div style='font-size: 145%;' class='printing_cost_display'>Printing Cost Per Box: <strong id='cpc'>£</strong></span><br></div>";
        }

        $html .= "<div style='font-size: 145%;' class='handhole_cost_display'>Handhole Cost  Per Box: <strong id='chc'>£</strong></span><br></div>";

        $html .= "<span style='font-size: 175%;'><span style='font-weight: 700; padding-right: 25px'>Total: <strong id='total_price'>£</strong></span>";
        $html .= "Price Per Box: <strong id='ppb'>£</strong></span><br>";
        $html .= "<em style='font-size: 90%;'>Excludes shipping. Add to cart to calculate shipping.</em><br><br>";
        $html .= "Estimated Delivery Date: <strong id='dispatch_date'></strong>. Need them sooner? <a href='" . get_site_url(null,"contact") . "'>Get in touch with us today.</a><br><br>";
        $html .= "<input type='hidden' name='quote_price' id='quote_price'>";
        $html .= "<input type='hidden' name='stock_board' id='stock_board'>";
        $html .= "<input type='hidden' name='total_weight' id='total_weight'>";
        $html .= "<input type='hidden' name='box' id='box_object'>";


        $html .= "<input type='text' placeholder='Box reference (optional)' value='" . $values['box_ref'] . "' id='quote_ref' name='quote_ref' style='display: inline-block; width: auto; margin: 0px; padding: 12px 5px; margin-right: 10px; vertical-align: bottom;'>";

        $html .= "<input type='submit' class='uk-button uk-button-primary' id='add_to_cart' name='add_costabox_to_cart' style='display: none;' value='Add To Cart' />&nbsp;&nbsp;";

        if(!$this->user){
            $html .= "Registered customers can save boxes! <a href='/my-account'>Login or create an account.</a>";
        }else{
            $html .= "<a href='#!' class='uk-button uk-button-default' id='save_box' style='display: none;'>Save Box</a>";
        }

        $html .= "</div>"; // Close price_panel container

        return $html;
    }

    private function get_quote_values( int $quoteId ): array {

        $box = Costabox_Box::loadFromQuote( $quoteId );

        $values = $box->getFormValues();
        $values['quote_id'] = $quoteId;

        return $values;

    }

    private function get_cart_values( string $cartItemId ): array {

        WC()->initialize_cart();

        $box = WC()->cart->cart_contents[$cartItemId]['costabox_box'];

        return $box->getFormValues();

    }

    private function actions(): void
    {
        if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'db'){
            Costabox_DB::initialise();
        }
    }

    public function process(): void
    {
        // TODO: Implement process() method.
    }
}