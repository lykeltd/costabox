<?php

require_once 'Costabox_Box.php';
require_once 'Costabox_User.php';

class Costabox_Order_Functions
{

    private ?WP_User $user = null;

    public function __construct()
    {

        $this->user = Costabox_User::get_user() ?: null;

        add_filter("woocommerce_order_item_display_meta_value", array($this, "display_meta_value"), 10, 3);
        add_filter("woocommerce_new_customer_data", array($this, "new_cutomer_data"), 10, 1);

        add_action("woocommerce_checkout_update_order_meta", array($this, "placed_by_meta"), 10, 1);
        add_action("woocommerce_checkout_update_order_meta", array($this, "delivery_date_meta"), 10, 1);

        add_filter("manage_edit-shop_order_columns", [$this, "placed_by_column"], 10, 1);
        add_action("manage_shop_order_posts_custom_column", [$this, "placed_by_column_content"], 10, 2);
        add_action("restrict_manage_posts", [$this, "placed_by_column_filter"], 10, 2);
        add_filter("parse_query", [$this, "placed_by_column_filter_query"], 10, 1);

        add_action("woocommerce_admin_order_data_after_shipping_address", array($this, "display_delivery_date"), 10, 1);

        add_action("woocommerce_checkout_create_order", array($this, "remove_odoo_lead"), 10, 2);


        // Disable the "Order Again" feature as this does not work with the costabox function
        add_filter('woocommerce_valid_order_statuses_for_order_again', function () {
            return [];
        });

        // Set status to "Processing" when order is placed
        add_action("woocommerce_thankyou", [$this, "set_order_status"], 10, 1);

        // Make sure telephone number has country code
        add_action("woocommerce_thankyou", [$this, "add_tel_country_code"], 10, 1);

        add_filter("woocommerce_rest_check_permissions", "__return_true", 10, 4);

        add_action('rest_api_init', [$this, 'register_rest_routes']);

        add_filter("woocommerce_order_actions", [$this, "add_order_actions"], 10, 1);
        add_action("woocommerce_order_action_costabox_customer_email", [$this, "resend_customer_email"], 10, 1);

        add_action("woocommerce_email_after_order_table", [$this, "add_email_footer"], 10, 4);

    }

    public function add_email_footer(WC_Order $order, bool $sent_to_admin, bool $plain_text, WC_Email $email): void
    {
        if ($email->id === "customer_processing_order") {

            $this->display_delivery_date($order);

            $po_number = $order->get_meta("wc_checkout_purchase_order");

            if($po_number){
                echo "<p><strong>Purchase Order Number:</strong> {$po_number}</p>";
            }

        }
    }

    public function set_order_status( int $order_id ): void
    {
        $order = wc_get_order($order_id);
        $order->update_status("processing");
    }

    public function add_tel_country_code( int $order_id ): void
    {
        try {
            $order = wc_get_order($order_id);
            $billing_phone = $order->get_billing_phone();

            if (str_starts_with($billing_phone, "0")) {
                $billing_phone = "+44" . substr($billing_phone, 1);
                $order->set_billing_phone($billing_phone);
            }

            $shipping_phone = $order->get_shipping_phone();
            if (str_starts_with($shipping_phone, "0")) {
                $shipping_phone = "+44" . substr($shipping_phone, 1);
                $order->set_shipping_phone($shipping_phone);
            }

            $order->save();
        }catch (Exception $e){
            error_log($e->getMessage());
        }
    }

    public function add_order_actions( array $actions): array
    {
        $actions["costabox_customer_email"] = "Resend Customer Email";
        return $actions;
    }

    public function resend_customer_email( WC_Order $order): void
    {
        $mailer = WC()->mailer();

        foreach($mailer->get_emails() as $email){
            if($email->id === "customer_processing_order"){
                $email->trigger($order->get_id());
            }
        }

    }

    public function display_meta_value($value, $meta, $item): string
    {

        $isStaff = Costabox_User::is_staff(get_current_user_id());

        if(!$isStaff && in_array($meta->key, ["_Supplied by", ""])){
            return $value;
        }

        switch ($meta->key) {
            case "Artwork":
            case "Die Cut Files";
                $value = Costabox_Box::getArtworkDownloadLink($value);
                break;
            case "_Supplied by":
                $details = Costabox_Box::getSupplierDetails($value);
                $url = home_url("costabox?supplier_download={$value}");
                $value = "{$details['supplier']} (Ref: {$details['reference']}) | <a href='{$url}'>Download</a>";
                break;
            default:
                if(str_starts_with($meta->key, "_")){
                    $value = $meta->value;
                }

        }

        return $value;

    }

    public function new_cutomer_data( array $data ): array
    {
        $data['user_login'] = $data['user_email'];

        return $data;
    }


    public function placed_by_meta( int $order_id ): void
    {
        if($this->user){
            update_post_meta($order_id, "qbcb_placed_by", $this->user->ID);
        }
    }

    public function delivery_date_meta( int $order_id ): void
    {
        if (!empty($_POST['qbcb_preferred_delivery_date'])) {
            update_post_meta($order_id, 'qbcb_preferred_delivery_date', sanitize_text_field($_POST['qbcb_preferred_delivery_date']));
        }
    }

    public function display_delivery_date( WC_Order $order ): void
    {
        $delivery_date = get_post_meta($order->get_id(), 'qbcb_preferred_delivery_date', true);

        if($delivery_date){
            echo "<p><strong>Preferred Delivery Date:</strong> {$delivery_date}</p>";
        }
    }

    // Remove any reference to a lead in Odoo if the order is placed
    public function remove_odoo_lead( $order, $data ): void
    {
        WC()->session->__unset('qbcb_lead_id');
    }

    public function placed_by_column( $columns ): array
    {
        $columns['qbcb_placed'] = 'Placed By';
        return $columns;
    }

    public function placed_by_column_content( $column, $post_id ): void
    {
        if($column == "qbcb_placed"){

            $placed_by = get_post_meta($post_id, "qbcb_placed_by", true);

            if($placed_by && Costabox_User::is_staff($placed_by)){
                $user = get_user_by("ID", $placed_by);
                echo $user->display_name . " (Staff)";
            }else{
                echo "Customer";
            }

        }
    }

    public function placed_by_column_filter( $post_type, $which ): void
    {
        if($post_type == "shop_order" && $which == "top"){

            $users = get_users(['fields' => ['ID', 'user_login'], 'role__in' => ['administrator', 'quickbox_staff']]);

            $selected = $_GET['qbcb_placed_by'] ?? "";

            echo "<select name='qbcb_placed_by'>";
            echo "<option value=''>Order Taken By</option>";
            /** @var WP_User $user */
            foreach($users as $user){
                $selected_attr = $selected == $user->ID ? "selected" : "";
                echo "<option value='{$user->ID}' {$selected_attr}>{$user->user_login}</option>";
            }
            echo "</select>";

        }
    }

    public function placed_by_column_filter_query( $query ): void
    {
        global $pagenow, $typenow;

        if($pagenow == "edit.php" && $typenow == "shop_order" && !empty($_GET['qbcb_placed_by'])){

            $query->query_vars['meta_key'] = "qbcb_placed_by";
            $query->query_vars['meta_value'] = $_GET['qbcb_placed_by'];

        }
    }

    public function register_rest_routes(): void
    {
        register_rest_route( 'qbcb', '/die-cut-file/(?P<order_id>\d+)/(?P<item_id>\d+)', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_die_cut_file'],
        ) );

        register_rest_route( 'qbcb', '/supplier-file/(?P<order_id>\d+)/(?P<item_id>\d+)', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_supplier_file'],
        ) );

        register_rest_route( 'qbcb', '/artwork-file/(?P<order_id>\d+)/(?P<item_id>\d+)', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_artwork_file'],
        ) );

    }

    public function get_die_cut_file( WP_REST_Request $request ): WP_REST_Response
    {
        $order_id = $request->get_param('order_id');
        $item_id = $request->get_param('item_id');

        $order = wc_get_order($order_id);

        if(!$order){
            return new WP_REST_Response("Order not found", 404);
        }

        $item = $order->get_item($item_id);

        if(!$item){
            return new WP_REST_Response("Item not found", 404);
        }

        if(isset($item['Die Cut Files'])){
            $file = Costabox_Box::getArtworkFilePath($item['Die Cut Files']);

            $response = new WP_REST_Response(file_get_contents($file));
            $response->header('Content-Type', 'application/pdf');
            $response->header('Content-Disposition', 'attachment; filename="die-cut-file.pdf"');
            $response->header('Content-Length', filesize($file));


            return $response;
        }

        return new WP_REST_Response("No die cut file found", 404);
    }
    public function get_supplier_file( WP_REST_Request $request ): WP_REST_Response
    {
        $order_id = $request->get_param('order_id');
        $item_id = $request->get_param('item_id');

        $order = wc_get_order($order_id);

        if(!$order){
            return new WP_REST_Response("Order not found", 404);
        }

        $item = $order->get_item($item_id);

        if(!$item){
            return new WP_REST_Response("Item not found", 404);
        }

        $meta = $item->get_meta_data();
        foreach($meta as $m){
            if($m->key == "_Supplied by"){
                $file = Costabox_Box::getSupplierFile($m->value);

                if(!file_exists($file)){
                    return new WP_REST_Response("No supplier file found", 404);
                }

                $response = new WP_REST_Response(file_get_contents($file));
                $response->header('Content-Type', 'application/pdf');
                $response->header('Content-Disposition', 'attachment; filename="supplier-file.pdf"');
                $response->header('Content-Length', filesize($file));


                return $response;
            }
        }
        return new WP_REST_Response("No file found", 404);
    }

    public function get_artwork_file( WP_REST_Request $request ): WP_REST_Response
    {
        $order_id = $request->get_param('order_id');
        $item_id = $request->get_param('item_id');

        $order = wc_get_order($order_id);

        if(!$order){
            return new WP_REST_Response("Order not found", 404);
        }

        $item = $order->get_item($item_id);

        if(!$item){
            return new WP_REST_Response("Item not found", 404);
        }

        if(isset($item['Artwork'])){
            $file = Costabox_Box::getArtworkFilePath($item['Artwork']);

            $response = new WP_REST_Response(file_get_contents($file));
            $response->header('Content-Type', 'application/pdf');
            $response->header('Content-Disposition', 'attachment; filename="die-cut-file.pdf"');
            $response->header('Content-Length', filesize($file));


            return $response;
        }

        return new WP_REST_Response("No die cut file found", 404);
    }
}