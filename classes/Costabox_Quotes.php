<?php

class Costabox_Quotes
{
    public function __construct()
    {
        add_rewrite_endpoint('costabox-quotes', EP_ROOT | EP_PAGES);
        add_filter("woocommerce_account_menu_items", array($this, "account_menu_items"), 8, 1);
        add_action("woocommerce_account_costabox-quotes_endpoint", array($this, "endpoint_content"));
    }

    public function account_menu_items( array $items ): array
    {
        $items['costabox-quotes'] = "Your Boxes";

        return $items;
    }

    public  function endpoint_content(): void
    {
        $html = "<h4 style='text-align: center;'>You haven't saved any quotes yet.</h4><h2 style='text-align: center; color: #000; text-transform: uppercase'>Visit our <a href='" . home_url('costabox') . "'>Custom Box Calculator</a> to get started.</h2><a class='button' style='display: block; width: 40%; margin: 0 auto; margin-top: 40px;' href='" . home_url('costabox') . "'>Instant Box Quote</a>";

        global $wpdb;
        $table = $wpdb->prefix . "qbcb_quotes";

        $user = wp_get_current_user();
        $root_user = Costabox_User::get_user();
        $is_staff = Costabox_User::is_staff($root_user->ID);

        if (isset($_GET['del_qid'])) {

            $wpdb->delete($table, array(
                "customer_id" => $user->ID,
                "quote_id" => $_GET['del_qid']
            ));
        }

        if (isset($_GET['buy_qid'])) {
            // Add the box to the cart
            $box = Costabox_Box::loadFromQuote($_GET['buy_qid']);

            $addToCartUrl = home_url() . "/wp-json/costabox/add_to_cart";
            $ch = curl_init($addToCartUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                "box" => serialize($box)
            ]);

            echo curl_exec($ch);
            curl_close($ch);

//            wp_redirect(home_url("shopping-cart"));
        }

        $sql = "SELECT quote_id, date_produced, box_type_id, qty, ref, quoted_price FROM {$table} WHERE customer_id = " . $user->ID;

        $results = $wpdb->get_results($sql, "ARRAY_A");

        if (count($results) == 0) {
            echo $html;
            return;
        }

        $page_link = get_page_link(costabox_get_setting("page_id"));

        $html = "<table  class='qbcb_boxes'><tr><th>RSC</th><th>Description</th><th>Last Updated</th><th>Ref.</th><th>Price</th><th>Actions</th></tr>";

        foreach ($results as $q) {
            $html .= "<tr>";

            $box = Costabox_Box::loadFromQuote($q['quote_id']);

            if(!$box) {
                continue;
            }

            if($box->isDieCut() && !$is_staff) {
                continue;
            }

            $html .= "<td>" . $box->getRSC() . "</td>";

            $html .= "<td>" . $q['qty'] . " x " . $box->getBoxDescription(false) . "</td>";

            $html .= "<td>" . date("d M Y", strtotime($q['date_produced'])) . "</td>";

            $html .= "<td>" . $q['ref'] . "</td>";

            $html .= "<td>£" . $q['quoted_price'] . "*</td>";

            $html .= "<td style='padding: 0px 5px; vertical-align: middle; text-align: center;'>";

//    		$html .= "<a href='?buy_qid=" . $q['quote_id'] . "' class='button' style='padding: 7px 12px; margin: 0 5px;'>Add to cart</a>";
            $html .= "<a href='" . $page_link . "?qid=" . $q['quote_id'] . "' class='button' style='padding: 7px 12px; margin: 0 5px;'>Re-order</a>";
            $html .= "<a href='?del_qid=" . $q['quote_id'] . "' class='button' style='padding: 7px 12px; margin: 0 5px;'>Delete</a>";

            $html .= "</td>";

            $html .= "</tr>";
        }

        $html .= "</table>";

        $html .= "<small><em>* At the time of quote. Pricing subject to change.</em></small>";

        echo $html;
    }

}





