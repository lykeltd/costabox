<?php

class Costabox_New_User
{

    private static function get_public_domains(): array {
        return [
            /* Default domains included */
            'aol.com', 'att.net', 'comcast.net', 'facebook.com', 'gmail.com', 'gmx.com', 'googlemail.com',
            'google.com', 'hotmail.com', 'hotmail.co.uk', 'mac.com', 'me.com', 'mail.com', 'msn.com',
            'live.com', 'sbcglobal.net', 'verizon.net', 'yahoo.com', 'yahoo.co.uk',

            /* Other global domains */
            'email.com', 'fastmail.fm', 'games.com' /* AOL */, 'gmx.net', 'hush.com', 'hushmail.com', 'icloud.com',
            'iname.com', 'inbox.com', 'lavabit.com', 'love.com' /* AOL */, 'outlook.com', 'pobox.com', 'protonmail.ch', 'protonmail.com', 'tutanota.de', 'tutanota.com', 'tutamail.com', 'tuta.io',
            'keemail.me', 'rocketmail.com' /* Yahoo */, 'safe-mail.net', 'wow.com' /* AOL */, 'ygm.com' /* AOL */,
            'ymail.com' /* Yahoo */, 'zoho.com', 'yandex.com',

            /* United States ISP domains */
            'bellsouth.net', 'charter.net', 'cox.net', 'earthlink.net', 'juno.com',

            /* British ISP domains */
            'btinternet.com', 'virginmedia.com', 'blueyonder.co.uk', 'freeserve.co.uk', 'live.co.uk',
            'ntlworld.com', 'o2.co.uk', 'orange.net', 'sky.com', 'talktalk.co.uk', 'tiscali.co.uk',
            'virgin.net', 'wanadoo.co.uk', 'bt.com',

            /* Domains used in Asia */
            'sina.com', 'sina.cn', 'qq.com', 'naver.com', 'hanmail.net', 'daum.net', 'nate.com', 'yahoo.co.jp', 'yahoo.co.kr', 'yahoo.co.id', 'yahoo.co.in', 'yahoo.com.sg', 'yahoo.com.ph', '163.com', 'yeah.net', '126.com', '21cn.com', 'aliyun.com', 'foxmail.com',

            /* French ISP domains */
            'hotmail.fr', 'live.fr', 'laposte.net', 'yahoo.fr', 'wanadoo.fr', 'orange.fr', 'gmx.fr', 'sfr.fr', 'neuf.fr', 'free.fr',

            /* German ISP domains */
            'gmx.de', 'hotmail.de', 'live.de', 'online.de', 't-online.de' /* T-Mobile */, 'web.de', 'yahoo.de',

            /* Italian ISP domains */
            'libero.it', 'virgilio.it', 'hotmail.it', 'aol.it', 'tiscali.it', 'alice.it', 'live.it', 'yahoo.it', 'email.it', 'tin.it', 'poste.it', 'teletu.it',

            /* Russian ISP domains */
            'mail.ru', 'rambler.ru', 'yandex.ru', 'ya.ru', 'list.ru',

            /* Belgian ISP domains */
            'hotmail.be', 'live.be', 'skynet.be', 'voo.be', 'tvcablenet.be', 'telenet.be',

            /* Argentinian ISP domains */
            'hotmail.com.ar', 'live.com.ar', 'yahoo.com.ar', 'fibertel.com.ar', 'speedy.com.ar', 'arnet.com.ar',

            /* Domains used in Mexico */
            'yahoo.com.mx', 'live.com.mx', 'hotmail.es', 'hotmail.com.mx', 'prodigy.net.mx',

            /* Domains used in Canada */
            'yahoo.ca', 'hotmail.ca', 'bell.net', 'shaw.ca', 'sympatico.ca', 'rogers.com',

            /* Domains used in Brazil */
            'yahoo.com.br', 'hotmail.com.br', 'outlook.com.br', 'uol.com.br', 'bol.com.br', 'terra.com.br', 'ig.com.br', 'itelefonica.com.br', 'r7.com', 'zipmail.com.br', 'globo.com', 'globomail.com', 'oi.com.br',
        ];
    }
    public static function process($customer_id = 0): void
    {
        // Automatically set the user's country
        if(get_user_meta($customer_id, "billing_country", true) == ""){
            update_user_meta($customer_id, "billing_country", "GB");
            update_user_meta($customer_id, "shipping_country", "GB");
        }

        // The user may already be associated with a company, so check
        if(!empty(get_user_meta($customer_id, "wcb2brp_company", true))) return;


        $user = new WP_User($customer_id);
        $companyDomain = substr($user->user_email, strrpos($user->user_email, '@') + 1);
        $companyName = get_user_meta($customer_id,'billing_company', true );

        if(empty(trim($companyName)) || $companyName == "Personal"){
            return; // Don't try and associate the user with a company if they have no company name.
        }

        if ((in_array($companyDomain, self::get_public_domains()))) {
            $companyDomain = '';
        }


        // Check to see if a company has already been registered with the same domain
        global $wpdb;

        if(!empty($companyDomain)) {
            // $sql = "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = 'qbcb_domain' AND meta_value = '{$companyDomain}'";
            $sql = $wpdb->prepare('SELECT p.ID
                FROM ' . $wpdb->postmeta . ' pm
                JOIN ' . $wpdb->posts . ' p ON pm.post_id = p.ID
                WHERE pm.meta_key = \'qbcb_domain\' AND pm.meta_value = %s AND p.post_type = \'publish\'',
                $companyDomain
            );
        } else {
            $sql = "SELECT ID FROM {$wpdb->prefix}posts WHERE post_title = '{$companyName}' AND post_type = 'company'";
        }

        $companyID = $wpdb->get_var($sql);
        $companyCreated = false;

        if(empty($companyID)) {
            // Create Company within Wordpress
            $companyID = wp_insert_post([
                'post_title' => $companyName,
                'post_type' => 'company',
                'post_status' => 'publish',
            ]);

            $companyCreated = true;
        }

        foreach(["country","address_1", "address_2", "city", "state", "postcode", "phone", "company"] as $field){
            if($companyCreated) {
                update_post_meta($companyID, 'qbcb_billing_' . $field, get_user_meta($customer_id, 'billing_' . $field, true));
                update_post_meta($companyID, 'qbcb_shipping_' . $field, get_user_meta($customer_id, 'shipping_' . $field, true));
            }else{
                update_user_meta($customer_id, 'billing_' . $field, get_post_meta($companyID, 'qbcb_billing_' . $field, true));
            }
        }

        if($companyCreated) {
            update_post_meta($companyID, 'qbcb_domain', $companyDomain);
            update_post_meta($companyID, 'qbcb_groups', '7');
        }

        $user->add_cap('wcb2brp_view_users');
        $user->add_cap('wcb2brp_add_user');
        $user->add_cap('wcb2brp_edit_user');
        $user->add_cap('wcb2brp_set_rules');
        $user->add_cap('wcb2brp_delete_user');
        $user->add_role('customer');

        update_user_meta( $customer_id, 'wcb2brp_company', $companyID );
        update_user_meta( $customer_id, 'wcb2brp_company_user_role', 'company_admin');
        update_user_meta( $customer_id, 'wcb2brp_company_owner', 'on');
        update_user_meta( $customer_id, 'wcb2brp_company_user_status', 'active');

    }

    public static function add_telephone_prefix($user_id): void
    {
        $billing_phone = get_user_meta($user_id, "billing_phone", true);

        if (!empty($billing_phone)) {

            // If phone number starts with 0, replace with +44
            if (str_starts_with($billing_phone, "0")) {
                $phone = "+44" . substr($billing_phone, 1);
                update_user_meta($user_id, "billing_phone", $phone);
            }

        }

        $shipping_phone = get_user_meta($user_id, "shipping_phone", true);

        if (!empty($shipping_phone)) {

            // If phone number starts with 0, replace with +44
            if (str_starts_with($shipping_phone, "0")) {
                $phone = "+44" . substr($shipping_phone, 1);
                update_user_meta($user_id, "shipping_phone", $phone);
            }
        }

        // Ensure country is set to GB
        update_user_meta($user_id, "billing_country", "GB");
        update_user_meta($user_id, "shipping_country", "GB");
    }

    public static function bronze_group( int $user_id ): void
    {
        $registered_group = Groups_Group::read_by_name( "Bronze" );

        if ( !$registered_group ) {
            return;
        }
        else {
            $registered_group_id = $registered_group->group_id;
        }

        if ( $registered_group_id )
        {
            if ( !is_multisite() || is_user_member_of_blog( $user_id ) )
            {
                Groups_User_Group::create(
                    array(
                        'user_id'  => $user_id,
                        'group_id' => $registered_group_id
                    )
                );
            }
        }
    }

    public function disable_existing_company_registrations( string $username, string $email, WP_Error $errors ): void
    {

        $email_domain = substr($email, strrpos($email, '@') + 1);

        if (in_array($email_domain, self::get_public_domains())) {
            return;
        }

        // Check for match company domains
        global $wpdb;
        // $sql = "SELECT COUNT(*) FROM {$wpdb->postmeta} WHERE meta_key = 'qbcb_domain' AND meta_value = %s";
        // $sql = $wpdb->prepare($sql, $email_domain);

        $sql = $wpdb->prepare('SELECT COUNT(*)
            FROM ' . $wpdb->postmeta . ' pm
            JOIN ' . $wpdb->posts . ' p ON pm.post_id = p.ID
            WHERE pm.meta_key = \'qbcb_domain\' AND pm.meta_value = %s AND p.post_type = \'publish\'',
            $email_domain
        );


        if ( $wpdb->get_var($sql) > 0 ) {
            $errors->add( 'email_error', "Your company is already set up with a Quickbox account. To be set up to order on your account, please contact us on 01472 868047 or <a href='mailto:sales@quickbox.co'>sales@quickbox.co</a>.  Many thanks!");
        }

    }

    public function copy_company_details_to_user(): void
    {

        // Check user was created
        $user = get_user_by("email", $_POST["wcb2brp_email"]);

        if(!$user) {
            return;
        }

        $companyID = get_user_meta($user->ID, "wcb2brp_company", true);

        if(!$companyID) {
            return;
        }

        foreach(["country","address_1", "address_2", "city", "state", "postcode", "phone", "company"] as $field){
            update_user_meta($user->ID, 'billing_' . $field, get_post_meta($companyID, 'qbcb_billing_' . $field, true));
            update_user_meta($user->ID, 'shipping_' . $field, get_post_meta($companyID, 'qbcb_shipping_' . $field, true));
        }

        update_user_meta($user->ID, 'billing_email', $_POST["wcb2brp_email"]);
        update_user_meta($user->ID, 'shipping_email', $_POST["wcb2brp_email"]);
        update_user_meta($user->ID, 'billing_first_name', $_POST["wcb2brp_first_name"]);
        update_user_meta($user->ID, 'billing_last_name', $_POST["wcb2brp_last_name"]);
        update_user_meta($user->ID, 'shipping_first_name', $_POST["wcb2brp_first_name"]);
        update_user_meta($user->ID, 'shipping_last_name', $_POST["wcb2brp_last_name"]);

        update_user_meta($user->ID, 'billing_company', get_the_title($companyID));
        update_user_meta($user->ID, 'shipping_company', get_the_title($companyID));
        
        // Convert 07XXX numbers to +44 7XXX
        self::add_telephone_prefix($user->ID);
    }

    public function __construct()
    {
        add_action("profile_update", [self::class, "add_telephone_prefix"], 10, 1);
        add_action("user_register", [self::class, "add_telephone_prefix"], 10, 1);

        add_action("woocommerce_register_post", [$this, "disable_existing_company_registrations"], 10, 3);

        add_action("wcb2brp_add_user_action", [$this, "copy_company_details_to_user"], 10, 1);


        if (in_array('groups/groups.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            include_once dirname(__DIR__) . '/admin/group_margins.php';

            // Stop users from going into the 'registered' group
            remove_action( "user_register", array("Groups_Registered", "user_register"), 10 );

            // Automatically put new users in the "Bronze" group
            add_action("user_register", [self::class, "bronze_group"], 10, 1);
        }
    }

}