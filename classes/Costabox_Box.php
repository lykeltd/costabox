<?php

require_once __DIR__ . '/Costabox_Board.php';
require_once dirname(__DIR__) . '/Matex/Evaluator.php';

class Costabox_Box implements JsonSerializable
{
    private int $boxTypeId;
    private string $boxCode = '';
    private string $customerReference = '';
    private string $boxDescription = '';
    private int $fluteId;
    private int $originalFluteId; // In case the flute passed in equivalent to a different flute
    private int $gradeId;

    private int $quantity;
    private int $height;
    private int $width;
    private int $length;
    private int $qtyPerBlank;

    private array $calculatedBaseDimensions = [
        "width" => 0,
        "length" => 0,
    ];
    private array $calculatedLidDimensions = [
        "width" => 0,
        "length" => 0,
    ];
    private array $boxAllowances = [
        "width" => 0,
        "length" => 0,
    ];
    private array $lidAllowances = [
        "width" => 0,
        "length" => 0,
    ];
    private ?int $boardDepth = null;

    private bool $isPrinted = false;
    private bool $hasHandholes = false;
    private bool $useStock = false;
    private bool $shortLeadTime = false;
    private bool $hasLid = false;

    private ?WP_User $user = null;
    private int $weight = 0;
    private ?array $supplierPrices = null;
    private ?array $pricingArray = null;
    private ?string $supplierName = null;
    private ?string $supplierReference = null;
    private ?int $supplierBoxId = null;
    private string $supplierFile = '';

    private ?int $artworkId = null;
    private string $artworkFile = '';

    private ?int $dieFileId = null;
    private string $dieFile = '';

    private ?int $chop = null;
    private ?int $deckle = null;
    private ?int $qtyUp = null;
    private bool $isDieCut = false;

    private float $printingCost = 0;
    private float $handholeCost = 0;

    private Costabox_Board $board;

    private float $totalArea = 0;
    private float $unitArea = 0;
    private float $unitLidArea = 0;
    private float $totalLidArea = 0;
    private array $sheetsRequired = [
        'base' => 0,
        'lid' => 0,
    ];

    private float $labourTime = 0;
    private float $labourRate = 0;
    private float $boardCost = 0;
    private float $labourCost = 0;
    private int $MOQ = 0;
    private string $RSC = '';
    private ?float $overridenTotalPrice = null;

    private ?float $margin = null;

    private bool $cloned = false;
    private int $quoteId = 0;

    public function __clone(): void
    {
        // Set a flag so that we know to try and prevent recursion
        $this->cloned = true;
    }

    public function __construct(string $boxTypeId, string $fluteId, int $gradeId, int $quantity, int $height, int $width, int $length, string $customerReference = "")
    {
        $this->customerReference = $customerReference;

        if (!is_numeric($boxTypeId) || str_starts_with($boxTypeId, "0")) {
            $this->boxTypeId = $this->getBoxTypeId($boxTypeId);
        } else {
            $this->boxTypeId = (int)$boxTypeId;
        }

        if (function_exists("costabox_get_box_code")) {
            $this->boxCode = costabox_get_box_code($this->boxTypeId);

        }

        if (!is_numeric($fluteId)) {
            $this->fluteId = Costabox_Board::getFluteId($fluteId);
        } else {
            $this->fluteId = (int)$fluteId;
        }

        $this->originalFluteId = $this->fluteId;
        $this->fluteId = Costabox_Board::getFluteEquivalent($this->fluteId);

        if ($gradeId > 100) {
            $this->gradeId = Costabox_Board::getGradeId($gradeId);
        } else {
            $this->gradeId = $gradeId;
        }

        $this->quantity = $quantity;
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        $this->labourRate = function_exists("costabox_get_setting") ? costabox_get_setting("labour_cost") : 0;

        $this->board = new Costabox_Board($this->fluteId, $this->gradeId);
    }

    public static function fromJSON( array $json ): self
    {
        $box = new self($json['boxTypeId'], $json['fluteId'], $json['gradeId'], $json['quantity'], $json['height'], $json['width'], $json['length'], $json['customerReference'] ?? "");

        $box->importJSON($json);

        return $box;

    }

    public function importJSON( array $json ): void
    {
        foreach ($json as $key => $value) {
            if (property_exists($this, $key) && !in_array($key, ["user", "board"])) {
                $this->$key = $value;
            }
        }

        if(is_array($json['user'])){
            $this->setCustomer($json['user']['data']['ID']);
        }

        $this->calculate();
    }

    private function getBoxTypeId( string $boxCode ): int
    {
        global $wpdb;
        $table = $wpdb->prefix . 'qbcb_box_type';

        $boxTypeId = $wpdb->get_var($wpdb->prepare("SELECT box_type_id FROM {$table} WHERE code = %s", $boxCode));


        return $boxTypeId ?? 0;
    }

    public function calculate(): void
    {
        $this->pricingArray = null;

        $this->calculateAreaRequired();
        $this->calculateLabourTime();
        $this->calculateWeight();

        $this->labourCost = $this->labourTime/3600 * $this->labourRate;

        $this->boardCost = $this->board->getPrice($this->totalArea, $this->useStock);

        $this->calculateHandholeCosts();
        $this->calculatePrintingCosts();

        $this->calculateMOQ();
    }

    public function getPricingArray( ): array
    {

        if($this->cloned){
            return [];
        }

        $userId = $this->user ? $this->user->ID : 0;

        if($this->useStock){
            return [1 => $this->boardCost];
        }

        if($this->supplierPrices){

            $supplierPrices = $this->supplierPrices;

            foreach($supplierPrices as $qty => &$break){

                if(!$break){
                    unset($supplierPrices[$qty]);
                    continue;
                }

                $margin = costabox_get_profit_margin($this->totalArea, $userId, $this->useStock);

                $break = round($break * $margin, 2);

            }

            return $supplierPrices;

        }

        $previousPrice = 0;

        $priceBreaks = [];
        foreach($this->board->getBreaks() as $break){

            if($previousPrice == $break["price"]){
                continue;
            }else{
                $previousPrice = $break["price"];
            }

            if($break["min_qty"] == 0){
                $break["min_qty"] = 200;
            }

            $divide_area = $this->unitArea - ($break['min_qty'] <= 200 ? $this->unitLidArea : 0);

            // Determine the number of boxes required for this price
            $boxes = ceil($break['min_qty'] / $divide_area);

            $box = clone $this;
            $box->setQuantity($boxes);
            $box->calculate();

            $priceBreaks[$boxes] = $box->getTotalPrice(true);

        }

        $this->pricingArray = $priceBreaks;

        return $priceBreaks;
    }

    public function overridePrice(float $price): void
    {
        // Only allow the box to be free if the quantity is 1, as this is a sample
        if($price > 0 || $this->quantity == 1) {
            $this->overridenTotalPrice = round($price, 2) * $this->quantity;
        }else{
            $this->overridenTotalPrice = null;
        }
    }

    public function getTotalPrice( bool $inPricingArray = false ): float
    {

        if(!is_null($this->overridenTotalPrice) && !$inPricingArray){
            return $this->overridenTotalPrice;
        }
        $userId = $this->user ? $this->user->ID : 0;

        $this->margin = $margin = costabox_get_profit_margin($this->totalArea, $userId, $this->useStock);

        if($this->supplierPrices){

            // Find the best supplier price
            $bestPrice = 99999;

            foreach($this->supplierPrices as $qty => $price){
                $qty = intval($qty);

                if($qty == 0 || $qty > $this->quantity || !is_numeric($price)){
                    continue;
                }

                $thisPrice = round((floatval($price) / $qty) * $this->quantity, 2);

                if(($thisPrice / $qty) < $bestPrice){
                    $bestPrice = $thisPrice;
                }
            }

            return round($bestPrice / $this->quantity, 2) * $this->quantity * $margin;
        }

        $total = round(($this->labourCost + $this->boardCost) * $margin, 2);

        // Add a minimum order fee, if applicable
        $min_order_value = floatval(costabox_get_setting("minimum_order_amount"));

        if(($this->totalArea - $this->totalLidArea) < $min_order_value){
            $total += costabox_get_setting("minimum_order_fee");
        }else{
            if($this->useStock){
                // If using stock board, the lowest price a unit should be is the price for 200m2
                $total += (costabox_get_setting("minimum_order_fee") / (ceil($min_order_value  / $this->unitArea)-1)) * $this->quantity;
            }
        }


        $total += ($this->printingCost) * $this->quantity;
        $total += ($this->handholeCost);

        // To avoid rounding errors, get a unit price and multiply by quantity
        $unit_price = $total / $this->quantity;

        $unit_price = round($unit_price, 2) * $this->quantity;


        $pricingArray = $this->getPricingArray();

        if(!$inPricingArray && !$this->useStock && count($pricingArray) > 0){

            $keys = array_keys($pricingArray);
            $key = $keys[min(2, count($keys)-1)]; // Get the 3rd highest price

            $maxPrice = $pricingArray[$key] / $key; // Get the 3rd highest price

            if($this->quantity > $key && ($maxPrice <= $unit_price)){
                $unit_price = $maxPrice * $this->quantity;
            }

        }

        return $unit_price;

    }

    public function calculateMOQ(): void
    {
        if($this->supplierPrices){
            $this->MOQ = array_key_first($this->supplierPrices);
            return;
        }

        $fluteMOQ = $originalMOQ = $this->board->getFluteMoq($this->useStock);
        $unitArea = (($this->totalArea - $this->totalLidArea) / $this->quantity);

        // Check to see if the stock board has a lower MOQ
        if($this->useStock){
            $stockMOQ = $this->board->getFluteMoq(true);
            if($stockMOQ < $fluteMOQ){
                $fluteMOQ = $stockMOQ;
            }
        }

        if($this->quantity < ceil($originalMOQ / $unitArea)){
            $this->shortLeadTime = true;
        }

        if($fluteMOQ < 200 && $this->qtyPerBlank < 1){
            $fluteMOQ = 200;
        }

        if($fluteMOQ > 0){
            $this->MOQ = ceil($fluteMOQ / $unitArea);
        }


    }

    private function calculatePrintingCosts(): void
    {
        if(!$this->isPrinted){
            return;
        }

        $cost = costabox_get_setting("printing_cost") ?? 0;

        $this->printingCost = ($this->chop ?? 0) * ($cost / 100);
    }

    private function calculateHandholeCosts(): void
    {
        if(!$this->hasHandholes){
            return;
        }

        $cost = costabox_get_setting("handhole_cost") ?? 0;

        $this->handholeCost = $cost * $this->quantity;
    }

    public function attachArtwork(array $file, bool $dieCut = false): void
    {
        $currentFile = $dieCut ? $this->dieFile : $this->artworkFile;

        if($file['name'] == $currentFile){
            return;
        }

        $uploadPath = dirname(ABSPATH) . DIRECTORY_SEPARATOR . "qbcb_artwork";
        $fileName = time() . $file["name"];

        if($dieCut){
            $this->dieFile = $file['name'];
        }else{
            $this->artworkFile = $file['name'];
        }
        move_uploaded_file($file["tmp_name"], $uploadPath . DIRECTORY_SEPARATOR . $fileName);

        global $wpdb;

        $wpdb->insert("{$wpdb->prefix}qbcb_artwork", [
            "customer" => $this->user->ID,
            "file" => $fileName,
        ]);

        if($dieCut){
            $this->dieFileId = $wpdb->insert_id;
        }else {
            $this->artworkId = $wpdb->insert_id;
        }
    }
    public function setSupplierPrices(array $supplierPrices, string $name, string $reference, ?array $file): void
    {
        $this->supplierPrices = array_filter($supplierPrices, function($value, $key) {
            return (bool)$key;
        }, ARRAY_FILTER_USE_BOTH);

        if(!$file){
            return;
        }

        // Upload the file
        if($file['name'] != $this->supplierFile) {
            $uploadPath = qbcb_supplier_quotes_upload_path();
            $fileName = time() . $file["name"];

            $this->supplierFile = $file['name'];

            move_uploaded_file($file["tmp_name"], $uploadPath . DIRECTORY_SEPARATOR . $fileName);

            global $wpdb;

            $wpdb->insert("{$wpdb->prefix}qbcb_purchased_box", [
                "supplier" => $name,
                "reference" => $reference,
                "rsc" => $this->getRSC(),
                "file" => $fileName,
            ]);

            $this->supplierBoxId = $wpdb->insert_id;
        }

        $this->supplierName = $name;
        $this->supplierReference = $reference;

        // TODO: Add download link to meta

        foreach($supplierPrices as $qty => $price){
            $wpdb->insert("{$wpdb->prefix}qbcb_purchased_box_prices", [
                "purchased_box_id" => $this->supplierBoxId,
                "qty" => $qty,
                "price" => $price,
            ]);
        }

    }

    public function setDiecutDimensions(int $chop, int $deckle, int $qtyUp): void
    {
        $this->chop = $chop + 50;
        $this->deckle = $deckle + 50;
        $this->qtyUp = $qtyUp;
        $this->isDieCut = true;
    }

    public function setBoxDescription(string $description): void
    {
        $this->boxDescription = $description;
    }

    public function useStock(bool $useStock = true): void
    {
        $this->useStock = $useStock;
        $this->shortLeadTime = $useStock;
    }

    public function isStock(): bool
    {
        return $this->useStock;
    }

    public function setCustomer( int $userId ): void
    {
        $this->user = get_user_by( 'ID', $userId ) ?: null;
;
    }

    public function setPrinted(bool $isPrinted): void
    {
        $this->isPrinted = $isPrinted;
    }

    public function setHandholes(bool $hasHandholes): void
    {
        $this->hasHandholes = $hasHandholes;
    }

    public function calculateAreaRequired(): void
    {
        if($this->isDieCut){
            $chop = $this->chop;
            $deckle = $this->deckle;

            $this->calculatedBaseDimensions = [
                'width' => $deckle,
                'length' => $chop,
            ];

            $this->calculatedLidDimensions = [
                'width' => 0,
                'length' => 0,
            ];

            // Convert to meters
            $chop /= 1000;
            $deckle /= 1000;

            $this->totalArea = ($chop * $deckle) * ($this->quantity / $this->qtyUp);
            $this->unitArea = ($chop * $deckle) / $this->qtyUp;

        }else{
            // If the box isn't diecut...

            $this->getAllowances();

            $boxWidth = $this->getFormula('width');
            $boxLength = $this->getFormula('length');

            $lidWidth = $this->getFormula('width', 'lid');
            $lidLength = $this->getFormula('length', 'lid');

            $this->hasLid = !(empty($lidWidth) || empty($lidLength));

            // Evaluate the formulas
            $matex = new \Matex\Evaluator();

            $this->calculatedBaseDimensions = [
                'width' => ceil($matex->execute($boxWidth) + ($this->boxCode == '0401' ? 0 : $this->boxAllowances['width'])),
                'length' => ceil($matex->execute($boxLength) + $this->boxAllowances['length']),
            ];

            $this->chop = $this->calculatedBaseDimensions['length'];
            $this->deckle = $this->calculatedBaseDimensions['width'];

            if($this->hasLid){
                $this->calculatedLidDimensions = [
                    'width' => $matex->execute($lidWidth) + $this->lidAllowances['width'],
                    'length' => $matex->execute($lidLength) + $this->lidAllowances['length'],
                ];
            }

            $this->unitArea = ((ceil($this->calculatedBaseDimensions['width'])/1000)
                * (ceil($this->calculatedBaseDimensions['length'])/1000))
                + ((ceil($this->calculatedLidDimensions['width'])/1000) * (ceil($this->calculatedLidDimensions['length'])/1000));

            $this->totalArea = $this->unitArea * $this->quantity;

            $this->unitLidArea = (ceil($this->calculatedLidDimensions['width'])/1000) * (ceil($this->calculatedLidDimensions['length'])/1000);
            $this->totalLidArea = $this->unitLidArea * $this->quantity;
        }

        $this->qtyPerBlank = $this->board->getQtyOut($this->calculatedBaseDimensions['length'], $this->calculatedBaseDimensions['width'], stock: $this->useStock);

        $this->sheetsRequired['base'] = ($this->qtyPerBlank == 0) ? $this->quantity * 2 : ceil($this->quantity / $this->qtyPerBlank);

        if($this->totalLidArea > 0){
            $perBlank = $this->board->getQtyOut($this->calculatedLidDimensions['length'], $this->calculatedLidDimensions['width'], stock: $this->useStock);

            if($perBlank == 0){
                $this->sheetsRequired['lid'] = $this->quantity * 2;
            }else{
                $this->sheetsRequired['lid'] = ceil($this->quantity / $perBlank);
            }
        }

        if(!$this->useStock && $this->totalArea >= 200){
            $this->qtyPerBlank = 1;
        }
    }

    private function calculateWeight(): void
    {
        $this->weight = $this->board->getWeight(true) * $this->totalArea;
    }

    private function getFormula( string $dimension, string $component = 'box', $replaceValues = true ): string
    {

        global $wpdb;
        $table = $wpdb->prefix . "qbcb_" . $dimension . "_calculations";

        $sql = $wpdb->prepare("SELECT formula FROM {$table} WHERE component = %s AND box_type_id = %d", $component, $this->boxTypeId);

        $formula = $wpdb->get_var($sql);
        $wpdb->flush();

        if(is_null($formula)){
            return "";
        }

        return $replaceValues ? $this->replaceFormulaValues($formula): $formula;

    }

    private function getAllowances(): void
    {

        if(str_contains($this->boxCode, "PAD")){
            return; // This method falls back on a default allowance if one isn't defined, so prevent this from happening
        }

        if(str_contains($this->boxCode, "0320")){
            // Because 0320 boxes are different, we're going to hardcode the allowances as the feature to change them per flute group isn't otherwise required
            $flute = $this->getBoardDetails()["flute"];

            if(str_ends_with($flute, "BC")){
                $this->boxAllowances = [
                    'width' => 10,
                    'length' => 76,
                ];

                $this->lidAllowances = [
                    'width' => 39,
                    'length' => 140,
                ];

                return;
            }

            $this->boxAllowances = [
                'width' => 5,
                'length' => 57,
            ];

            $this->lidAllowances = [
                'width' => 16,
                'length' => 81,
            ];

            return;
        }

        global $wpdb;
        $table = $wpdb->prefix . "qbcb_allowances";

        $sql = $wpdb->prepare("SELECT value FROM {$table} WHERE (box_type_ids IS NULL OR box_type_ids LIKE %s) AND flute_id = %d AND component = %s AND dimension = %s ORDER BY box_type_ids DESC",
            "%{$this->boxTypeId}%",
            $this->fluteId,
            'box',
            'length'
        );

        $allowance = $wpdb->get_var($sql);

        $allowances = [
            ['component' => 'box', 'dimension' => 'length', 'value' => $allowance],
            ['component' => 'box', 'dimension' => 'width', 'value' => $allowance],
            ['component' => 'lid', 'dimension' => 'length', 'value' => $allowance],
            ['component' => 'lid', 'dimension' => 'width', 'value' => $allowance],
        ];

        foreach ($allowances as $allowance){
            $allowance['component'] = strtolower($allowance['component']);
            $allowance['dimension'] = strtolower($allowance['dimension']);

            if(str_ends_with($allowance['component'], "_multiply")) continue;

            $multiplier = $this->getFormula($allowance['dimension'], $allowance['component'] . "_multiply");

            $multiplier = is_numeric($multiplier) ? $multiplier : 1;

            if($allowance['component'] == 'box') {
                $this->boxAllowances[$allowance['dimension']] = $allowance['value'] * $multiplier;
            }else{
                $this->lidAllowances[$allowance['dimension']] = $allowance['value'] * $multiplier;
            }

        }

    }

    private function replaceFormulaValues(string $formula ): string
    {

        $formula = str_replace("[height]", $this->height, $formula);
        $formula = str_replace("[length]", $this->length, $formula);
        $formula = str_replace("[width]", $this->width, $formula);
        $formula = str_replace("[breadth]", $this->width, $formula);

        $formula = str_replace("[allowance_w]", $this->boxAllowances['width'], $formula);
        $formula = str_replace("[allowance_l]", $this->boxAllowances['length'], $formula);

        return $formula;
    }

    public function calculateLabourTime(): void
    {
        $out = $this->qtyPerBlank;

        // Determine which time to use based on how many boxes/lids can be created per blank
        $qty = 1;
        if($out == 1 || $out == 2) $qty = 2;
        if($out > 2) $qty = 3;

        global $wpdb;
        $table = $wpdb->prefix . "qbcb_box_type_times";

        $sql = "SELECT stage, seconds, qty FROM {$table} WHERE box_type_id = %d";

        $results = $wpdb->get_results($wpdb->prepare($sql, $this->boxTypeId), "ARRAY_A");

        $total_time = 0;

        foreach($results as $t){
            if($t['qty'] != 0 && $t['qty'] != $qty) continue;

            if(!$this->isPrinted && $t['stage'] == 'print') continue;

            if($this->totalArea >= 200 && !$this->useStock){
                if($t['stage'] == 'slit') continue;

                if($t['stage'] == 'setup') $t['seconds'] = 0;//$t['seconds'] * 0.67 * 60;
            }

            $seconds = $t['seconds'];

            if($t['stage'] != 'setup' && $seconds > 0) $seconds = $this->quantity * ( 60 / $seconds );

            $total_time += $seconds;
        }

        $this->labourTime = $total_time;

    }

    public function setQuantity(float $quantity ): void
    {
        $this->quantity = $quantity;
    }

    public function getAdditonalInformation(): array
    {
        $cost_pm = $this->boardCost / $this->totalArea;

        return [
            "total_cost" => $this->boardCost + $this->labourCost,
            "board_cost" => $this->boardCost,
            "base_cost" => $cost_pm * ($this->totalArea - $this->totalLidArea),
            "lid_cost" => $cost_pm * $this->totalLidArea,
            "quantity" => $this->quantity,
            "labour_time" => $this->labourTime,
            "labour_cost" => $this->labourCost,
            "total_area" => $this->totalArea,
            "rsc" => $this->getRSC(),
            "box_deckle" => $this->deckle,
            "box_chop" => $this->chop,
            "lid_deckle" => $this->calculatedLidDimensions['width'] ?? 0,
            "lid_chop" => $this->calculatedLidDimensions['length'] ?? 0,
            "moq" => $this->MOQ,
            "board_depth" => $this->boardDepth,
            "breaks" => $this->getPricingArray(),
            "box_two_piece" => $this->isBoxTwoPiece(),
            "lid_two_piece" => $this->isLidTwoPiece(),
            "box_sqm" => $this->unitArea - $this->unitLidArea,
            "lid_sqm" => $this->unitLidArea,
            "box_sheets_required" => $this->sheetsRequired['base'],
            "lid_sheets_required" => $this->sheetsRequired['lid'],
            "margin" => $this->margin,
        ];
    }

    public function getRSC(): string {
        if($this->RSC){
            return $this->RSC;
        }

        global $wpdb;

        $sql = "SELECT code FROM {$wpdb->prefix}qbcb_rsc_codes WHERE length = %d AND width = %d AND height = %d AND box_type_id = %d AND grade = %s AND flute_id = %d";
        $sql = $wpdb->prepare($sql, $this->length, $this->width, $this->height, $this->boxTypeId, $this->gradeId, $this->fluteId);

        $code = $wpdb->get_var($sql);

        if (!$code) {

            $existing_code = true;

            while ($existing_code) {
                // Generate random six digit string
                $code = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);

                // Check to see if this code already exists
                $sql = "SELECT code FROM {$wpdb->prefix}qbcb_rsc_codes WHERE code = %s";
                $sql = $wpdb->prepare($sql, $code);

                $existing_code = $wpdb->get_var($sql);
            }


            $box_code = $this->boxCode;
            $box_code = ($box_code == 'DIE-CUT') ? "DC" : $box_code;
            $box_code = ($box_code == "Unknown") ? "" : $box_code;

            // Insert new code into database
            $sql = "INSERT INTO {$wpdb->prefix}qbcb_rsc_codes (code, length, width, height, box_type_id, grade, flute_id) VALUES (%s, %d, %d, %d, %d, %s, %d)";
            $sql = $wpdb->prepare($sql, $box_code . $code, $this->length, $this->width, $this->height, $this->boxTypeId, $this->gradeId, $this->fluteId);

            $wpdb->query($sql);
        }

        if ($this->supplierPrices) {
            $code = $code . 'S';
        }

        if ($this->isPrinted) {
            $code = $code . 'P';
        }

        $this->RSC = (str_starts_with($code, "RSC") ? "" : "RSC") . $code;
        return "RSC" . $code;
    }

    public function getMoq(): int
    {
        if($this->MOQ) {
            return $this->MOQ;
        }

        $this->calculateMOQ();

        return $this->MOQ;
    }

    public function getDepth(): int
    {

        if($this->boardDepth) {
            return $this->boardDepth;
        }

        $flute = $this->board->getDetails()['flute'];

        // B - 3mm
        // C - 5mm
        // BC - 7mm
        // E - 1.2mm
        // EB - 5mm
        if(str_ends_with($flute, 'BC')) {
            $this->boardDepth = 7;
        }elseif (str_ends_with($flute, "EB")){
            $this->boardDepth = 5;
        } elseif(str_ends_with($flute, 'B')){
            $this->boardDepth = 3;
        } else if(str_ends_with($flute, 'C')){
            $this->boardDepth = 5;
        } elseif(str_ends_with($flute, 'E')) {
            $this->boardDepth = 1.5;
        }else{
            $this->boardDepth = 7;
        }

        return $this->boardDepth;

        global $wpdb;

        // Get the depth of the board for the box type
        $depth = $wpdb->get_var("SELECT depth FROM {$wpdb->prefix}qbcb_box_type WHERE box_type_id = {$this->boxTypeId}");

        if (is_null($depth)) {
            $depth = 6;
        }

        // Determine if the box has a lid
        if ($this->unitLidArea > 0) {
            $depth = $depth * 2;
        }

        // Multiply by the number of walls in the flute
        $walls = $this->board->getWalls();

        return $depth * $walls;
    }

    private function isBoxTwoPiece(): bool
    {
        if(in_array($this->boxCode, ["0201", "0200", "0320"])){

            if($this->calculatedBaseDimensions['length'] > 2600 && ($this->unitArea - $this->unitLidArea) < 100){
                return true;
            }

        }

        return false;
    }

    private function isLidTwoPiece(): bool
    {
        if($this->boxCode == "0320"){

            if($this->calculatedLidDimensions['length'] > 2600 && ($this->unitLidArea) < 200){
                return true;
            }

        }

        return false;
    }

    public function hasShortLeadTime(): bool
    {
        return $this->shortLeadTime;
    }

    public function getPrintingCost(): float
    {
        return $this->printingCost;
    }

    public function getHandholeCost(): float
    {
        return $this->handholeCost;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }

    public function getBoxType(): string
    {
        return $this->boxCode;
    }

    public function getBoardDetails( bool $original = false ): array
    {
        return $this->board->getDetails($original ? $this->originalFluteId : 0);
    }

    public function isPrinted(): bool
    {
        return $this->isPrinted;
    }

    public function hasHandholes(): bool
    {
        return $this->hasHandholes;
    }

    public function getDispatchDate( bool $formatted = true ): string
    {

        $lead_time = costabox_get_setting(($this->useStock ? "stock_" : "") .'lead_time');
        if (date('G') > 12) {
            $lead_time += 1;
        } // If ordering after midday

        $time = strtotime($lead_time . ' weekdays');

        return $formatted ? date('d/m/Y', $time) : $time;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getChop(): int
    {
        return $this->chop;
    }

    public function getLidChop(): int
    {
        return $this->calculatedLidDimensions['width'];
    }

    public function getDeckle(): int
    {
        return $this->deckle;
    }

    public function getLidDeckle(): int
    {
        return $this->calculatedLidDimensions['length'];
    }

    public function getCustomerReference(): string
    {
        return $this->customerReference;
    }

    public function getOdooDetails(): array
    {

        $boardDetails = $this->getBoardDetails(true);
        $cost_pm = $this->boardCost / $this->totalArea;

        $details = [
            "_Flute Description" => $boardDetails['description'],
            "_Flute Code" => $boardDetails['flute'],
            "_Unit Price" => $this->getTotalPrice() / $this->quantity,
            "_Board Cost" => $this->boardCost,
            "_Labour Cost" => $this->labourCost,
            "_Labour Time" => $this->labourTime,
            "_Total Area" => $this->totalArea,
            "_RSC" => $this->getRSC(),
            "_Box Chop" => $this->chop,
            "_Box Deckle" => $this->deckle,
            "_Lid Chop" => $this->calculatedLidDimensions['length'],
            "_Lid Deckle" => $this->calculatedLidDimensions['width'],
            "_Weight" => $this->weight,
            "_Short Lead Time" => $this->shortLeadTime ? "Yes" : "No",
            "_Box Sheets Required" => $this->sheetsRequired['base'],
            "_Lid Sheets Required" => $this->sheetsRequired['lid'],
            "_Base Cost" => $cost_pm * ($this->totalArea - $this->totalLidArea),
            "_Lid Area" => $this->totalLidArea,
            "_Lid Cost" => $cost_pm * $this->totalLidArea,
            "_UP Quantity" => $this->qtyUp ?? "",
        ];

        if($this->supplierBoxId){
            $details['_Supplied by'] = $this->supplierBoxId;
        }

        if($this->artworkId){
            $details['Artwork'] = $this->artworkId;
        }

        if($this->dieFileId){
            $details['Die Cut Files'] = $this->dieFileId;
        }

        return $details;

    }

    public function getFormValues(): array
    {
        return [
            "customer_id" => $this->user ? $this->user->ID : 0,
            "box_type_id" => $this->boxTypeId,
            "box_colour" => $this->getBoardDetails()['colour'],
            "box_flute" => $this->fluteId,
            "flute_edit" => true,
            "qty" => $this->quantity,
            "length" => $this->length,
            "width" => $this->width,
            "height" => $this->height,
            "box_ref" => $this->customerReference,
            "plate_code" => "",
            "forme_code" => "",
            "box_printing" => $this->isPrinted,
            "handhole_box" => $this->hasHandholes,
            "die_cut_chop" => $this->isDieCut ? ($this->chop - 50) : 0,
            "die_cut_deckle" => $this->isDieCut ? ($this->deckle - 50) : 0,
            "die_cut_up" => $this->isDieCut ? $this->qtyUp : 0,
        ];
    }

    public function saveQuote(): int
    {
        global $wpdb;
        $quotes_table = $wpdb->prefix . "qbcb_quotes";

        if($this->quoteId == 0) {
            $wpdb->insert($quotes_table, [
                "customer_id" => $this->user ? $this->user->ID : 0,
                "user_id" => get_current_user_id(),
                "date_produced" => date('Y-m-d H:i:s'),
                "quoted_price" => $this->getTotalPrice(),
                "box_type_id" => $this->getBoxTypeId($this->boxCode),
                "qty" => $this->quantity,
                "ref" => $this->customerReference,
                "object" => serialize($this),
            ]);
        }else{
            $wpdb->update($quotes_table, [
                "customer_id" => $this->user ? $this->user->ID : 0,
                "user_id" => get_current_user_id(),
                "date_produced" => date('Y-m-d H:i:s'),
                "quoted_price" => $this->getTotalPrice(),
                "box_type_id" => $this->getBoxTypeId($this->boxCode),
                "qty" => $this->quantity,
                "ref" => $this->customerReference,
                "object" => serialize($this),
            ], [
                "quote_id" => $this->quoteId
            ]);
        }

        $this->quoteId = $wpdb->insert_id;

        return $this->quoteId;

    }

    public static function loadFromQuote( int $quoteId ): ?Costabox_Box
    {
        global $wpdb;
        $quotes_table = $wpdb->prefix . "qbcb_quotes";

        $quote = $wpdb->get_row("SELECT * FROM {$quotes_table} WHERE quote_id = {$quoteId}");

        if(!$quote){
            return null;
        }

        if($quote->object) {
            $quote = unserialize($quote->object);

            return $quote ?: null;

        }else {
            // If no object has been saved, create a new one based on the old data
            global $wpdb;
            $attributes_table = $wpdb->prefix . "qbcb_quote_attributes";
            $quotes_table = $wpdb->prefix . "qbcb_quotes";


            $sql = "SELECT customer_id, box_type_id, qty, ref FROM {$quotes_table} WHERE quote_id = {$quoteId}";
            $quote = $wpdb->get_row($sql, "ARRAY_A");

            if (!$quote) {
                return null;
            }

            $sql = "SELECT attribute, value FROM {$attributes_table} WHERE quote_id = {$quoteId}";
            $attributes = $wpdb->get_results($sql, "ARRAY_A");

            if (!$attributes) {
                return null;
            }

            $quote_attributes = [];
            foreach ($attributes as $attribute) {
                $quote_attributes[$attribute['attribute']] = $attribute['value'];
            }

            $box = new self($quote['box_type_id'], $quote_attributes['flute'], $quote_attributes['grade'], $quote['qty'], $quote_attributes['height'], $quote_attributes['width'], $quote_attributes['length'], $quote['ref']);
            $box->setCustomer($quote['customer_id']);
            $box->calculate();

            return $box;
        }


    }

    public static function getArtworkDownloadLink(int $artworkId): string
    {
        global $wpdb;

        // Get the name of the file
        $sql = "SELECT file, customer FROM {$wpdb->prefix}qbcb_artwork WHERE artwork_id = {$artworkId}";

        $file = $wpdb->get_row($sql);

        if (empty($file)) {
            return 'Not uploaded';
        }

        $user = Costabox_User::get_user();

        if (!Costabox_User::is_staff(get_current_user_id()) && (!$user || $user->ID != $file->customer)) {
            return 'Uploaded';
        }

        return "<a href='" . home_url('?artwork_download=' . $artworkId) . "'>" . substr($file->file, 10) . '</a>';
    }

    public static function getArtworkFilePath(int $artworkId): string
    {
        global $wpdb;

        // Get the name of the file
        $sql = "SELECT file, customer FROM {$wpdb->prefix}qbcb_artwork WHERE artwork_id = {$artworkId}";

        $file = $wpdb->get_row($sql);

        if (empty($file)) {
            return 'Not uploaded';
        }

        $uploadPath = dirname(ABSPATH) . DIRECTORY_SEPARATOR . "qbcb_artwork";

        return $uploadPath . DIRECTORY_SEPARATOR . $file->file;
    }

    public static function getSupplierDetails(int $supplierId): array
    {
        global $wpdb;

        $sql = "SELECT supplier, reference, file FROM {$wpdb->prefix}qbcb_purchased_box WHERE purchased_box_id = {$supplierId}";

        return $wpdb->get_row($sql, "ARRAY_A") ?? [];
    }

    public static function getSupplierFile(int $supplierId): string
    {
        global $wpdb;

        $sql = "SELECT file FROM {$wpdb->prefix}qbcb_purchased_box WHERE purchased_box_id = {$supplierId}";

        $details = $wpdb->get_row($sql);

        if (empty($details)) {
            return 'No file found';
        }

        $file_name = $details->file;

        return qbcb_supplier_quotes_upload_path() . DIRECTORY_SEPARATOR . $file_name;
    }

    public static function getAllTypes( bool $includeMeda = false ): array {
        global $wpdb;

        $sql = "SELECT box_type_id, code, name, image FROM {$wpdb->prefix}qbcb_box_type";
        $results = $wpdb->get_results($sql, "ARRAY_A");
        $options = [];

        foreach($results as $result){
            if($includeMeda) {
                $options[$result['box_type_id']] = $result;
            }else{
                $options[$result['box_type_id']] = $result['code'] . " " . $result['name'];
            }
        }

        return $options;
    }

    public function getBoxDescription( bool $includeRSC ): string
    {

        $boardDetails = $this->getBoardDetails();

        return ($includeRSC ? $this->getRSC() . ": " : "") .
            $this->boxCode . " " .
            "(" . ($this->isPrinted ? "Printed" : "Plain") . ") " .
            ($this->hasHandholes ? "(Handholes) " : "") .
            $this->length . " x " . $this->width . " x " . $this->height . " " .
            $boardDetails['grade'] . " " . $boardDetails['flute'] . (!empty($this->customerReference) ? " (" . $this->customerReference . ")" : "");

    }

    public function setCustomerReference(mixed $boxReference): void
    {
        $this->customerReference = $boxReference;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function isDieCut(): bool
    {
        return $this->isDieCut;
    }
}