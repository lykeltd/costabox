<?php

class Costabox_Board
{

    private int $fluteId;
    private int $gradeId;

    private array $prices = [];

    public function __construct(int $fluteId, int $gradeId)
    {
        $this->fluteId = $fluteId;
        $this->gradeId = $gradeId;

        $this->refreshPrices();
    }

    public function refreshPrices(): void
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_board_prices";

        $stockPrices = [];
        $nonStockPrices = [];

        $sql = "SELECT min_qty, max_qty, price, stock FROM $table WHERE flute_id = %d AND grade_id = %d ORDER BY price ASC";
        $results = $wpdb->get_results($wpdb->prepare($sql, $this->fluteId, $this->gradeId), ARRAY_A);

        foreach ($results as $result){
            if($result['price'] > 5){
                $result['price'] /= 100;
            }

            if($result['stock'] == 1){
                $stockPrices[] = $result;
            }else{
                $nonStockPrices[] = $result;
            }
        }

        $this->prices = [
            'stock' => $stockPrices,
            'non-stock' => $nonStockPrices
        ];
    }

    public function getPrice(float $area, bool $stock): float
    {
        $area= round($area, 0);

        $prices = $this->prices[$stock ? 'stock' : 'non-stock'];

        foreach ($prices as $price){
            if((is_null($price['min_qty']) || $area >= $price['min_qty']) && $area <= $price['max_qty']){
                return $price['price'] * $area;
            }
        }

        // If we're here, no price was found so look for stock board
        if(!$stock){
            return $this->getPrice($area, true);
        }

        if(!is_array($prices) || count($prices) == 0) return 0;

        // If there is no stock board price, return the highest non-stock price
        return $prices[count($prices) - 1]['price'] * $area;

    }

    public function getQtyOut(float $length, float $width, ?float $blank_length = null, ?float $blank_width = null, bool $stock = false): int
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_board_lengths";

        $sql = "SELECT length FROM {$table} WHERE flute_id = {$this->fluteId}";
        $board_length = $wpdb->get_var($sql);

        if(is_null($blank_length) && !is_null($board_length)) $blank_length = $board_length;

        // Get the dimensions of the blank
        if(is_null($blank_length)) $blank_length = 2600;
        if(is_null($blank_width)) $blank_width = 2000;


        if($stock) {
            $blank_length = costabox_get_setting('stock_board_chop');
            $blank_width = costabox_get_setting('stock_board_deckle');
        }

        if($length < 1 || $width < 1) return 0;

        $length_ways = $blank_length / $length;
        if($length_ways < 1){
            $length_ways = ($length_ways < 0.5) ? 0 : 0.5;
        }else{
            $length_ways = floor($length_ways);
        }

        $width_ways = $blank_width / $width;

        if($width_ways < 1){
            $width_ways = ($width_ways < 0.5) ? 0 : 0.5;
        }else{
            $width_ways = floor($width_ways);
        }

        $total = $width_ways * $length_ways;

        return $total == 0 ? 0.5 : $total;
    }

    public function getWeight(bool $finished = false): float
    {
        global $wpdb;
        $table  = $wpdb->prefix . 'qbcb_board_weight';
        $column = $finished ? 'finished_weight' : 'board_weight';

        $sql = "SELECT {$column} FROM {$table} WHERE flute_id = {$this->fluteId} AND grade_id = {$this->gradeId}";

        $weight = $wpdb->get_var($sql);

        return is_null($weight) ? 0 : $weight;
    }

    public function getFluteMoq(bool $stock = false): int
    {
        global $wpdb;

        $table = $wpdb->prefix . 'qbcb_flutes';

        $sql = "SELECT moq FROM {$table} WHERE flute_id = {$this->fluteId}";

        $result = $wpdb->get_var($sql);

        if (is_null($result) || $result < 1) {
            $result = 0 ;
        }

        // If a board grade has been specified, get the absolute minimum qty a price has been set for
        $table = $wpdb->prefix . "qbcb_board_prices";

        $sql = "SELECT MIN(IFNULL(min_qty, 0)) FROM {$table} WHERE stock = %d AND flute_id = %d AND grade_id = %d";
        $sql = $wpdb->prepare($sql, $stock ? 1 : 0, $this->fluteId, $this->gradeId);

        $min_qty = $wpdb->get_var($sql);

        if(is_null($min_qty) || $min_qty < $result)
            return $result;

        return $min_qty;
    }

    public function getBreaks( ): array
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_board_prices";

        $sql = "(SELECT min_qty, price FROM {$table} WHERE stock = 1 AND flute_id = {$this->fluteId} AND grade_id = {$this->gradeId} ORDER BY min_qty ASC LIMIT 1) UNION (SELECT min_qty, price FROM {$table} WHERE stock = 0 AND flute_id = {$this->fluteId} AND grade_id = {$this->gradeId})";

        return $wpdb->get_results($sql, "ARRAY_A");

    }

    public function getWalls(): int
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_flutes";

        $sql = "SELECT wall FROM {$table} WHERE flute_id = {$this->fluteId}";

        $walls = $wpdb->get_var($sql);

        return is_null($walls) ? 1 : $walls;
    }

    public function getDetails( $fluteId = 0 ): array
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_flutes";

        if($fluteId == 0){
            $fluteId = $this->fluteId;
        }

        $sql = "SELECT flute, colour, description FROM {$table} WHERE flute_id = {$fluteId}";

        $row = $wpdb->get_row($sql, "ARRAY_A");

        // Also add the grade
        $row['grade'] = $wpdb->get_var("SELECT grade FROM {$wpdb->prefix}qbcb_grades WHERE grade_id = {$this->gradeId}");

        return $row;
    }

    public static function getAllFlutes(  bool $showAll, bool $includeImages = false, $showStaff = false): array
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_flutes";

        $sql = "SELECT flute_id, flute, colour, image, description FROM {$table}";

        if(!$showAll){
            $sql .= " WHERE staff_only = " . ($showStaff ? 1 : 0);
        }

        $sql .= " ORDER BY wall ASC";

        $results = $wpdb->get_results($sql, "ARRAY_A");

        $options = [];
        foreach ($results as $flute) {
            $opt = $flute['flute'] . ($showStaff ? ' - ' . $flute['description'] : '');

            $opt = ['flute' => $opt, 'description' => $flute['description']];

            if ($includeImages) {
                $opt['image'] = plugin_dir_url(__FILE__) . '/img/select_placeholder.webp';
                if(!empty($flute['image'])) {
                    $flute_img = wp_get_attachment_image_src($flute['image'], 'full');

                    if(is_array($flute_img)) {
                        $opt['image'] = $flute_img[0];
                    }
                }
            }

            $opt['colour'] = $flute['colour'];

            $options[$flute['flute_id']] = $opt;
        }

        return $options;
    }

    public static function getStockFlutes(): array
    {
        global $wpdb;

        $table = $wpdb->prefix . 'qbcb_board_prices';

        $sql = "SELECT flute_id FROM {$table} WHERE stock = 1";

        $results = $wpdb->get_results($sql, "ARRAY_A");

        $flutes = [];

        foreach($results as $r){
            $flutes[] = $r['flute_id'];


            $sql = "SELECT flute_id FROM {$wpdb->prefix}qbcb_flutes WHERE equivalent = " . $r['flute_id'];


            foreach($wpdb->get_results($sql, "ARRAY_A") as $e)
                $flutes[] = $e['flute_id'];


        }

        return $flutes;
    }

    public static function getAllGrades( bool $includeStaff = true, bool $staffOnly = false): array
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_grades";

        $sql = "SELECT grade_id, grade FROM {$table}";

        if(!$includeStaff || $staffOnly){
            $sql .= " WHERE staff_only = " . ($staffOnly ? 1 : 0);
        }

        $sql .= " ORDER BY grade ASC";

        $results = $wpdb->get_results($sql, "ARRAY_A");

        $options = [];
        foreach ($results as $grade) {
            $options[$grade['grade_id']] = $grade['grade'];
        }

        return $options;
    }

    public static function getGradeById( int $gradeId ): string
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_grades";

        $sql = "SELECT grade FROM {$table} WHERE grade_id = {$gradeId}";

        $result = $wpdb->get_var($sql);

        return is_null($result) ? 'Unknown' : $result;
    }

    public static function getGradeId( string $grade ): int
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_grades";

        $sql = "SELECT grade_id FROM {$table} WHERE grade = '{$grade}'";

        $result = $wpdb->get_var($sql);

        return is_null($result) ? 0 : $result;
    }

    public static function getFluteEquivalent( int $fluteId ): int
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_flutes";

        $sql = "SELECT equivalent FROM {$table} WHERE flute_id = {$fluteId}";

        $result = $wpdb->get_var($sql);

        return (is_null($result) || $result == 0) ? $fluteId : $result;
    }

    public static function getFluteId( string $flute ): int
    {
        global $wpdb;
        $table = $wpdb->prefix . "qbcb_flutes";

        $sql = "SELECT flute_id FROM {$table} WHERE flute = '{$flute}'";

        $result = $wpdb->get_var($sql);

        return is_null($result) ? 0 : $result;
    }

}