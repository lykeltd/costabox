<?php

/*
 * Contains code relating to user management
 */

class Costabox_User
{
    
    private ?WP_User $user = null; 
    
    public function __construct()
    {
        $this->user = self::get_user() ?: null;
        
        add_action('show_user_profile', array($this, 'add_odoo_id_field'));
        add_action('edit_user_profile', array($this, 'add_odoo_id_field'));
        add_action('personal_options_update', array($this, 'save_odoo_id_field'));
        add_action('edit_user_profile_update', array($this, 'save_odoo_id_field'));

        add_action("show_user_profile", [$this, "add_collection_field"]);
        add_action("edit_user_profile", [$this, "add_collection_field"]);
        add_action("personal_options_update", [$this, "save_collection_field"]);
        add_action("edit_user_profile_update", [$this, "save_collection_field"]);

        add_action("woocommerce_register_form_start", [$this, "extra_register_fields"]);
        add_action("woocommerce_register_post", [$this, "validate_extra_register_fields"], 10, 3);
        add_action("woocommerce_created_customer", [$this, "save_extra_register_fields"], 10, 1);

        add_filter("woocommerce_min_password_strength", "__return_zero");
        add_action("login_enqueue_scripts", [$this, "login_css"]);

        // Change new user email notification
        add_filter("wp_new_user_notification_email", [$this, "new_user_email"], 10, 3);

        add_action("admin_head-user-edit.php", [$this, "change_profile_fields"]);
        add_action("admin_head-profile.php", [$this, "change_profile_fields"]);
        add_action("admin_footer-user-new.php", [$this, "new_user_company_dropdown_script"]);
        add_action("admin_enqueue_scripts", [$this, "enqueue_admin_scripts"]);

        add_action("admin_init", [$this, "user_roles"]);
    }

    function add_odoo_id_field($user): void
    {
        // Only show field for Costabox users
        if(!$this->user || !self::is_staff($this->user->ID)) {
            return;
        }

        $odoo_id = get_user_meta($user->ID, 'odoo_id', true);
        ?>
        <h3>Odoo</h3>
        <table class="form-table">
            <tr>
                <th><label for="odoo_id">Odoo User ID</label></th>
                <td>
                    <input type="text" name="odoo_id" id="odoo_id" value="<?php echo esc_attr($odoo_id); ?>" class="regular-text" />
                </td>
            </tr>
        </table>
        <?php
    }

    function save_odoo_id_field($user_id): void
    {
        if(empty($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'update-user_' . $user_id)){
            return;
        }

        if (!current_user_can('edit_user', $user_id)) {
            return;
        }

        update_user_meta($user_id, 'odoo_id', $_POST['odoo_id']);
    }

    public function add_collection_field( WP_User $user): void
    {
        if(!$this->user || !self::is_staff($this->user->ID)) {
            return;
        }

        ?>

        <h3>Collection</h3>

        <table class="form-table">
            <tr>
                <th scope="row">
                    <label for="allow_collection">Allow Customer to collect orders?</label>

                </th>
                <td>
                    <input class='checkbox' type="checkbox" name="qbcb_collection" <?php echo($user->get('qbcb_collection') == 'yes' ? "checked='checked'" : '') ?>
                    id="allow_collection">
                </td>
            </tr>
        </table>

        <?php
    }

    public function save_collection_field( int $user_id): void
    {
        if(empty($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'update-user_' . $user_id)){
            return;
        }

        update_user_meta($user_id, 'qbcb_collection', isset($_POST['qbcb_collection']) ? 'yes' : 'no');
    }

    public function extra_register_fields(): void
    {

        woocommerce_form_field(
            'billing_first_name',
            array(
                'type'          => 'text',
                'required'      => true, // just adds an "*"
                'label'         => __('First name'),
                'placeholder'   => __('Enter First name'),
                'class'         => array('form-row form-row-first'),
                'id'            => 'billing_first_name'
            ),
            ($_POST['billing_first_name'] ?? '')
        );

        woocommerce_form_field(
            'billing_last_name',
            array(
                'type'          => 'text',
                'required'      => true, // just adds an "*"
                'label'         => __('Last name'),
                'placeholder'   => __('Enter Last name'),
                'class'         => array('form-row form-row-last'),
                'id'            => 'billing_last_name'
            ),
            ($_POST['billing_last_name'] ?? '')
        );

        woocommerce_form_field(
            'billing_company',
            array(
                'type'          => 'text',
                'required'      => true, // just adds an "*"
                'label'         => __('Company name'),
                'placeholder'   => __('Enter Company name'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_company'
            ),
            ($_POST['billing_company'] ?? '')
        );

        woocommerce_form_field(
            'billing_address_1',
            array(
                'type'          => 'text',
                'required'      => true, // just adds an "*"
                'label'         => __('Street address'),
                'placeholder'   => __('House number and street name'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_address_1'
            ),
            ($_POST['billing_address_1'] ?? '')
        );

        woocommerce_form_field(
            'billing_address_2',
            array(
                'type'          => 'text',
                'required'      => false,
                'placeholder'   => __('Apartment, suite, unit, etc. (optional)'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_address_2'
            ),
            ($_POST['billing_address_2'] ?? '')
        );

        woocommerce_form_field(
            'billing_city',
            array(
                'type'          => 'text',
                'required'      => true, // just adds an "*"
                'label'         => __('Town / City'),
                'placeholder'   => __('Enter Town / City'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_city'
            ),
            ($_POST['billing_city'] ?? '')
        );

        woocommerce_form_field(
            'billing_state',
            array(
                'type'          => 'text',
                'required'      => false,
                'label'         => __('County'),
                'placeholder'   => __('Enter County'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_state'
            ),
            ($_POST['billing_state'] ?? '')
        );

        woocommerce_form_field(
            'billing_postcode',
            array(
                'type'          => 'text',
                'required'      => true, // just adds an "*"
                'label'         => __('Postcode'),
                'placeholder'   => __('Enter Postcode'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_postcode'
            ),
            ($_POST['billing_postcode'] ?? '')
        );

        woocommerce_form_field(
            'billing_phone',
            array(
                'type'          => 'text',
                'required'      => true,
                'label'         => __('Phone (Best contact number for when we deliver.)'),
                'placeholder'   => __('Enter Phone'),
                'class'         => array('form-row form-row-wide'),
                'id'            => 'billing_phone'
            ),
            ($_POST['billing_phone'] ?? '')
        );
    }


    function validate_extra_register_fields( $username, $email, $validation_errors )
    {
        if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) )
        {
            $validation_errors->add( 'billing_first_name_error', __( 'First name is required!', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) )
        {
            $validation_errors->add( 'billing_last_name_error', __( 'Last name is required!.', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) )
        {
            $validation_errors->add( 'billing_company_error', __( 'Company name is required!', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) )
        {
            $validation_errors->add( 'billing_address_1_error', __( 'Address is required!', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) )
        {
            $validation_errors->add( 'billing_city_error', __( 'Town or City is required!', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) )
        {
            $validation_errors->add( 'billing_postcode_error', __( 'Postcode is required!', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) )
        {
            $validation_errors->add( 'billing_phone_error', __( 'Phone or Mobile # is required!', 'woocommerce' ) );
        }

        return $validation_errors;
    }

    function save_extra_register_fields( $customer_id )
    {
        if ( isset( $_POST['billing_first_name'] ) )
        {
            update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
            update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
            update_user_meta( $customer_id, 'shipping_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        }

        if ( isset( $_POST['billing_last_name'] ) )
        {
            update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
            update_user_meta( $customer_id, 'shipping_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
            update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        }

        if ( isset( $_POST['billing_company'] ) )
        {
            update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
            update_user_meta( $customer_id, 'shipping_company', sanitize_text_field( $_POST['billing_company'] ) );
        }

        if ( isset( $_POST['billing_address_1'] ) )
        {
            update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
            update_user_meta( $customer_id, 'shipping_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
        }

        if ( isset( $_POST['billing_address_2'] ) )
        {
            update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
            update_user_meta( $customer_id, 'shipping_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
        }

        if ( isset( $_POST['billing_city'] ) )
        {
            update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
            update_user_meta( $customer_id, 'shipping_city', sanitize_text_field( $_POST['billing_city'] ) );
        }

        if ( isset( $_POST['billing_state'] ) )
        {
            update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_state'] ) );
            update_user_meta( $customer_id, 'shipping_state', sanitize_text_field( $_POST['billing_state'] ) );
        }

        if ( isset( $_POST['billing_postcode'] ) )
        {
            update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
            update_user_meta( $customer_id, 'shipping_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
        }

        if ( isset( $_POST['billing_phone'] ) )
        {
            update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
            update_user_meta( $customer_id, 'shipping_phone', sanitize_text_field( $_POST['billing_phone'] ) );
        }

        update_user_meta($customer_id, 'billing_country', 'GB');
        update_user_meta($customer_id, 'shipping_country', 'GB');

        // Ensure a company is created and assigned
        Costabox_New_User::process( $customer_id );
    }

    public function login_css(): void
    {
        wp_dequeue_script('password-strength-meter');
        wp_dequeue_script('user-profile');
        wp_deregister_script('user-profile');

        $suffix = SCRIPT_DEBUG ? '' : '.min';
        wp_enqueue_script( 'user-profile', "/wp-admin/js/user-profile$suffix.js", array( 'jquery', 'wp-util' ), false, 1 );
    }

    public function user_roles(): void
    {
        add_role('quickbox_staff', 'Quickbox Staff');
    }

    public function new_user_email( array $email, WP_User $user, string $blogname ): string
    {

        $reset_key = get_password_reset_key($user);
        $reset_link = network_site_url("wp-login.php?action=rp&key=$reset_key&login=" . rawurlencode($user->user_login), 'login');

        $greeting = $user->first_name;

        if (empty($greeting) && function_exists('wcb2brp_is_b2b_user')) {
            if (wcb2brp_is_b2b_user($user->id)) {
                $greeting = get_the_title(get_user_meta($user->id, 'wcb2brp_company', true));
            }
        }

        $email['subject'] = "Your new account on $blogname";
        $message = 'Hi ' . $greeting . ",<br><br>We are pleased to confirm your account has been set up on our website. Now you can enjoy a better and more simple buying experience with us, view your orders and quotations, and benefit from prices that are bespoke to you.<br><br>To set a password for your account if you haven't already done so, simply click the following link. Your username is your email address.<br><br><a href='{$reset_link}'>" . $reset_link . '</a><br><br>' . 'Kind Regards,<br>Quickbox Manufacturing Limited';
        $heading = 'Your Account';

        // Get woocommerce mailer from instance
        $mailer = WC()->mailer();

        // Wrap message using woocommerce html email template
        $wrapped_message = $mailer->wrap_message($heading, $message);

        // Create new WC_Email instance
        $wc_email = new WC_Email;

        // Style the wrapped message with woocommerce inline styles
        $html_message = $wc_email->style_inline($wrapped_message);

        $email['headers'] = ['Content-Type: text/html; charset=UTF-8'];

        $email['message'] = $html_message;

        return $email;

    }

    public function change_profile_fields(): void {
        global $pagenow;

        echo "<script type='text/javascript'>jQuery(document).ready(function(){jQuery('select[name=\"company\"]').select2();});</script>";

        if ($pagenow == 'user-new.php') {
            return;
        }

        $to_remove = ['url', 'facebook', 'twitter', 'instagram', 'myspace', 'linkedin', 'pinterest', 'soundcloud', 'tumblr', 'youtube', 'wikipedia', 'description', 'rich-editing', 'syntax-highlighting', 'admin-color', 'comment-shortcuts', 'admin-bar-front'];

        echo '<style>';

        foreach ($to_remove as $ftr) {
            echo "tr.user-{$ftr}-wrap,";
        }

        echo 'tr.test{ display: none; }';

        echo 'div.yoast-settings { display: none }';

        echo '</style>';
    }

    public function new_user_company_dropdown_script(): void
    {
        ?>

        <style type="text/css">
            select[name='company'] {
                opacity: 0.05 !important;
            }
        </style>

        <script type='text/javascript'>
            jQuery(document).ready(function() {

                jQuery('select[name=\"company\"]').removeClass("chosen-select").select2();
            });

            jQuery('#createusersub').hover(function() {
                var sel_co = (jQuery('select[name="company"]').select2('val'));

                jQuery("select[name='company'] option[selected='selected']").removeAttr("selected");

                jQuery("select[name='company'] option[value='" + sel_co + "']").attr('selected', '');


            });

            jQuery("#createusersub").click(function() {
                jQuery(".select2-container").remove();
            });
        </script>

        <?php
    }

    public function enqueue_admin_scripts(): void
    {
        global $pagenow;

        if($pagenow == 'user-new.php'){
            wp_register_style('select2css', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css', false, '1.0', 'all');
            wp_register_script('select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js', ['jquery'], '1.0', true);
            wp_enqueue_style('select2css');
            wp_enqueue_script('select2');
        }
    }

    public static function is_staff( ?int $user_id): bool
    {
        if(!$user_id) {
            return false;
        }

        $user = get_userdata($user_id);

        return $user && (in_array('quickbox_staff', (array) $user->roles) || in_array('administrator', (array) $user->roles));
    }

    public static function get_user(): WP_User|false
    {
        if(!is_user_logged_in()) {
            return false;
        }

        if (isset($_COOKIE['wp_loginasuser_olduser_' . COOKIEHASH])) {
            $cookie      =  wp_unslash($_COOKIE['wp_loginasuser_olduser_' . COOKIEHASH]);
            $old_user_id = wp_validate_auth_cookie($cookie, 'logged_in');

            if ($old_user_id) {
                return get_userdata($old_user_id);
            }
        }

        return wp_get_current_user() ?? false;

    }

    public static function get_odoo_id( $user_id = null ): int|false
    {
        $user = $user_id ? get_userdata($user_id) : self::get_user();

        if(!$user) {
            return false;
        }

        $odoo_id = get_user_meta($user->ID, 'odoo_id', true);

        return empty($odoo_id) ? false : (int) $odoo_id;
    }

    public static function can_collect( ?int $user_id = null ): bool
    {
        $user = $user_id ? get_userdata($user_id) : wp_get_current_user();

        if(!$user) {
            return false;
        }

        $can_collect = get_user_meta($user->ID, 'qbcb_collection', true) === 'yes';

        if($can_collect) {
            return true;
        }

        if(function_exists('wcb2brp_is_b2b_user') && wcb2brp_is_b2b_user($user->ID)) {
            $company_id = get_user_meta($user->ID, 'wcb2brp_company', true);
            return get_post_meta($company_id, 'qbcb_collection', true) === 'yes';
        }

        return false;
    }

    public static function in_group( $group_name, $user_id = null ): bool
    {
        $user = $user_id ? get_userdata($user_id) : self::get_user();

        if(!$user) {
            return false;
        }

        $groups_user = new Groups_User($user_id);
        $user_groups = $groups_user->groups;

        if (empty($groups_user->groups)) {
            return false;
        }

        foreach ($user_groups as $grp) {
            if (strtolower($grp->name) == strtolower($group_name)) {
                return true;
            }
        }

        return false;

    }

}