<?php
class QBCB_Courier_Shipping_Method extends WC_Shipping_Method {
    /**
     * Constructor for your shipping class
     *
     * @access public
     * @return void
     */
    public function __construct( $instance_id = 0 ) {
        $this->id                 = 'qbcb_courier';
        $this->instance_id 		  = absint($instance_id);
        $this->method_title       = __( 'Courier (Costabox)', 'qbcb' );
        $this->method_description = __( 'Courier service for Quickbox Costabox', 'qbcb' );

        $this->supports              = array(
            'shipping-zones',
            'instance-settings',
            'instance-settings-modal'
        );



        $this->init();

        $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
        $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Courier Shipping', 'qbcb' );
    }

    public function get_label(){
        return $this->title;
    }

    /**
     * Init your settings
     *
     * @access public
     * @return void
     */
    function init() {
        // Load the settings API
        $this->init_form_fields();
        //$this->init_settings();

        // Save settings in admin if you have any defined
        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
    }

    /**
     * Define settings field for this shipping
     * @return void
     */
    function init_form_fields() {

        $this->instance_form_fields = array(

            'enabled' => array(
                'title' => __( 'Enable', 'qbcb' ),
                'type' => 'checkbox',
                'description' => __( 'Enable this shipping.', 'qbcb' ),
                'default' => 'yes'
            ),

            'title' => array(
                'title' => __( 'Title', 'qbcb' ),
                'type' => 'text',
                'description' => __( 'Title to be display on site', 'qbcb' ),
                'default' => __( 'Courier', 'qbcb' )
            ),

            'initial_price' => array(
                'title' => "Price up to 20kg",
                'type' => 'number',
                'description' => "How much does the first 20kg cost?",
                'default' => 7.9
            ),

            'additional_price' => array(
                'title' => 'Price per kg',
                'type' => 'number',
                'description' => 'How much is each kg after 20kg?',
                'default' => 0.36
            )

        );

    }

    /**
     * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
     *
     * @access public
     * @param mixed $package
     * @return void
     */
    public function calculate_shipping( $package = array() ) {

        $weight = 0;
        $cost = 0;

        $l_product = null;

        foreach ( $package['contents'] as $item_id => $values )
        {
            $l_product = $values;
            $_product = $values['data'];

            if(isset($values['_costabox']['weight'])){
                $values['_costabox']['weight'] = str_replace(",", "", $values['_costabox']['weight']);
                $weight += $values['_costabox']['weight'];
            }else{
                if(is_numeric($_product->get_weight()))
                    $weight = $weight + $_product->get_weight();
            }
        }

        //$weight = wc_get_weight( $weight, 'kg' );

        $addition_fee = $weight - 20;

        $cost = empty($this->instance_settings['initial_price']) ? 7.9 : $this->instance_settings['initial_price'];

        if($addition_fee > 0){
            $addition_fee = ceil($addition_fee);
            $cost += (empty($this->instance_setting['additional_price']) ? 0.36 : $this->instance_settings['additional_price']) * $addition_fee;
        }

        $rate = array(
            'id' => $this->id,
            'label' => $this->title,
            'cost' => $cost,
        );

        $this->add_rate( $rate );

    }
}
