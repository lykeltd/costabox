<?php

class Costabox_Companies
{
    private bool $is_staff = false;
    public function __construct()
    {

        $user = Costabox_User::get_user();

        $this->is_staff = $user && Costabox_User::is_staff($user->ID);

        add_action("add_meta_boxes", [$this, "add_meta_boxes"]);
        add_action("save_post", [$this, "save_post"], 10, 3);

        add_filter( 'manage_company_posts_columns', [$this, "admin_table_cols"] );
        add_action( 'manage_company_posts_custom_column' , [$this, "admin_table_cols_content"], 10, 2 );


        add_action("restrict_manage_posts", [$this, "company_groups_filter_select"]);
        add_filter( 'parse_query', [$this, "company_groups_filter"] );

        add_action("wp_enqueue_scripts", [$this, "enqueue_scripts"]);

    }

    public function add_meta_boxes(): void
    {
        add_meta_box('qbcb_company_margins', "Costabox Price Markup", [$this, "markup_callback"], "company");
        add_meta_box('qbcb_company_collection', "Collection", [$this, "collection_callback"], "company");
        add_meta_box('qbcb_company_account_number', "Account", [$this, "account_callback"], "company");
        add_meta_box('qbcb_company_domain', "Domain Name", [$this, "domain_callback"], "company");
        add_meta_box('qbcb_company_addresses', "Addresses", [$this, "address_callback"], "company");
        add_meta_box('qbcb_company_groups', "Groups", [$this, "groups_callback"], "company");

        remove_meta_box( 'wpseo_meta' , 'company' , 'normal' );
        remove_meta_box( 'groups-permissions' , 'company' , 'side' );
    }

    public function markup_callback( WP_Post $post ): void
    {

        if(!$this->is_staff) return;

        $readonly = current_user_can("administrator") ? "" : "readonly='readonly'";

        echo '<table class="form-table">';

        echo '<tr><th scope="row"><label for="markup_1">Markup under 200m<sup>2</sup></label></th><td>';

        $markup_1 = get_post_meta($post->ID, "_qbcb_markup_1", true);

        echo "<input {$readonly} type='number' id='markup_1' name='qbcb_markup_1' min='0' max='1000' style='min-width: 200px' placeholder='Leave blank to use " . costabox_get_setting("markup_1") . "%' value='{$markup_1}'>%";

        echo "</td></tr>";

        echo '<tr><th scope="row"><label for="markup_2">Markup between 200m<sup>2</sup> and 500m<sup>2</sup></label></th><td>';

        $markup_2 = get_post_meta($post->ID, "_qbcb_markup_2", true);

        echo "<input {$readonly} type='number' id='markup_2' name='qbcb_markup_2' min='0' max='1000' style='min-width: 200px' placeholder='Leave blank to use  " . costabox_get_setting("markup_2") . "%' value='{$markup_2}'>%";

        echo "</td></tr>";

        echo '<tr><th scope="row"><label for="markup_3">Markup over 500m<sup>2</sup></th><td>';

        $markup_3 = get_post_meta($post->ID, "_qbcb_markup_3", true);

        echo "<input {$readonly} type='number' id='markup_3' name='qbcb_markup_3' min='0' max='1000' style='min-width: 200px' placeholder='Leave blank to use " . costabox_get_setting("markup_3") . "%' value='{$markup_3}'>%";

        echo "</td></tr>";

        echo "</table>";

    }

    public function collection_callback( WP_Post $post ): void
    {
        if(!$this->is_staff) return;

        $readonly = current_user_can("administrator") ? "" : "readonly='readonly'";

        echo '<table class="form-table">';

        echo '<tr><th scope="row"><label for="allow_collection">Allow customer to collect?</label></th><td>';

        $allow_collection = get_post_meta($post->ID, "qbcb_collection", true);

        echo "<input {$readonly} type='checkbox' class='checkbox' value='yes' id='allow_collection' name='qbcb_collection' " . (($allow_collection == 'yes') ? 'checked="checked"' : '') . ">";

        echo "</td></tr>";

        echo "</table>";
    }

    public function account_callback( WP_Post $post ): void
    {
        if(!$this->is_staff) return;

        $readonly = current_user_can("administrator") ? "" : "readonly='readonly'";

        echo '<table class="form-table">';

        echo '<tr><th scope="row"><label for="account_number">Customer Account Number</label></th><td>';

        $account_number = get_post_meta($post->ID, "qbcb_account_number", true);

        echo "<input {$readonly} type='text' id='account_number' name='qbcb_account_number' value='{$account_number}'>";

        echo "</td></tr>";

        echo "</table>";

        echo "<p style='font-style: italic'>Leave blank to disable pay on account for customer.</p>";
    }

    public function domain_callback( WP_Post $post ): void
    {
        if(!$this->is_staff) return;

        echo '<table class="form-table">';

        echo '<tr><th scope="row"><label for="domain_name">Domain name:</label></th><td>';

        $value = get_post_meta($post->ID, "qbcb_domain", true);

        echo "<input type='text' value='{$value}' id='domain_name' name='qbcb_domain' >";

        echo "</td></tr>";

        echo "</table>";
    }

    public function address_callback( WP_Post $post ): void
    {
        if (!$this->is_staff) return;

        $readonly = "";// current_user_can("administrator") ? "" : "readonly='readonly'";

        $fields = array("address_1" => "Address 1", "address_2" => "Address 2", "city" => "Town/City", "postcode" => "Postcode", "state" => "County/State", "country" => "Country");

        $types = array("Billing", "Shipping");

        foreach($types as $type){

            $t = strtolower($type);

            echo '<table class="form-table" style="float: left; width: 40%; margin-left: 10px; clear: none;">';

            echo "<tr><td colspan='2'><h3>{$type} Address</h3></td></tr>";

            foreach($fields as $f => $l){

                $v = get_post_meta($post->ID, "qbcb_{$t}_{$f}", true);

                echo "<tr><th scope='row' for='{$t}_{$f}'>{$l}</label></th><td><input type='text' id='{$t}_{$f}' name='qbcb_{$t}_{$f}' value='{$v}' {$readonly}></td></tr>";

            }

            echo "</table>";

        }

        echo "<div style='clear: both'></div>";
    }

    public function groups_callback( WP_Post $post ): void
    {
        if(!$this->is_staff) return;

        if(!current_user_can("administrator")) return;

        wp_nonce_field( 'qbcb_groups_company_fields', 'qbcb_groups_company_fields_nonce' );

        $grps = Groups_Group::get_groups();

        $group_options = "";

        $selected = get_post_meta($post->ID, "qbcb_groups", true);
        $selected = explode(",", $selected);

        echo "<script type='text/javascript'>jQuery(document).ready(function(){jQuery('.wc-enhanced-select').select2();});</script>";

        foreach($grps as $grp){
            $group_options .= "<option value='{$grp->group_id}'" . (in_array($grp->group_id, $selected) ? " selected" : "") . ">{$grp->name}</option>";
        }

        echo '<table class="form-table">';

        echo "<tr><th scope='row' for='company_groups'>Groups</label></th><td><select multiple style='width: 100%' class='wc-enhanced-select' id='company_groups' name='qbcb_groups[]'>{$group_options}</select></td></tr>";

        echo "</table>";
    }

    public function save_post( int $post_id ): void
    {

        if(array_key_exists("qbcb_markup_1", $_POST))
            update_post_meta($post_id, "_qbcb_markup_1", $_POST['qbcb_markup_1']);

        if(array_key_exists("qbcb_markup_2", $_POST))
            update_post_meta($post_id, "_qbcb_markup_2", $_POST['qbcb_markup_2']);

        if(array_key_exists("qbcb_markup_3", $_POST))
            update_post_meta($post_id, "_qbcb_markup_3", $_POST['qbcb_markup_3']);

        if(array_key_exists("qbcb_collection", $_POST))
            update_post_meta($post_id, "qbcb_collection", ($_POST['qbcb_collection'] == 'yes' ? "yes" : "no"));

        if(array_key_exists("qbcb_account_number", $_POST))
            update_post_meta($post_id, "qbcb_account_number", $_POST['qbcb_account_number']);

        if(array_key_exists("qbcb_domain", $_POST))
            update_post_meta($post_id, "qbcb_domain", $_POST['qbcb_domain']);


        $fields = array("address_1" => "Address 1", "address_2" => "Address 2", "city" => "Town/City", "postcode" => "Postcode", "state" => "County/State", "country" => "Country");

        $types = array("Billing", "Shipping");

        foreach($types as $type){

            $t = strtolower($type);

            foreach($fields as $f => $l){

                if(array_key_exists("qbcb_{$t}_{$f}", $_POST))
                    update_post_meta($post_id, "qbcb_{$t}_{$f}", $_POST["qbcb_{$t}_{$f}"]);

            }

        }


        if(array_key_exists("qbcb_groups",  $_POST)) {

            update_post_meta($post_id, "qbcb_groups", implode(",", ($_POST['qbcb_groups'])));

            // Make sure the users associated with this company are in the correct groups
            global $wpdb;

            $sql = "SELECT user_id FROM {$wpdb->prefix}usermeta WHERE meta_key = 'wcb2brp_company' AND meta_value = '{$post_id}'";

            $results = $wpdb->get_results($sql, "ARRAY_A");

            $groups = Groups_Group::get_groups();


            foreach ($results as $r) {

                foreach ($groups as $grp) {
                    Groups_User_Group::delete($r['user_id'], $grp->group_id);
                }

                foreach ($_POST['qbcb_groups'] as $grp_id)
                    Groups_User_Group::create(array("user_id" => $r['user_id'], "group_id" => $grp_id));

            }
        }
    }

    public function admin_table_cols( array $columns ): array
    {
        $columns['qbcb_groups'] = "Groups";
        return $columns;
    }

    public function admin_table_cols_content( string $column, int $post_id ): void
    {
        if($column == "qbcb_groups"){

            $grps = Groups_Group::get_groups();


            $selected = get_post_meta($post_id, "qbcb_groups", true);
            $selected = explode(",", $selected);

            $names = array();

            foreach ($grps as $g) {
                if(in_array($g->group_id, $selected)){
                    $names[] = $g->name;
                }
            }

            echo implode(", ", $names);
        }
    }

    public function company_groups_filter_select(): void
    {
        $type = 'posts';
        if (isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
        }

        //only add filter to post type you want
        if ('company' == $type){

            $grps = Groups_Group::get_groups();

            $values = array();

            foreach($grps as $g){
                $values[$g->name] = $g->group_id;
            }
            ?>
            <select name="qbcb_filter_company_group">
                <option value="">All Groups</option>
                <?php
                $current_v = isset($_GET['qbcb_filter_company_group'])? $_GET['qbcb_filter_company_group']:'';
                foreach ($values as $label => $value) {
                    printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
                ?>
            </select>
            <?php
        }
    }

    public function company_groups_filter( WP_Query $query ): void
    {
        global $pagenow;
        $type = 'post';
        if (isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
        }
        if ( 'company' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['qbcb_filter_company_group']) && $_GET['qbcb_filter_company_group'] != '') {
            $query->query_vars['meta_key'] = 'qbcb_groups';
            $query->query_vars['meta_value'] = $_GET['qbcb_filter_company_group'];
            $query->query_vars['meta_compare'] = 'LIKE';
        }
    }

    public function enqueue_scripts(): void
    {
        global $wp;

        if(!strrpos(add_query_arg( array(), $wp->request ) , "company/add-user")) return;

        wp_enqueue_script("qbcb_company_user", plugin_dir_url(__FILE__) . "/js/new_company_user.js?v=1.0", array('jquery'));
    }

}