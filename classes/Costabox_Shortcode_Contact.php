<?php

include_once 'Costabox_Shortcode_Form.php';
include_once WP_PLUGIN_DIR . '/costabox/odoo/class.php';

class Costabox_Shortcode_Contact extends Costabox_Shortcode_Form
{

    private array $fields = array();

    private int $thank_you_page = 71805;

    public function __construct()
    {
        add_action("template_redirect", array($this, "protect_thank_you_page"));

        $this->fields = array(
            "first_name" => array(
                "label" => "First Name",
                "type" => "text",
                "required" => true,
            ),
            "last_name" => array(
                "label" => "Last Name",
                "type" => "text",
                "required" => true,
            ),
            "email" => array(
                "label" => "Email",
                "type" => "email",
                "required" => true,
            ),
            "phone" => array(
                "label" => "Phone",
                "type" => "tel",
                "required" => true,
            ),
            "address" => array(
                "label" => "Address",
                "type" => "heading",
                "description" => "Let us have your address so we can include any delivery costs.",
                "required" => false,
            ),
            "address_1" => array(
                "label" => "Address 1",
                "type" => "text",
                "required" => false,
            ),
            "address_2" => array(
                "label" => "Address 2",
                "type" => "text",
                "required" => false,
            ),
            "city" => array(
                "label" => "City",
                "type" => "text",
                "required" => false,
            ),
            "county" => array(
                "label" => "County",
                "type" => "text",
                "required" => false,
            ),
            "postcode" => array(
                "label" => "Postcode",
                "type" => "text",
                "required" => false,
            ),
            "country" => array(
                "label" => "Country",
                "type" => "text",
                "required" => false,
            ),
            "subject" => array(
                "label" => "Subject",
                "type" => "text",
                "required" => true,
            ),
            "message" => array(
                "label" => "Message",
                "type" => "textarea",
                "description" => "Let us know how we can help you.",
                "required" => true,
            ),
        );

        parent::__construct("contact_form", $this->fields, "Send Message");
    }

    public function process(): void
    {

        $form_values = $this->get_form_values();

        // Make sure the telephone number starts wih +44
        if(str_starts_with($form_values["phone"], "0")) {
            $form_values["phone"] = "+44" . substr($form_values["phone"], 1);
        }

        // First, send an automated email to the user
        $to = $form_values["email"];
        $subject = "Thank you for contacting us";
        $message = "Hello {$form_values['first_name']},<br><br>Thank you for contacting us. We will get back to you as soon as possible. Here is a copy of your submission for your records:<br><br>";

        foreach ($form_values as $key => $value) {
            $message .= "<strong>{$this->fields[$key]['label']}</strong>: {$value}<br>";
        }

        $message .= "<br>Kind regards,<br>Quickbox";

        wp_mail($to, $subject, $message, array('Content-Type: text/html; charset=UTF-8'));

        // Then, create a lead in Odoo
        $odoo = new Costabox_Odoo();
        $odoo->createContactFormLead($form_values);

        // Finally, redirect the user to the thank you page
        wp_redirect(get_permalink($this->thank_you_page) . "?cs=" . strtotime("now"));
        exit;
    }

    public function protect_thank_you_page(): void
    {
        if (is_page($this->thank_you_page)) {

            $diff = strtotime("now") - ($_GET["cs"] ?? 500);

            if(abs(round($diff)) > 60) {
                wp_redirect(home_url());
                exit;
            }
        }
    }
}