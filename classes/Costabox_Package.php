<?php

use DVDoug\BoxPacker;

class Costabox_Package implements DVDoug\BoxPacker\Box {
	 /**
     * Reference for box type (e.g. SKU or description).
     *
     * @return string
     */
    public function getReference() {
    	return "Package";
    }

    /**
     * Outer width in mm.
     *
     * @return int
     */
    public function getOuterWidth(){
    	return 1790;
    }

    /**
     * Outer length in mm.
     *
     * @return int
     */
    public function getOuterLength(){
    	return 1190;
    }

    /**
     * Outer depth in mm.
     *
     * @return int
     */
    public function getOuterDepth(){
    	return 590;
    }

    /**
     * Empty weight in g.
     *
     * @return int
     */
    public function getEmptyWeight(){
    	return 0;
    }

    /**
     * Inner width in mm.
     *
     * @return int
     */
    public function getInnerWidth(){
    	return 1790;
    }

    /**
     * Inner length in mm.
     *
     * @return int
     */
    public function getInnerLength(){
    	return 1190;
    }

    /**
     * Inner depth in mm.
     *
     * @return int
     */
    public function getInnerDepth(){
    	return 590;
    }

    /**
     * Total inner volume of packing in mm^3.
     *
     * @return int
     */
    public function getInnerVolume(){
    	return $this->getInnerWidth() * $this->getInnerLength() * $this->getInnerDepth();
    }

    /**
     * Max weight the packaging can hold in g.
     *
     * @return int
     */
    public function getMaxWeight(){
    	return 25000;
    }

    public static function getHeight(): int
    {
        return 590;
    }
}