<?php

abstract class Costabox_Shortcode_Form
{

    private string $shortcode_name = "";
    private array $fields = array();
    private string $submit_label = "Submit";
    private string $reCaptcha_site_key = "6LfW9cIUAAAAACLJVoyax9X-t5vFsjpFIvUS6sFa";
    private string $reCaptcha_secret_key = "6LfW9cIUAAAAAOogmsL_MfsblB2Lrig_iuWJhdZI";

    public function __construct(string $shortcode_name, array $fields, string $submit_label = "Submit", ?array $callback = null )
    {
        $this->shortcode_name = $shortcode_name;
        $this->fields = $fields;
        $this->submit_label = $submit_label;

        if(!$callback){
            $callback = array($this, 'output');
        }

        add_shortcode("qbcb_" . $this->shortcode_name, $callback);
    }

    public function get_form_values(): array {
        $values = array();
        foreach($this->fields as $name => $field){
            $values[$name] = $_POST[$name] ?? '';
        }
        return $values;
    }

    public function required_fields_present(): bool {
        foreach($this->fields as $name => $field){
            if($field['required'] && empty($_POST[$name])){
                return false;
            }
        }

        return true;
    }

    public abstract function process(): void;


    public function output(): string {

        $output = "";

        if(isset($_POST['costabox_shortcode']) &&
            $_POST['costabox_shortcode'] == $this->shortcode_name &&
            wp_verify_nonce($_POST["costabox_{$this->shortcode_name}_nonce"], "costabox_{$this->shortcode_name}")){

            if(!$this->required_fields_present()) {
                $output .= "<p style='color: red; background: rgb(255, 0, 0, 0.25); border: 1px solid red;'>Please fill in all required fields</p>";

            }elseif($this->does_pass_recaptcha()){

                $this->process();

                $output .= "<p style='color: green; background: rgb(0, 255, 0, 0.25); border: 1px solid green;'>Thanks for contacting us! We will get in touch with you shortly.</p>";

            }else{

                $output .= "<p style='color: red; background: rgb(255, 0, 0, 0.25); border: 1px solid red;'>Invalid reCaptcha</p>";

            }

        }

        $output .= "<form class='costabox-form costabox' method='post' id='form_{$this->shortcode_name}'>";
        $output .= "<input type='hidden' name='costabox_shortcode' value='{$this->shortcode_name}' />";
        $output .= wp_nonce_field("costabox_{$this->shortcode_name}", "costabox_{$this->shortcode_name}_nonce", true, false);

        foreach($this->fields as $name => $field){

            $value = isset($_POST[$name]) ? sanitize_text_field($_POST[$name]) : '';

            if($field['type'] == 'heading') {
                $output .= "<h3>{$field['label']}</h3>";
                $output .= "<p>{$field['description']}</p>";

            }elseif($field['type'] == 'textarea') {
                $required = $field['required'] ? "required='required'" : "";
                $output .= "<div class='costabox-form-field'>";
                $output .= "<label for='{$name}'> {$field['label']}" . (!empty($required) ? "*" : "") . "</label>";
                $output .= "<textarea $required name='{$name}' id='{$name}' placeholder='{$field['description']}' style='width: 100%; height: 75px !important; font-family: inherit; border: 1px solid #e2e2e2;line-height: 44px;padding: 0 10px !important;height: 44px;margin-bottom: 20px;font-size: 14px;'>{$value}</textarea>";
                $output .= "</div>";
            }else {
                $required = $field['required'] ? "required='required'" : "";
                $output .= "<div class='costabox-form-field'>";
                $output .= "<label for='{$name}'> {$field['label']}" . (!empty($required) ? "*" : "") . "</label>";
                $output .= "<input type='{$field['type']}' name='{$name}' id='{$name}' style='width: 100%' $required value='{$value}'/>";
                $output .= "</div>";
            }
        }

        $output .= $this->get_recaptcha_code();
        // $output .= "<input type='submit' style='margin: 0 auto; background: #a81710; display: block; color: #fff; font-weight: 700; padding: 0 50px !important;' value='{$this->submit_label}' />";
        $output .= "<button class='g-recaptcha' style='margin: 0 auto; background: #a81710; display: block; color: #fff; font-weight: 700; padding: 10px 50px !important; box-shadow: 0px 15px 20px -15px #a81710 !important; border-radius: 5px; border: none;' data-sitekey='{$this->reCaptcha_site_key}' data-callback='onSubmit' data-action='submit'>{$this->submit_label}</button>";

        $output .= "</form>";

        return $output;
    }

    private function get_recaptcha_code(): string
    {
        $recaptcha_code = '<script src="https://www.google.com/recaptcha/api.js"></script>';

        $recaptcha_code .= ' <script>
                                function onSubmit(token) {
                                    document.getElementById("form_' . $this->shortcode_name . '").submit();
                                }
                            </script>';

        return $recaptcha_code;
    }

    private function does_pass_recaptcha(): bool
    {
        if(empty($_POST['g-recaptcha-response'])){
            return false;
        }

        $token = $_POST['g-recaptcha-response'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $this->reCaptcha_secret_key, 'response' => $token)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        $arrResponse = json_decode($response, true);

        return $arrResponse["success"] == '1' && $arrResponse["score"] >= 0.5;

    }


}