<?php

class Costabox_DB
{
    public static function initialise()
    {
        global $wpdb;

        $charset = $wpdb->get_charset_collate();

        $tbl_prefix = $wpdb->prefix . 'qbcb_';

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        // ATTRIBUTES TABLE
        $sql = "CREATE TABLE {$tbl_prefix}attributes (
  			`attribute_id` INT NOT NULL AUTO_INCREMENT,
  			`name` VARCHAR(100) NOT NULL,
  			`slug` VARCHAR(45) NOT NULL,
  			`type` VARCHAR(45) NOT NULL COMMENT 'HTML data type for input',
  			PRIMARY KEY (`attribute_id`),
  			UNIQUE INDEX `slug_UNIQUE` (`slug` ASC)
  		) {$charset};";

        dbDelta($sql);

        // ATTRIBUTE OPTIONS TABLE
        $sql = "CREATE TABLE {$tbl_prefix}attribute_options (
      `aoption_id` INT NOT NULL AUTO_INCREMENT,
      `attribute_id` INT NOT NULL,
      `label` VARCHAR(100) NOT NULL,
      `value` VARCHAR(45) NOT NULL,
      PRIMARY KEY (`aoption_id`))
    {$charset};";

        dbDelta($sql);

        // ATTRIBUTE OPTIONS RESTRICTIONS TABLE
        $sql = "CREATE TABLE {$tbl_prefix}attribute_options_restrictions (
      `restriction_id` INT NOT NULL AUTO_INCREMENT,
      `box_type_id` INT NOT NULL,
      `attribute_id` INT NOT NULL,
      `type` VARCHAR(45) NOT NULL,
      `value` VARCHAR(45) NOT NULL,
      PRIMARY KEY (`restriction_id`))
    {$charset};";

        dbDelta($sql);

        // BOX TYPE TABLE
        $sql = "CREATE TABLE {$tbl_prefix}box_type (
      `box_type_id` INT NOT NULL AUTO_INCREMENT,
      `code` VARCHAR(45) NOT NULL,
      `name` VARCHAR(45) NOT NULL,
      `description` VARCHAR(255) NULL,
      `depth` INT NULL,
      `image` LONGTEXT NULL,
      PRIMARY KEY (`box_type_id`))
    {$charset};";

        dbDelta($sql);

        // BOX TYPE ATTRIBUTES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}box_type_attributes` (
      `box_type_id` INT NOT NULL,
      `attribute_id` INT NOT NULL,
      PRIMARY KEY (`box_type_id`, `attribute_id`))
    {$charset};";

        dbDelta($sql);

        // BOX TYPE RESTRICTIONS TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}box_type_restrictions` (
      `restrictions_id` INT NOT NULL AUTO_INCREMENT,
      `box_type_id` INT NOT NULL,
      `slug` VARCHAR(45) NULL,
      `dimension` VARCHAR(45) NULL,
      `min` DOUBLE NULL,
      `max` DOUBLE NULL,
      `intervals` DOUBLE NULL,
      `values` LONGTEXT NULL,
      PRIMARY KEY (`restrictions_id`))
    {$charset};";

        dbDelta($sql);

        // FLUTES TABLE
        $sql = "CREATE TABLE {$tbl_prefix}flutes (
      `flute_id` INT NOT NULL AUTO_INCREMENT,
      `flute` VARCHAR(45) NOT NULL,
      `paper` VARCHAR(45) NOT NULL,
      `colour` VARCHAR(45) NOT NULL,
      `wall` INT NOT NULL,
      `description` LONGTEXT NULL,
      `image` VARCHAR(255) NULL,
      `moq` VARCHAR(255) NULL,
      `equivalent` INT NULL,
      `staff_only` INT NOT NULL DEFAULT 0,
      PRIMARY KEY (`flute_id`))
    {$charset};";

        dbDelta($sql);

        // GRADES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}grades` (
      `grade_id` INT NOT NULL AUTO_INCREMENT,
      `grade` INT NULL,
      `staff_only` INT NOT NULL DEFAULT 0,     
      PRIMARY KEY (`grade_id`))
    {$charset};";

        dbDelta($sql);

        // BOARD PRICES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}board_prices` (
        `board_price_id` INT NOT NULL AUTO_INCREMENT,
        `flute_id` INT NOT NULL,
        `grade_id` INT NOT NULL,
        `min_qty` INT NULL,
        `max_qty` INT NULL,
        `price` DOUBLE NOT NULL,
        `stock` INT NOT NULL DEFAULT 0,
        PRIMARY KEY (`board_price_id`))
      {$charset};";

        dbDelta($sql);

        // BOARD WEIGHT TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}board_weight` (
      `board_weight_id` INT NOT NULL AUTO_INCREMENT,
      `flute_id` INT NOT NULL,
      `grade_id` INT NOT NULL,
      `board_weight` DOUBLE NOT NULL,
      `finished_weight` DOUBLE NOT NULL,
      PRIMARY KEY (`board_weight_id`))
    {$charset};";

        dbDelta($sql);

        // BOARD LENGTHS TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}board_lengths` (
      `board_length_id` INT NOT NULL AUTO_INCREMENT,
      `flute_id` INT NOT NULL,
      `length` DOUBLE NOT NULL,
      PRIMARY KEY (`board_length_id`))
    {$charset};";

        dbDelta($sql);

        // ALLOWANCES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}allowances` (
      `allowance_id` INT NOT NULL AUTO_INCREMENT,
      `flute_id` INT NOT NULL,
      `component` VARCHAR(45) NOT NULL COMMENT '‘BOX’ Or ‘LID’',
      `dimension` VARCHAR(45) NOT NULL COMMENT '‘LENGTH’ or ‘WIDTH’',
      `box_type_ids` VARCHAR(225) NULL COMMENT 'Comma separated list of box types this specifically applies to',
      `value` DOUBLE NOT NULL,
      PRIMARY KEY (`allowance_id`))
    {$charset};";

        dbDelta($sql);

        // LENGTH CALCULATIONS TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}length_calculations` (
      `lcalculation_id` INT NOT NULL AUTO_INCREMENT,
      `box_type_id` INT NOT NULL,
      `component` VARCHAR(45) NOT NULL COMMENT '‘BOX’ or ‘LID’',
      `formula` LONGTEXT NOT NULL,
      PRIMARY KEY (`lcalculation_id`))
    {$charset};";

        dbDelta($sql);

        // WIDTH CALCULATIONS TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}width_calculations` (
      `wcalculation_id` INT NOT NULL AUTO_INCREMENT,
      `box_type_id` INT NOT NULL,
      `component` VARCHAR(45) NOT NULL COMMENT '‘BOX’ or ‘LID’',
      `formula` LONGTEXT NOT NULL,
      PRIMARY KEY (`wcalculation_id`))
    {$charset};";

        dbDelta($sql);

        // BOX TYPE TIMES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}box_type_times` (
      `box_time_id` INT NOT NULL AUTO_INCREMENT,
      `box_type_id` INT NOT NULL,
      `stage` VARCHAR(45) NOT NULL,
      `qty` INT NOT NULL,
      `seconds` INT NOT NULL,
      PRIMARY KEY (`box_time_id`))
    {$charset}";

        dbDelta($sql);

        // OPTIONS TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}options` (
      `option_id` INT NOT NULL AUTO_INCREMENT,
      `meta_key` VARCHAR(45) NOT NULL,
      `meta_value` LONGTEXT NOT NULL,
      PRIMARY KEY (`option_id`))
    {$charset};";

        dbDelta($sql);

        // QUOTES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}quotes` (
      `quote_id` INT NOT NULL AUTO_INCREMENT,
      `customer_id` INT NULL COMMENT 'Who the quote is for',
      `user_id` INT NULL COMMENT 'Who produced the quote',
      `date_produced` DATETIME NOT NULL,
      `quoted_price` DOUBLE NOT NULL,
      `box_type_id` INT NOT NULL,
      `qty` INT NULL,
      `ref` VARCHAR(100) NULL COMMENT 'Allow the customer to give the quote a reference.',
      `object` LONGTEXT NULL COMMENT 'The object that was quoted for',
      PRIMARY KEY (`quote_id`))
    {$charset};";

        dbDelta($sql);

        // QUOTE ATTRIBUTES
        $sql = "CREATE TABLE `{$tbl_prefix}quote_attributes` (
      `quote_attribute_id` INT NOT NULL AUTO_INCREMENT,
      `quote_id` INT NOT NULL,
      `attribute` VARCHAR(100) NOT NULL,
      `value` VARCHAR(255) NOT NULL,
      PRIMARY KEY (`quote_attribute_id`))
    {$charset};";

        dbDelta($sql);

        //  BOUGHT IN BOXES TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}purchased_box` (
      `purchased_box_id` INT NOT NULL AUTO_INCREMENT,
      `rsc` VARCHAR(100) NOT NULL,
      `supplier` VARCHAR(255) NOT NULL,
      `reference` VARCHAR(100) NOT NULL,
      `file` VARCHAR(255) NULL,
      PRIMARY KEY (`purchased_box_id`))
    {$charset};";

        dbDelta($sql);

        //  BOUGHT IN BOXES TABLE - Prices
        $sql = "CREATE TABLE `{$tbl_prefix}purchased_box_prices` (
      `purchased_box_price_id` INT NOT NULL AUTO_INCREMENT,
      `purchased_box_id` INT NOT NULL,
      `qty` INT NOT NULL,
      `price` DOUBLE NOT NULL,
      PRIMARY KEY (`purchased_box_price_id`))
    {$charset};";

        dbDelta($sql);

        //  ARTWORK TABLE
        $sql = "CREATE TABLE `{$tbl_prefix}artwork` (
      `artwork_id` INT NOT NULL AUTO_INCREMENT,
      `file` VARCHAR(255) NOT NULL,
      `customer` INT NOT NULL,
      PRIMARY KEY (`artwork_id`))
    {$charset};";

        dbDelta($sql);

        // RSC Codes Table
        $sql = "CREATE TABLE `{$tbl_prefix}rsc_codes` (
      `rsc_id` INT NOT NULL AUTO_INCREMENT,
      `code` VARCHAR(100) NOT NULL,
      `box_type_id` INT NOT NULL,
      `grade` INT NOT NULL,
      `flute_id` INT NOT NULL,
      `length` INT NOT NULL,
      `width` INT NOT NULL,
      `height` INT NOT NULL,
      PRIMARY KEY (`rsc_id`))
    {$charset};";

        dbDelta($sql);
    }

}