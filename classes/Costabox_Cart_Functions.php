<?php

require_once __DIR__ . '/Costabox_Box.php';
require_once __DIR__ . '/Costabox_Pallet.php';
require_once __DIR__ . '/Costabox_Blank.php';
require_once __DIR__ . '/Costabox_Package.php';
require_once dirname(__DIR__) . '/odoo/class.php';

class Costabox_Cart_Functions
{

//    private int $API_VERSION = 1;
    private int $CUSTOM_PRODUCT_ID = 0;
    private int $FORME_PRODUCT_ID = 0;
    private int $PLATE_PRODUCT_ID = 0;
    private ?WP_User $user = null;
    private string $earliestDeliveryDate = "";

    public function __construct()
    {

        $this->CUSTOM_PRODUCT_ID = strval(costabox_get_setting("product_id"));
        $this->FORME_PRODUCT_ID = strval(costabox_get_setting("forme_product_id"));
        $this->PLATE_PRODUCT_ID = strval(costabox_get_setting("plate_product_id"));

        $this->user = wp_get_current_user();

        // Register the endpoints
        add_action('rest_api_init', [$this, 'register_endpoints']);

        add_filter("woocommerce_get_item_data", [$this, "item_data"], 10, 2);
        add_filter("woocommerce_cart_item_price", [$this, "item_price"], 10, 3);
        add_filter("woocommerce_cart_item_quantity", [$this, "item_quantity"], 10, 3);
        add_filter("woocommerce_cart_item_subtotal", [$this, "item_subtotal"], 10, 3);

//        add_action("woocommerce_after_cart_item_name", [$this, "re_add_box"], 10, 2);

        add_filter("woocommerce_add_cart_item_data", [$this, "add_unique_id"], 10, 3);

        add_action("woocommerce_before_calculate_totals", [$this, "calculate_totals"], 10, 1);

        add_filter("woocommerce_billing_fields", [$this, "billing_fields"], 100, 1);
        add_filter("woocommerce_shipping_fields", [$this, "shipping_fields"], 100, 1);
        add_action("woocommerce_after_order_notes", [$this, "delivery_date_field"], 10, 1);
        add_action("woocommerce_checkout_process", [$this, "validate_delivery_date_field"], 10, 1);
        add_filter("woocommerce_checkout_fields", [$this, "checkout_fields"], 10, 1);

        add_action("wp_head", [$this, "change_quantity_price"],1);
        add_action("wp_head", [$this, "add_to_cart"],1);

        add_action("woocommerce_checkout_create_order_line_item", [$this, "add_order_item_meta"], 10, 4);

        add_action("woocommerce_check_cart_items", [$this, "empty_cart_check"], 10);

        add_filter("woocommerce_cart_shipping_packages", [$this, "shipping_packages"], 10, 1);
        add_filter("woocommerce_shipping_package_name", [$this, "shipping_package_name"], 10, 3);
        add_filter("woocommerce_package_rates", [$this, "shipping_package_rates"], 50, 2);

        add_action("wp_enqueue_scripts", [$this, "enqueue_scripts"], 10);
        add_action("wp_enqueue_scripts", [$this, "enqueue_styles"], 10);

        add_action('wp_head', [$this, 'quote_form']);

        add_filter("woocommerce_coupons_enabled", [$this, "disable_coupons"]);
        add_action("woocommerce_cart_actions", [$this, "empty_cart_button"]);
        add_action("wp_head", [$this, "empty_cart"]);

        add_action("woocommerce_proceed_to_checkout", [$this, "override_shipping_costs"], 0);

        add_action("woocommerce_proceed_to_checkout", [$this, "send_quote_button"], 1);

        add_action("woocommerce_cart_item_removed", [$this, "remove_linked_items"], 10, 2);

        remove_action("woocommerce_cart_collaterals", ["WCB2BRP_Share_Cart", "wcb2brp_checkout_user_list"]);

//        add_filter("woocommerce_paypal_payments_checkout_button_renderer_hook", [$this, "dcc_renderer"], 10, 1);

    }

    public function dcc_renderer($renderer): string {
        return "woocommerce_after_checkout_form";
    }

    public function re_add_box($cart_item, $cart_item_key): void
    {
        if($cart_item['product_id'] != $this->CUSTOM_PRODUCT_ID) return;

        ?>

            <a href="<?php echo esc_url(home_url("instant_box_quote")); ?>?readd=<?php echo $cart_item_key; ?>" style="font-size: 90%; color: #A81711; text-decoration: underline !important; font-weight: 700"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-refresh-ccw"><polyline points="1 4 1 10 7 10"></polyline><polyline points="23 20 23 14 17 14"></polyline><path d="M20.49 9A9 9 0 0 0 5.64 5.64L1 10m22 4l-4.64 4.36A9 9 0 0 1 3.51 15"></path></svg>&nbsp;Load In Calculator</a>

        <?php

    }

    public function add_unique_id( array $cart_item_data, int $product_id, int $variation_id): array
    {
        if($product_id != $this->CUSTOM_PRODUCT_ID) return $cart_item_data;

        $cart_item_data['unique_key'] = md5(uniqid(rand(), true));

        return $cart_item_data;
    }

    public function register_endpoints(): void
    {
        register_rest_route('/costabox', '/add_to_cart', [
            'methods' => 'POST',
            'callback' => [$this, 'ajax_add_to_cart'],
        ]);

        register_rest_route('/costabox', '/save_box', [
            'methods' => 'POST',
            'callback' => [$this, 'save_box'],
        ]);
    }

    public function ajax_add_to_cart( WP_REST_Request $request ): WP_REST_Response
    {

        $boxObject = $request->get_param('box');

        /** @var Costabox_Box $customBox */
        $customBox = unserialize($boxObject);

        try {

            $boxReference = $_REQUEST['reference'];
            if($boxReference != null) {
                $customBox->setCustomerReference($boxReference);
            }

            $boxCartKey = WC()->cart->add_to_cart($this->CUSTOM_PRODUCT_ID, 1, 0, [], [
                'costabox_box' => $customBox,
            ]);

            // Add forme if a cost has been specified
            $formeCost = $_REQUEST['forme_cost'];
            if($formeCost > 0) {
                $forme_item = WC()->cart->add_to_cart($this->FORME_PRODUCT_ID, 1, 0, [], [
                    'price' => round($formeCost * 1.666, 0),
                    'box_key' => $boxCartKey,
                    'forme_code' => $_REQUEST['forme_code'],
                ]);

                WC()->cart->cart_contents[$boxCartKey]['forme_item'] = $forme_item;
            }

            // Add plate if a cost has been specified
            $plateCost = $_REQUEST['plate_cost'];
            if($plateCost > 0){
                $plate_item = WC()->cart->add_to_cart($this->PLATE_PRODUCT_ID, 1, 0, [], [
                    'price' => round($plateCost *  1.666, 0),
                    'box_key' => $boxCartKey,
                    'plate_code' => $_REQUEST['plate_code'],
                ]);

                WC()->cart->cart_contents[$boxCartKey]['plate_item'] = $plate_item;
            }

        }catch (Exception $e){
            return new WP_REST_Response([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }

        WC()->cart->set_session();
        WC()->cart->persistent_cart_update();

        return new WP_REST_Response([
            'success' => true,
            'cart_key' => $boxCartKey,
            "redirect_url" => wc_get_cart_url(),
        ]);
    }

    public function add_to_cart( ): void
    {

        if(!isset($_POST['box']) || !isset($_POST["add_costabox_to_cart"])) return;

        $boxObject = stripslashes($_POST['box']);
        $boxObject = json_decode($boxObject, true);

        $customBox = Costabox_Box::fromJSON($boxObject);


        try {

            $boxReference = $_POST['quote_ref'];
            if($boxReference != null) {
                $customBox->setCustomerReference($boxReference);
            }

            $boxCartKey = WC()->cart->add_to_cart($this->CUSTOM_PRODUCT_ID, 1, 0, [], [
                'costabox_box' => $customBox,
            ]);


            // Add forme if a cost has been specified
            $formeCost = $_REQUEST['forme_cost'];
            if($formeCost > 0) {
                $forme_item = WC()->cart->add_to_cart($this->FORME_PRODUCT_ID, 1, 0, [], [
                    'price' => round($formeCost * 1.666, 0),
                    'box_key' => $boxCartKey,
                    'forme_code' => $_REQUEST['forme_code'],
                ]);

                WC()->cart->cart_contents[$boxCartKey]['forme_item'] = $forme_item;
            }

            // Add plate if a cost has been specified
            $plateCost = $_REQUEST['plate_cost'];
            if($plateCost > 0){
                $plate_item = WC()->cart->add_to_cart($this->PLATE_PRODUCT_ID, 1, 0, [], [
                    'price' => round($plateCost *  1.666, 0),
                    'box_key' => $boxCartKey,
                    'plate_code' => $_REQUEST['plate_code'],
                ]);

                WC()->cart->cart_contents[$boxCartKey]['plate_item'] = $plate_item;
            }

        }catch (Exception $e){
            print_r($e->getMessage());
            die();
        }
    }

    public function save_box( WP_REST_Request $request ): WP_REST_Response
    {
        $boxObject = $request->get_param('box');

        $box = Costabox_Box::fromJSON($boxObject);
        $quoteId = $box->saveQuote();

        return new WP_REST_Response([
            'success' => true,
            'quote_id' => $quoteId,
        ]);
    }

    public function item_data($item_data, $cart_item): array
    {
        if(isset($cart_item['plate_code'])){
            $item_data[] = [
                'key' => "Code",
                'value' => $cart_item['plate_code'],
            ];
        }

        if(isset($cart_item['forme_code'])){
            $item_data[] = [
                'key' => "Code",
                'value' => $cart_item['forme_code'],
            ];
        }

        if(!isset($cart_item['costabox_box'])) {
            return $item_data;
        }

        /** @var Costabox_Box $box */
        $box = $cart_item['costabox_box'];

        $meta = [
            "RSC" => $box->getRSC(),
            "Box Type" => $box->getBoxType(),
        ];

        foreach($box->getBoardDetails() as $n => $detail){
            if($n == 'description'){
                continue;
            }

            $meta[ucfirst($n)] = $detail;
        }

        $meta['Length (mm)'] = $box->getLength();
        $meta['Width (mm)'] = $box->getWidth();
        $meta['Height (mm)'] = $box->getHeight();
        $meta["Quantity"] = $box->getQuantity();
        $meta["Printing"] = $box->isPrinted() ? "Yes" : "No";
        $meta["Handhole"] = $box->hasHandholes() ? "Yes" : "No";
        $meta["Est. Dispatch Date"] = $box->getDispatchDate();

        if($box->getCustomerReference()){
            $item_data[] = [
                'key' => "Reference",
                'value' => $box->getCustomerReference(),
            ];
        }

        foreach ($meta as $key => $value) {
            $item_data[] = [
                'key' => $key,
                'value' => $value,
            ];
        }

        return $item_data;
    }

    public function item_price($price, $cart_item, $cart_item_key): string
    {
        $product_id = $cart_item['data']->get_id();

        if($product_id == $this->FORME_PRODUCT_ID || $product_id == $this->PLATE_PRODUCT_ID){
            return wc_price($cart_item['price']);
        }

        if(!isset($cart_item['costabox_box'])) {
            return $price;
        }

        /** @var Costabox_Box $box */
        $box = $cart_item['costabox_box'];

        $root_user = Costabox_User::get_user();
        $is_staff = $root_user && Costabox_User::is_staff($root_user->ID);

        $price = number_format($box->getTotalPrice() / $box->getQuantity(), 2);

        if(!$is_staff) {
            return wc_price($price);
        }

        return "<input type='number' name='costabox_price_{$cart_item_key}' value='{$price}' step='0.01'><input type='hidden' name='costabox_price_{$cart_item_key}_original' value='{$price}'>";
    }

    public function item_quantity($quantity, $cart_item_key, $cart_item): string
    {
        if(!isset($cart_item['costabox_box'])) {
            return $quantity;
        }

        /** @var Costabox_Box $box */
        $box = $cart_item['costabox_box'];

        return woocommerce_quantity_input(
            [
                'input_name'   => "costabox_qty_{$cart_item_key}",
                'input_value'  => $box->getQuantity(),
                'max_value'    => 100000,
                'min_value'    => '0',
                'product_name' => "Custom Box Calculator"
            ],
            product: wc_get_product($this->CUSTOM_PRODUCT_ID),
            echo: false
        );
    }

    public function item_subtotal($subtotal, $cart_item, $cart_item_key): string
    {

        $product_id = $cart_item['data']->get_id();

        if($product_id == $this->FORME_PRODUCT_ID || $product_id == $this->PLATE_PRODUCT_ID){
            return wc_price($cart_item['price']);
        }

        if(!isset($cart_item['costabox_box'])) {
            return $subtotal;
        }

        /** @var Costabox_Box $box */
        $box = $cart_item['costabox_box'];

        $subtotal = $box->getTotalPrice();

        return wc_price($subtotal);

    }

    /**
     * Handle price and quantity changes of the custom box product
     */
    public function change_quantity_price(): void
    {
        if(!is_cart()) {
            return;
        }

        $cart = WC()->cart->get_cart_contents();
        $changesMade = false;

        foreach ($_POST as $key => $value){

            if(!str_starts_with($key, "costabox_")) {
                continue;
            }

            $cart_key = str_replace(["costabox_qty_", "costabox_price_", "[", "]"], "", $key);

            if(isset($cart[$cart_key]) && isset($cart[$cart_key]['costabox_box'])) {
                /** @var Costabox_Box $box */
                $box = $cart[$cart_key]['costabox_box'];

                if(str_starts_with($key, "costabox_qty")) {
                    $box->setQuantity($value);
                    $box->calculate();
                }else if(str_starts_with($key, "costabox_price_")) {
                    // Only change the price if the user has changed it
                    if($value != $_POST["costabox_price_{$cart_key}_original"]) {
                        $box->overridePrice($value);
                    }
                }

                $changesMade = true;
                WC()->cart->cart_contents[$cart_key]['costabox_box'] = $box;
            }
        }

        if($changesMade) {
            WC()->cart->set_session();
        }
    }

    public function calculate_totals($cart): void
    {
        foreach ($cart->get_cart_contents() as $key => $item) {
            /** @var WC_Product $product */
            $product = $item['data'];

            if($product->get_id() == $this->PLATE_PRODUCT_ID || $product->get_id() == $this->FORME_PRODUCT_ID){
                $cart->cart_contents[$key]['data']->set_price($item['price']);
                continue;
            }

            if(!isset($item['costabox_box'])) {
                continue;
            }

            /** @var Costabox_Box $box */
            $box = $item['costabox_box'];

            $cart->cart_contents[$key]['data']->set_price($box->getTotalPrice());
            $cart->cart_contents[$key]['data']->set_weight($box->getWeight());
        }
    }

    public function add_order_item_meta($item, $cart_item_key, $values, $order): void
    {
        if(!isset($values['costabox_box'])) {
            return;
        }

        /** @var Costabox_Box $box */
        $box = $values['costabox_box'];

        $customer_reference = $box->getCustomerReference();

        if($customer_reference) {
            $item->add_meta_data("Reference", $customer_reference);
        }

        $item->add_meta_data("RSC", $box->getRSC());
        $item->add_meta_data("Box Type", $box->getBoxType());

        foreach($box->getBoardDetails() as $n => $detail){
            $item->add_meta_data(ucfirst($n), $detail);
        }

        $item->add_meta_data('Length (mm)', $box->getLength());
        $item->add_meta_data('Width (mm)', $box->getWidth());
        $item->add_meta_data('Height (mm)', $box->getHeight());
        $item->add_meta_data("Quantity", $box->getQuantity());
        $item->add_meta_data("Printing", $box->isPrinted() ? "Yes" : "No");
        $item->add_meta_data("Handhole", $box->hasHandholes() ? "Yes" : "No");
        $item->add_meta_data("Est. Delivery Date", $box->getDispatchDate());

        // Add data for Odoo
        foreach ($box->getOdooDetails() as $key => $value) {
            $item->add_meta_data($key, $value);
        }

        if(array_key_exists("plate_code", $values)){
            $item->add_meta_data("_Plate Code", $values['plate_code']);
        }

        if(array_key_exists("forme_code", $values)){
            $item->add_meta_data("_Forme Code", $values['forme_code']);
        }

    }

    public function empty_cart_check(): void
    {
        if ( WC()->cart->get_cart_contents_count() == 0 || WC()->cart->is_empty()) {
            WC()->session->set('qbcb_shipping_cost', '');
        }
    }

    public function billing_fields( array $fields ): array
    {

        $fields['billing_company']['required'] = true;
        $fields['billing_company']['description'] = "<small>Please enter 'Personal' if not an organisation.</small>";

        if($this->user && function_exists('wcb2brp_is_b2b_user')){
            if(wcb2brp_is_b2b_user($this->user->ID)){
                $company_id = get_user_meta($this->user->ID, 'wcb2brp_company', true);

                $fields['billing_company']['description'] = '';
                $fields['billing_company']['default'] = get_the_title($company_id);
                $fields['billing_company']['custom_attributes'] = ['readonly' => 'readonly'];

                foreach ($fields as $field => &$data) {
                    $default = get_post_meta($company_id, 'qbcb_' . $field, true);

                    $data['default'] = $default;
                }
            }
        }

        $fields['billing_phone']['label'] .= ' (Best contact number for when we deliver.)';

        return $fields;
    }

    public function shipping_fields( array $fields ): array
    {

        if($this->user && function_exists('wcb2brp_is_b2b_user')){
            if(wcb2brp_is_b2b_user($this->user->ID)){
                $company_id = get_user_meta($this->user->ID, 'wcb2brp_company', true);

                foreach ($fields as $field => &$data) {
                    $default = get_post_meta($company_id, 'qbcb_' . $field, true);

                    $data['default'] = $default;
                }
            }
        }

        return $fields;
    }

    public function checkout_fields( array $fields ): array
    {

        $fields['order']['order_comments']['label'] .= '<br><span class="important_order_notes">Please include any delivery requirements including tail lift & specific vehicle requirements, if these are not included in the order notes we cannot be held responsible for the charge of a redelivery if delivery cannot take place.</span>';
        $fields['order']['order_comments']['placeholder'] .= ' (optional)';

        return $fields;
    }

    private function get_earliest_delivery_date(): string
    {
        $earliestDate = null;

        if(!WC()->cart){
            return strtotime("+1 day");
        }

        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item){
            /** @var WC_Product $product */
            $product = $cart_item['data'];

            if($product->get_id() != $this->CUSTOM_PRODUCT_ID || !isset($cart_item['costabox_box'])){
                continue;
            }

            /** @var Costabox_Box $box */
            $box = $cart_item['costabox_box'];

            if(!$box){
                continue;
            }

            $dispatchDate = $box->getDispatchDate(false);

            if($earliestDate == null || $dispatchDate < $earliestDate){
                $earliestDate = $dispatchDate;
            }
        }

        return $earliestDate ?? strtotime("+1 day");
    }

    public function delivery_date_field( $checkout ): void
    {

        $earliestDate = $this->earliestDeliveryDate;

        woocommerce_form_field('qbcb_preferred_delivery_date', [
            'type'  => 'text',
            'class' => [
                'form-row-wide',
            ],
            'label'             => __('Preferred Delivery Date:'),
            'required'          => false,
            'custom_attributes' => ['min' => date("Y-m-d", $earliestDate), 'id' => 'qbcb_preferred_delivery_date']],
        date("d/m/Y", $earliestDate));

        // Add Javascript to add restrictions to the date field
        ?>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                $('#qbcb_preferred_delivery_date').datepicker({
                    dateFormat: 'dd/mm/yy',
                    minDate: new Date('<?php echo date("Y-m-d", $earliestDate); ?>'),
                    beforeShowDay: qbcbAllowedDates
                });

                Date.prototype.yyyymmdd = function() {
                    var mm = this.getMonth() + 1; // getMonth() is zero-based
                    var dd = this.getDate();

                    return [this.getFullYear(),
                        (mm>9 ? '' : '0') + mm,
                        (dd>9 ? '' : '0') + dd
                    ].join('-');
                };

                function qbcbAllowedDates(date){
                    var noWeekend = $.datepicker.noWeekends(date);

                    if(noWeekend[0]){
                        var dates = '<?php echo costabox_get_setting("excluded_delivery_dates"); ?>';


                        if( $.inArray(date.yyyymmdd(), dates.split(',')) !== -1 ){
                            return [false, 'bh']
                        }else{
                            return [true, '']
                        }
                    }else{
                        return noWeekend;
                    }

                }
            });

        </script>


        <?php

    }

    public function validate_delivery_date_field( ): void
    {
        if (empty( $_POST['qbcb_preferred_delivery_date'] )) {
            return;
        }

        $date = DateTime::createFromFormat('d/m/Y', $_POST['qbcb_preferred_delivery_date']);

        if($date->getTimestamp() < $this->earliestDeliveryDate){
            wc_add_notice( __( 'Unfortunately we won\'t be able to deliver these items before ' . date("d/m/Y", $this->earliestDeliveryDate) ), 'error' );
        }
    }

    public function shipping_packages( array $packages ): array
    {

        $regular_packages = [];
        $custom_packages = [];

        $hasPalletItems = false;

        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item){
            /** @var WC_Product $product */
            $product = $cart_item['data'];

            if(!$product->needs_shipping()){
                continue;
            }

            if($product->get_id() == $this->CUSTOM_PRODUCT_ID || $product->get_shipping_class() == "pallets"){
                $hasPalletItems = true;
                $custom_packages[$cart_item_key] = $cart_item;
            }else{
                $regular_packages[$cart_item_key] = $cart_item;
            }

        }

        $package_contents = $hasPalletItems ? $custom_packages : $regular_packages;

        return [
            [
                'contents' => $package_contents,
                'contents_cost' => array_sum(wp_list_pluck($package_contents, 'line_total')),
                'applied_coupons' => WC()->cart->get_applied_coupons(),
                'user' => [
                    'ID' => $this->user ? $this->user->ID : 0,
                ],
                'destination' => [
                    'country' => WC()->customer->get_shipping_country(),
                    'state' => WC()->customer->get_shipping_state(),
                    'postcode' => WC()->customer->get_shipping_postcode(),
                    'city' => WC()->customer->get_shipping_city(),
                    'address' => WC()->customer->get_shipping_address(),
                    'address_2' => WC()->customer->get_shipping_address_2(),
                ],
            ]
        ];

    }

    public function shipping_package_name( string $name ): string
    {
        return ($name == "Shipping 2") ? "Pallets" : $name;
    }

    public function shipping_package_rates( array $rates, array $package ): array
    {
        $hasPalletItems = false;
        $productCount = 0;
        $shipPallets = false;
        $totalWeight = 0;
        $boxes = [];

        // Max dimensions for a package
        $package_length = 1300;
        $package_width  = 1790;

        foreach($package['contents'] as $cart_item_key => $cart_item){
            /** @var WC_Product $product */
            $product = $cart_item['data'];

            if(!$product->needs_shipping()){
                continue;
            }

            if($product->get_id() == $this->CUSTOM_PRODUCT_ID || $product->get_shipping_class() == "pallets"){
                $hasPalletItems = true;
            }

            if($product->get_id() != $this->CUSTOM_PRODUCT_ID){
                if($product->has_dimensions()){
                    $boxes[] = ['l' => $product->get_length() * 10, 'w' => $product->get_width() * 10, 'd' => $product->get_height() * 10,  'qty' => $cart_item['quantity']];
                    $productCount += $cart_item['quantity'];
                    $shipPallets = true;
                }
            }else{
                /** @var Costabox_Box $customBox */
                $customBox = $cart_item['costabox_box'];
                $customBox->calculateAreaRequired();

                $thisBox = [
                    'l' => $customBox->getChop() - ($customBox->isDieCut() ? 50 : 0),
                    'w' => $customBox->getDeckle() - ($customBox->isDieCut() ? 50 : 0),
                    'qty' => $customBox->getQuantity(),
                    'weight' => $customBox->getWeight() / $customBox->getQuantity(),
                    'd' => $customBox->getDepth(),
                ];

                $isFolded = !in_array($customBox->getBoxType(), ['0300','0401','0409', '0410', '0411', '0452', 'PAD', "DIE-CUT"]);

                if($isFolded){
                    $thisBox['d'] *= 2; // The blank is folded in half
                    $thisBox['l'] /= 2;
                }

                $boxes[] = $thisBox;

                if($customBox->getLidDeckle() > 0){
                    $dimensions = [$customBox->getLidChop(), $customBox->getLidDeckle()];
                    $length = min($dimensions);
                    $width = max($dimensions);

                    $boxes[] = [
                        'l' => $length,
                        'w' => $width * ($isFolded ? 0.5 : 1),
                        'qty' => $customBox->getQuantity(),
                        'weight' => $customBox->getWeight() / $customBox->getQuantity(),
                        'd' => $customBox->getDepth() * ($isFolded ? 2 : 1),
                    ];
                }

                // Check to see if we need to ship pallets or if a package will do
                if (($thisBox['l'] >= $package_length || $thisBox['w'] >= $package_width) && ($thisBox['l'] >= $package_width || $thisBox['w'] >= $package_length)) {
                    $shipPallets = true;
                }

                $productCount += 1;
                $totalWeight += $customBox->getWeight();
            }
        }

        if(count($boxes) == 0){
            foreach ($rates as $rk => $r) {
                if ($r->method_id == 'qbcb_courier') {
                    unset($rates[$rk]);
                }
            }

            return $rates;
        }

        if($totalWeight > 50){
            $shipPallets = true; // It's more economical to ship pallets than a package
        }

   //     $shipPallets = true; // Always ship pallets

        $has_courier = false;
        $has_pallets = false;

        foreach ($rates as $rate) {
            if ($rate->method_id == 'qbcb_courier') {
                $has_courier = true;
            }
            if ($rate->method_id == 'flat_rate') {
                $has_pallets = true;
            }
        }

        $canCollect = $this->user && Costabox_User::can_collect($this->user->ID);
        $overiddenCost = WC()->session->get('qbcb_shipping_cost');

        $localPickupMethod = [];

        foreach($rates as $rate_key => $rate){

            if($rate->method_id == 'local_pickup'){
                $localPickupMethod[$rate_key] = $rate;
                if(!$canCollect){
                    unset($rates[$rate_key]);
                    continue;
                }
            }

            if($rate->method_id == 'free_shipping'){
                continue; // If free shipping is available, we don't need to calculate anything
            }

            // If the cost has been overidden, we don't need to calculate anything
            if($overiddenCost){
                $cost = $overiddenCost == 9999 ? 0 : $overiddenCost;

                $rates[$rate_key]->cost = $cost;

                $taxes = [];
                foreach ($rate->taxes as $key => $tax) {
                    if ($rate->taxes[$key] > 0) {
                        $taxes[$key] = $cost * 0.2;
                    }
                }
                $rates[$rate_key]->taxes = $taxes;

                continue;
            }

            $palletCost = $rate->cost / $productCount;

            try {
                $packed = $this->calculateContainersRequired($boxes, $shipPallets ? "pallet" : "package");
            }catch(\Exception $e){
                try {
                    $packed = $this->calculateContainersRequired($boxes);
                    $shipPallets = true;
                }catch(\Exception $e){
                    unset($rates[$rate_key]);
                    continue;
                }
            }

            $totalPallets = $packed['total'];

            $packageText = self::pluralize($totalPallets,$shipPallets ? "pallet" : "package");

            $label = $totalPallets . " " . $packageText;

            if($totalPallets > 1 && $packed['volume'] > 0){
                $label .= ", Last " . $packed['volume'] . "% Full";

            }

            $rates[$rate_key]->label .= "\n ({$label})";

            if($shipPallets){
                $discount = match ($totalPallets){
                    1, 2, 3 => 0,
                    4 => 0.1,
                    5 => 0.13,
                    default => 0.15,
                };

                $rates[$rate_key]->cost = $palletCost * $totalPallets * (1 - $discount);


                // Set taxes rate cost (if enabled)
                $taxes = [];
                foreach ($rates[$rate_key]->taxes as $key => $tax) {
                    if ($rates[$rate_key]->taxes[$key] > 0) {
                        $taxes[$key] = $rates[$rate_key]->cost * 0.2;
                    }
                }
                $rates[$rate_key]->taxes = $taxes;
            }

        }

        // Disable unnecessary shipping methods
        foreach ($rates as $rate_key => $rate) {
            if ($rate->method_id == 'qbcb_courier') {
                if (!$hasPalletItems || ($has_courier)) {
                    unset($rates[$rate_key]);
                }
            }

            if ($rate->method_id == 'flat_rate') {
                if (!$hasPalletItems && ($has_courier && $has_pallets)) {
                    unset($rates[$rate_key]);
                }
            }
        }

        if(count($rates) == 0){
            wc_clear_notices();
            wc_add_notice("Unfortunately, we have no shipping options available based on your location and size of order. Please contact us or arrange your own collection.", 'error');
            $rates = $localPickupMethod;
        }

        return $rates;
    }

    private function calculateContainersRequired( array $boxes, string $container = "pallet" ): array
    {

        $packer = new \DVDoug\BoxPacker\Packer();
        $packer->setMaxBoxesToBalanceWeight(0);

        $costabox_package = new Costabox_Package();

        $maxHeight = 0;

        if($container == "pallet") {
            $packer->addBox(new Costabox_Pallet());
            $packer->addBox(new Costabox_Pallet(1, 2));
            $packer->addBox(new Costabox_Pallet(1, 3));
            $packer->addBox(new Costabox_Pallet(2, 1));
            $packer->addBox(new Costabox_Pallet(2, 2));
            $packer->addBox(new Costabox_Pallet(2, 3));

            $maxHeight = Costabox_Pallet::getHeight();
        }else{
            $packer->addBox($costabox_package);

            $maxHeight = Costabox_Package::getHeight();
        }

        $toPack = [];

        if($maxHeight > 0){

            // In order to minimise the number of oibjects we need to pack,
            // we need to group boxes by stacking them as tall as possible
            foreach ($boxes as $box){

                $stackSize = floor($maxHeight / $box['d']);

                if($container == "package" && $box['weight'] > 0){
                    $stackSize = min($stackSize, floor($costabox_package->getMaxWeight() / ($box['weight'] * 1000)));
                }

                $piles = ceil($box['qty'] / $stackSize);
                $remaining = $box['qty'];

                for($i = 0; $i < $piles; $i++){
                    $qty = min($remaining, $stackSize);
                    $remaining -= $qty;

                    $toPack[] = [
                        'd' => $box['d'] * $qty,
                        'w' => $box['w'],
                        'l' => $box['l'],
                        'qty' => 1,
                        'weight' => $box['weight'] * $qty,
                    ];
                }

            }
        }else{
            $toPack = $boxes;
        }

//        print_r($toPack); die();

        foreach ($toPack as $box){
            $packer->addItem(new Costabox_Blank($box['w'], $box['l'], $box['d'], $box['weight']), $box['qty']);
        }

        // Run the packing algorithm
        $packedBoxes = $packer->pack();

        $results = [
            'total' => 0,
            'oversized' => 0,
            'volume' => $packedBoxes->getVolumeUtilisation(),
        ];

        if($container == "pallet") {
            foreach ($packedBoxes as $packedBox) {
                $results['total'] += $packedBox->getBox()->getOversized();

                if ($packedBox->getBox()->getOversized() > 1) {
                    $results['oversized']++;
                }
            }
        }else{
            $results['total'] = $packedBoxes->count();
        }

        return $results;

    }

    public function enqueue_scripts(): void
    {
        if(is_checkout()){

            wp_enqueue_script('jquery-ui-datepicker');

            wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
            wp_enqueue_style('jquery-ui');

        }

        if(is_cart()){

            wp_register_script('qbcb_cart', plugin_dir_url(dirname(__FILE__)) . '/js/cart.js', in_footer: true);

            wp_localize_script('qbcb_cart', 'products', [
                'forme' => $this->FORME_PRODUCT_ID,
                'plate' => $this->PLATE_PRODUCT_ID
            ]);

            wp_enqueue_script('qbcb_cart');
        }
    }

    public function enqueue_styles(): void
    {
        if(is_cart()){
            wp_register_style('qbcb_cart', plugin_dir_url(dirname(__FILE__)) . '/css/cart.css');
            wp_enqueue_style('qbcb_cart');
        }
    }


    /**
     * Pluralizes a word if quantity is not one.
     *
     * @param int $quantity Number of items
     * @param string $singular Singular form of word
     * @param string|null $plural Plural form of word; function will attempt to deduce plural form from singular if not provided
     * @return string Pluralized word if quantity is not one, otherwise singular
     */
    private static function pluralize(int $quantity, string $singular, ?string $plural = null): string
    {
        if($quantity==1 || !strlen($singular)) return $singular;
        if($plural!==null) return $plural;

        $last_letter = strtolower($singular[strlen($singular)-1]);
        return match ($last_letter) {
            'y' => substr($singular, 0, -1) . 'ies',
            's' => $singular . 'es',
            default => $singular . 's',
        };
    }


    public function disable_coupons( $enabled ): bool
    {
        if ( is_cart() || is_checkout() ) {
            $enabled = false;
        }
        return $enabled;
    }

    public function empty_cart_button(): void {
        if(WC()->cart->get_cart_contents_count() === 0) {
            return;
        }

        if(!WC()->session)
            return;

        $cartId = WC()->session->get('qbcb_cart_id');

        $url = wp_nonce_url( wc_get_cart_url(), 'woocommerce-empty-cart' . $cartId, 'empty-cart' );

        echo '<a class="button uk-button" href="' . esc_url( $url ) . '">Empty Cart</a>';
    }

    public function empty_cart(): void {


        $this->earliestDeliveryDate = $this->get_earliest_delivery_date();

        if(!WC()->session)
            return;

        $cartId = WC()->session->get('qbcb_cart_id');

        if ( isset( $_GET['empty-cart'] ) && wp_verify_nonce( $_GET['empty-cart'], 'woocommerce-empty-cart' . $cartId) ) {
            WC()->cart->empty_cart();

            $cartId = uniqid();
            WC()->session->set('qbcb_cart_id', $cartId);
            WC()->session->set('qbcb_lead_id', "");
            WC()->session->save_data();

        }
    }

    /**
     * The following functions are related to allowing users to send quotes
     */
    public function quote_form(): void
    {
        if(!is_cart() || WC()->cart->is_empty()){
            return;
        }

        $this->email_quote();

        $emailAddress = $this->user ? $this->user->get("billing_email") : "";
        $phoneNumber = $this->user ? $this->user->get("billing_phone") : "";
        $firstName = $this->user ? $this->user->get("billing_first_name") : "";
        $lastName = $this->user ? $this->user->get("billing_last_name") : "";
        $actionUrl = esc_url(wp_nonce_url(add_query_arg(['email-quote' => '1'], wc_get_cart_url()), 'qbcb-cart-email'));

        ?>

        <a class="modal-link" id="quote-pull-out-toggle" href="#quote-pull-out" >Send a quote</a>

        <div id="quote-pull-out" uk-modal>
            <div class='uk-modal-dialog uk-modal-body'>
                <h2 class='uk-modal-title'>Email Quote</h2>

                <form method="POST" class="qbcb_email_quote"
                      action="<?php echo $actionUrl; ?>">
                    <div style="width: 45%; margin-right: 5%; display: inline-block">
                        <label for="quote_fname">First Name</label>
                        <input type="text" name="quote_fname" id="quote_fname" value="<?php echo $firstName; ?>" required="required"/>
                    </div><div style="width: 50%; display: inline-block">
                        <label for="quote_lname">Last Name</label>
                        <input type="text" name="quote_lname" id="quote_lname" value="<?php echo $lastName; ?>" required="required" />
                    </div>
                    <label for="quote_email">Email</label>
                    <input type='email' placeholder='Enter your email address'
                           value='<?php echo $emailAddress; ?>' name='quote_email' required="required">
                    <label for="quote_phone">Phone (Optional)</label>
                    <input type='tel' placeholder='Enter your phone number' value='<?php echo $phoneNumber; ?>' name='quote_phone'>
                    <label for="optional_text">Optional Text</label>
                    <textarea name="optional_text" value=""></textarea>
                    <input type="submit" name="email_quote" class="uk-button uk-button-primary button" value="Email Quote" id="email_quote">
                    <input type="hidden" name="email-quote" value="1">
                </form>

            </div>
        </div>

        <?php
    }

    private function email_quote(): void
    {
        if (!isset($_POST['email-quote']) || !is_cart() || WC()->cart->is_empty()) {
            return;
        }

        if (!isset($_GET['_wpnonce']) || !wp_verify_nonce($_GET['_wpnonce'], 'qbcb-cart-email')) {
            wc_add_notice('Unable to email your quote. Please try again.', 'error');
            return;
        }

        $email_address = sanitize_email($_POST['quote_email']);
        $text          = esc_attr($_POST['optional_text']);
        $fname         = esc_attr($_POST['quote_fname']);
        $lname         = esc_attr($_POST['quote_lname']);
        $phone         = esc_attr($_POST['quote_phone']);

        if (empty($email_address) || !is_email($email_address)) {
            wc_add_notice('Please enter a valid email address to send quote to.', 'error');

            return;
        }

        $options      = get_option('costabox_quote_email_options', true);
        $heading      = $options['title'];
        $message      = $options['message'];
        $message      = str_replace('{first_name}', $this->user->first_name, $message);
        $message      = str_replace('{cart_url}', '<a href="' . get_site_url(null, 'checkout') . '">checkout</a>', $message);

        if (!empty($text)) {
            $message .= '<br/><br/>Additional Text<br/>' . $text . '<br/>';
        }

        $pdf        = new CostaboxQuoteToPpf(plugin_dir_path(dirname(__FILE__)));
        $attachment = $pdf->create_quote_pdf($text);

        $mailer          = WC()->mailer();
        $wrapped_message = $mailer->wrap_message($heading, $message);
        $wc_email        = new WC_Email;
        $html_message    = $wc_email->style_inline($wrapped_message);

        wp_mail($email_address, 'Your quote - ' . date('d/m/Y H:i'), $html_message, ['Content-Type: text/html; charset=UTF-8'], $attachment);


        /* Add a lead to Odoo */
        $contact = [
            "first_name" => $fname,
            "last_name" => $lname,
            'email' => $email_address,
            'phone' => $phone,
        ];

        $odoo = new Costabox_Odoo();
        $odoo->createLead($contact);
    }

    public function override_shipping_costs(): void
    {
        if(!$this->user || !Costabox_User::is_staff(Costabox_User::get_user()->ID)){
            return;
        }

        if (isset($_POST['qbcb_shipping_cost'])) {
            WC()->session->set('qbcb_shipping_cost', $_POST['qbcb_shipping_cost']);
        }

        foreach (WC()->shipping->get_packages() as $k => $p) {
            WC()->session->set("shipping_for_package_$k", '');
        }
        ?>

        <form method="POST" class="qbcb_shipping_cost" action="<?php echo esc_url( wc_get_cart_url() ); ?>">

            <div>
                <input type="hidden" name="calc_shipping" value="1">
                <label>Override Shipping Cost (£): </label><input type="number" name="qbcb_shipping_cost" step="0.01" min="0"
                                                                  value="<?php echo WC()->session->get('qbcb_shipping_cost'); ?>">
                <input class="uk-button uk-button-primary" type="submit" name="qbcb_shipping_update"
                       value="Update Shipping Cost">
            </div>
        </form>


        <script type="text/javascript">

            jQuery(document).ready(function($){

                $( "form.qbcb_shipping_cost" ).on( "submit", function(e) {
                    let dataString = $(this).serialize();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo esc_url( wc_get_cart_url() ); ?>",
                        data: dataString,
                        success: function () {
                            let $form = $( 'form.woocommerce-shipping-calculator' );

                            // Provide the submit button value because wc-form-handler expects it.
                            $( '<input />' ).attr( 'type', 'hidden' )
                                .attr( 'name', 'calc_shipping' )
                                .attr( 'value', '1' )
                                .appendTo( $form );

                            location.href = "<?php echo esc_url( wc_get_cart_url() ); ?>";
                        }
                    });

                    e.preventDefault();
                });
            });

        </script>

        <?php
    }

    public function send_quote_button(): void
    {
        if(!is_cart() || WC()->cart->is_empty()){
            return;
        }

        echo '<a style="margin: 10px 0; width: 100%;" href="#quote-pull-out" class="modal-link uk-button uk-button-primary">Email Quote</a>';
    }

    public function remove_linked_items(): void
    {

        $keysToRemove = [$_REQUEST['remove_item']];
        $metaKeys = ['box_key', 'plate_item', 'forme_item'];

        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

            foreach($metaKeys as $metaKey){
                if(isset($cart_item[$metaKey])){

                    if(in_array($cart_item_key, $keysToRemove)){
                        $keysToRemove[] = $cart_item[$metaKey];
                    }elseif (in_array($cart_item[$metaKey], $keysToRemove)){
                        $keysToRemove[] = $cart_item_key;
                    }

                }
            }

        }

        foreach(array_unique($keysToRemove) as $key){
            WC()->cart->remove_cart_item($key);
        }

    }

    // TODO: Make subtotal work for costabox

}