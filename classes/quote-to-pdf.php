<?php

if (!class_exists(CostaboxQuoteToPpf::class)) {
    require_once __DIR__ . '../../vendor/autoload.php';
    class CostaboxQuoteToPpf
    {
        protected $mpdf;

        protected $uploadsFolder;
        protected $cartId;

        protected $baseurl;

        protected $options;

        public function __construct($baseurl)
        {
            $this->mpdf          = new \Mpdf\Mpdf();
            $uploads_dir         = wp_upload_dir();
            $this->uploadsFolder = $uploads_dir['basedir'] . '/costabox/';
            $this->baseurl       = $baseurl;
            $this->options       = get_option('costabox_quote_email_options', true);
            $session             = WC()->session;

            // Create unique cart id
            $this->cartId = $session->get('qbcb_cart_id');
            if (empty($this->cartId)) {
                $this->cartId = uniqid();
                $session->set('qbcb_cart_id', $this->cartId);
                $session->save_data();
            }
        }

        public function create_quote_pdf($additional_text = '')
        {
            $filename   = $this->cartId . '.pdf';
            $stylesheet = file_get_contents($this->baseurl . 'admin/css/quote-pdf.css');

            $html = $this->header_html();
            $html .= $this->message_html($additional_text);
            $html .= $this->cart_html();
            $html .= $this->footer_html();

            $this->mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $this->mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);

            $this->mpdf->output($this->uploadsFolder . $filename, 'F');
            $attachment = $this->uploadsFolder . $filename;

            return $attachment;
        }

        public function create_quote_custom_pdf($additional_text = '', $to_quote, $box_description)
        {
            $filename   = 'quickbox-quote-' . date('d-m-Y') . '-' . date('H:i') . '.pdf';
            $stylesheet = file_get_contents($this->baseurl . 'admin/css/quote-pdf.css');

            $html = $this->header_html();
            $html .= $this->message_html($additional_text);
            // $html .= $this->cart_html();
            $html .= $this->custom_cart_html($to_quote, $box_description);
            $html .= $this->footer_html();
            // print_r($html);
            // exit;

            $this->mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $this->mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);

            $this->mpdf->output($this->uploadsFolder . $filename, 'F');
            $attachment = $this->uploadsFolder . $filename;

            return $attachment;
        }

        protected function header_html()
        {
            $html = '<div id="header"><img src="' . $this->baseurl . 'img/quote-header.png" width="960"><div>';

            return $html;
        }

        protected function footer_html()
        {
            $html = '<div id="footer"><img src="' . $this->baseurl . 'img/quote-footer.png" width="960"><div>';

            return $html;
        }

        protected function message_html($additional_text)
        {
            $current_user = wp_get_current_user();
            $message      = $this->options['message'];
            $message      = str_replace('{first_name}', $current_user->first_name, $message);
            $message      = str_replace('{cart_url}', '<a href="' . get_site_url(null, 'checkout') . '">checkout</a>', $message);

            $html = '<section>';
            $html .= '<p id="message">' . $message . '</p>';
            if ($additional_text != '') {
                $html .= '<p id="message-additional-text">';
                $html .= $additional_text;
                $html .= '</p>';
            }
            $html .= '</section>';

            return $html;
        }

        protected function cart_html()
        {
            $html = '<section>';
            $html .= wc_get_template_html('cart/quote-pdf.php');
            $html .= '</section>';

            return $html;
        }

        protected function custom_cart_html($box_details, $box_description){

            

            $unit_price = explode(",",($box_details['price_break_unit_price']));
            $price_break_fixed = (array)json_decode(stripslashes($box_details['price_break']));
            $price_break_key = array_keys($price_break_fixed);
            $prev_unit = 0;

            foreach($price_break_key as $key => $value){
            
                // $unit_price = number_format($price_break_fixed[$value] / $value , 2);
            
                // if ($prev_unit == $unit_price[$key])continue;

                if($key > 2)break;
                
                $prev_unit = $unit_price;

                $price_break_table_qty .= '<th>' . $value . '+</th>';
                $price_break_table_unit .= '<td>£' . $unit_price[$key] . '</td>';
                
            }

            ob_start();
            ?>
            <form class="woocommerce-cart-form" action="https://royal-shoe.flywheelstaging.com/shopping-cart/" method="post">
                <h2> Your Costabox Custom Made Cardboard</h2>
                <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="product-name">Product</th>
                            <th class="product-price">Price</th>
                            <th class="product-quantity">Quantity</th>
                            <th class="product-subtotal">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr class="woocommerce-cart-form__cart-item cart_item">

                            <td class="product-name"
                                data-title="Product">
                                <p><?= $box_description?><p>				
                            </td>

                            <td class="product-price"
                                data-title="Price">
                                <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&pound;</span><?= $box_details['price_per_box'] ?></bdi></span>
                            </td>

                            <td class="product-quantity" data-title="Quantity">
                                <?= $box_details['box_qty'] ?>  				
                            </td>

                            <td class="product-subtotal"
                                data-title="Subtotal">
                                <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&pound;</span><?= $box_details['quote_price'] ?></bdi></span>				
                            </td>
                        </tr>
                        <tr class="price_breaks_row">
                            <td colspan="4" class="price_breaks" style="border-bottom: 1px solid #e2e2e2">
                                <table>
                                    <tr>
                                        <th>Price Break: </th>
                                        <?= $price_break_table_qty?>
                                    </tr>
                                    <tr>
                                        <th>Unit Price:</th>
                                        <?=$price_break_table_unit?>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" class="shop_table shop_table_responsive quote-totals" style="width: 100%">

                    <tr class="cart-subtotal">
                        <th style="width: 50%;">Subtotal</th>
                        <td data-title="Subtotal"
                            style="width: 50%;">
                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&pound;</span><?=$box_details['quote_price']?></bdi></span>
                        </td>
                    </tr>

                    
                    <!-- <tr class="cart-shipping">
                        <th>Shipping</th>
                        <td
                            data-title="Shipping">
                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&pound;</span>0.00</bdi></span>
                        </td>
                    </tr> -->

                    <tr class="tax-rate tax-rate-gb-vat-1">
                        <th>VAT</th>
                        <td data-title="VAT">
                            <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span><?=$box_details['price_per_box']?></span>
                        </td>
                    </tr>
                    
                    <tr class="order-total">
                        <th>Total</th>
                        <td
                            data-title="Total">
                            <strong><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&pound;</span><?=$box_details['quote_price']?></bdi></span></strong>
                        </td>
                    </tr>
                </table>
            </form>
    <?php
   return ob_get_clean();
        }
    }
}
