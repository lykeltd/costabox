## 27 October ##

- Added initial version of die cut boxes.
- Added the ability to treat price of regular products as cost price.

## 14 April 2023 ##

Refactored the majority of code, converting to OOP