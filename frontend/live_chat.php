<?php

class Costabox_Odoo_Live_Chat{
    public function __construct()
    {
        add_action('wp_footer', array($this, 'add_live_chat'));
    }

    public function add_live_chat()
    {
        echo '<link rel="stylesheet" href="https://quickbox.odoo.com/im_livechat/external_lib.css"/>
            
            <script type="text/javascript" src="https://quickbox.odoo.com/im_livechat/external_lib.js"></script>
            
            <script type="text/javascript" src="https://quickbox.odoo.com/im_livechat/loader/1"></script>';

        $live_chat = get_option('qbcb_live_chat');
        if (!empty($live_chat)) {
            echo $live_chat;
        }
    }
}