<?php 

//Tell WordPress to only load the basics
define('SHORTINIT',1);


include_once 'Matex/Evaluator.php';
include_once 'classes/Costabox_Box.php';

//get path of wp-load.php and load it
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

// register global database
global $wpdb;



function api_costabox_get_stock_flutes( )
{
    global $wpdb;

    $table = $wpdb->prefix . 'qbcb_board_prices';

    $sql = "SELECT flute_id FROM {$table} WHERE stock = 1";

    $results = $wpdb->get_results($sql, "ARRAY_A");

    $flutes = [];

    foreach($results as $r){
      $flutes[] = $r['flute_id'];


      $sql = "SELECT flute_id, flute FROM {$wpdb->prefix}qbcb_flutes WHERE equivalent = " . $r['flute_id'];


      foreach($wpdb->get_results($sql, "ARRAY_A") as $e){
      	if(strpos($e['flute'], "Lightweight") > -1 || strpos($e['flute'], "Extra") > -1) continue;

      	$flutes[] = intval($e['flute_id']);
      }
        
      

    }

    return $flutes;
}

function api_costabox_api_box_types_by_size( $width, $length, $height ){

	global $wpdb;

	$table = $wpdb->prefix . "qbcb_box_type_restrictions";
	$width_ids = array();
	$length_ids = array();
	$height_ids = array();

	// Get the box types that allow for the specified width
	$sql = "SELECT box_type_id FROM {$table} WHERE (min <= {$width} OR min IS NULL) AND (max >= {$width} OR max IS NULL) AND dimension = 'breadth'";
	if(empty($width)) $sql = "SELECT box_type_id FROM {$table} WHERE max = 0 AND dimension = 'breadth'";

	$results = $wpdb->get_results($sql);

	if(!empty($results)) {
        foreach ($results as $row)
            $width_ids[] = $row->box_type_id;
    }

	// Get the box types that allow for the specified length
	$sql = "SELECT box_type_id FROM {$table} WHERE (min <= {$length} OR min IS NULL) AND (max >= {$length} OR max IS NULL) AND dimension = 'length'";
	if(empty($length)) $sql = "SELECT box_type_id FROM {$table} WHERE max = 0 AND dimension = 'length'";

	$results = $wpdb->get_results($sql);

	if(!empty($results)) {
        foreach ($results as $row)
            $length_ids[] = $row->box_type_id;
    }

	// Get the box types that allow for the specified height
	$sql = "SELECT box_type_id FROM {$table} WHERE (min <= {$height} OR min IS NULL) AND (max >= {$height} OR max IS NULL) AND dimension = 'height'";
	if(empty($height)) $sql = "SELECT box_type_id FROM {$table} WHERE (max = 0 OR min = 0) AND dimension = 'height'";

	$results = $wpdb->get_results($sql);

	if(count($results) > 0) {
        foreach ($results as $row)
            $height_ids[] = $row->box_type_id;
    }

	// Find the common IDs that allow these dimensions
	$box_types = array_intersect($width_ids, $length_ids, $height_ids);

	// Some specific box types need to be calculated using chop and deckle
    if($height == 12 || $height == 22 || $height > 29) {

        $sql = "SELECT box_type_id FROM {$wpdb->prefix}qbcb_box_type WHERE code IN ('0401')";
        $results = $wpdb->get_results($sql);

        if (count($results) > 0) {

            foreach ($results as $r) {

                if(!(in_array($r->box_type_id, $width_ids) && in_array($r->box_type_id, $length_ids))) continue;

                $box = new Costabox_Box($r->box_type_id, 0, 0, 1, $height, $width, $length);
                $box->calculate();


                try {
                    $box_width = $box->getChop();
                    $box_length = $box->getDeckle();

                    if ($box_width < 2200 && $box_length < 4000) {
                        $box_types[] = $r->box_type_id;
                    }

                } catch (Exception $e) {
                }

            }

        }
    }


	return $box_types;

}


function api_costabox_can_use_stock($width, $height, $length, $box_type_id){

    global $wpdb;
    $box_code = $wpdb->get_var("SELECT code FROM {$wpdb->prefix}qbcb_box_type WHERE box_type_id = {$box_type_id}");

    if($box_code == "DIE-CUT") return true;

    $height = intval($height);

    $box = new Costabox_Box($box_type_id, 0, 0, 1, $height, $width, $length);
    $box->calculate();

	$blank_length = api_costabox_get_setting('stock_board_chop');
	$blank_width = api_costabox_get_setting('stock_board_deckle');

//	$qty = costabox_get_qty_per_blank( 14, $calculated_box_length, $calculated_box_width, null, $blank_length, $blank_width);

	if($box->getChop() > $blank_length || $box->getDeckle() > $blank_width) return false;

	return true;

}


function api_costabox_get_setting($key)
{
    global $wpdb;
    $table = $wpdb->prefix . 'qbcb_options';

    $sql = "SELECT meta_value FROM {$table} WHERE meta_key = '{$key}' ORDER BY option_id DESC";

    $value = $wpdb->get_var($sql);

    return $value;
}


function api_costabox_api_dimension_restrictions( $box_type, $dimension){

	// Get the minimum and maximum value for the specified dimension and box type
	global $wpdb;

	$sql = "SELECT min, max FROM {$wpdb->prefix}qbcb_box_type_restrictions WHERE box_type_id = {$box_type} AND dimension = '{$dimension}'";
	$restrictions = $wpdb->get_row($sql);

	if(is_null($restrictions)){
		return "500"; // Return an error if no restrictions are found
	}

	$data = array(
		"min" => $restrictions->min,
		"max" => $restrictions->max,
	);

	return json_encode($data);

}

function api_hubspot_get_contact_details( $hubspot_id ){

	$hubspot_api = api_costabox_get_setting('hubspot_api');
	$get_url = "https://api.hubapi.com/contacts/v1/contact/utk/" . $hubspot_id . "/profile?hapikey=" . $hubspot_api;

    $ch = curl_init(); 
    @curl_setopt($ch, CURLOPT_HTTPGET, true);
    @curl_setopt($ch, CURLOPT_URL, $get_url);
    @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response    = @curl_exec($ch); //Log the response from HubSpot as needed.
    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
    @curl_close($ch);

	//$data = json_decode($response,true);;

	return $response;
}

if(isset($_POST['hubspot_utk'])){
	$hubspot_id = $_POST["hubspot_utk"];
	$data = api_hubspot_get_contact_details( $hubspot_id );

	echo $data;

    exit;
}


if($_POST['qbcb_request'] == "stock_flutes"){
	$stock_flutes = api_costabox_get_stock_flutes();

	$stock_flutes = array_unique($stock_flutes);

	echo (implode(",", $stock_flutes));
}

if($_POST['qbcb_request'] == "dimension_restrictions" || $_POST['qbcb_request'] == "dimension_request"){

	$dimension = $_POST["dimension"];
	$box_type = $_POST["box_type"];

	echo api_costabox_api_dimension_restrictions($box_type, $dimension);
}

if($_POST['qbcb_request'] == "boxes_by_size"){
	$width = $_POST["width"];
	$length = $_POST["length"];
	$height = $_POST["height"];
	$use_stock = $_POST["use_stock"];

	$box_types = api_costabox_api_box_types_by_size($width, $length, $height);
    
	$eligible_box_types = array();

	if(is_array($box_types)){
		foreach($box_types as $bt){
			 if($use_stock == 1 && !api_costabox_can_use_stock($width, $height, $length, $bt))
			 	continue;

			$eligible_box_types[] = $bt;
		}
	}

	echo (implode(",", $eligible_box_types));
}