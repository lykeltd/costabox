<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_board_weight_menu");
 
function qbcb_board_weight_menu(){
        add_submenu_page('costabox', 'Board Weights', 'Board Weights', 'manage_options', 'costabox-weight', 'qbcb_board_weight_callback' );
}

function qbcb_board_weight_callback(){
	
	if(isset($_GET['action']) && $_GET['action'] == 'import'){
		qbcb_board_weight_import();
		return;
	}

	qbcb_board_weight_list();
}

add_action('admin_head', 'board_weight_list_width');

function board_weight_list_width() {
    echo '<style type="text/css">';
    echo '.costabox-box .column-flute_id { width:5% !important; }';
    echo '</style>';
}
 
function qbcb_board_weight_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Board Weights</h1><a href="?page=costabox-weight&action=import" class="page-title-action">Import Weights</a>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">
        	<div id="col-container" class="wp-clearfix">
        		<div id="col-left">
        			<div class="col-wrap">
        				<?php qbcb_board_weight_add(); ?>
        			</div>
        		</div>
        		<div id="col-right">
        			<div class="col-wrap">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">

					        <?php
					        	include_once 'board_weight_list.php';
					        	$list = new Board_Weight_List();
					        	$list->prepare_items();
					        	$list->display();
					        ?>

			        		</form>
			        	</div>
			        </div>
			    </div>
			</div>
        </div>

    </div>

	<?php
}

function qbcb_board_weight_form_values(){

	$values = array();

	$db = null;

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_board_weight WHERE board_weight_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

	}

	$values['flute'] = isset($_POST['flute']) ? $_POST['flute'] : (is_null($db) ? "" : $db->flute_id);
	$values['grade'] = isset($_POST['grade']) ? $_POST['grade'] : (is_null($db) ? "" : $db->grade_id);
	$values['board_weight'] = isset($_POST['board_weight']) ? $_POST['board_weight'] : (is_null($db) ? "" : $db->board_weight);
	$values['finished_weight'] = isset($_POST['finished_weight']) ? $_POST['finished_weight'] : (is_null($db) ? "" : $db->finished_weight);

	return $values;

}

 
function qbcb_board_weight_add(){

	qbcb_process_add_weight();

	extract(qbcb_board_weight_form_values());

	$action = isset($_GET['action']) ? $_GET['action'] : "add";

	?>
        <form method="POST">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="flutes">Flute</label>
        			</th>
        			<td>
						<select name="flute" id="flutes" >
							<?php foreach(Costabox_Board::getAllFlutes(true) as $id => $opt){
								echo "<option value='{$id}'" . ($id == $flute ? " selected" : "") . ">{$opt['flute']}</option>";
							} ?>
						</select>        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="grades">Grade</label>
        			</th>
        			<td>
						<select name="grade" id="grades" >
							<?php foreach(Costabox_Board::getAllGrades() as $o_id => $opt){
								echo "<option value='{$o_id}'" . ($o_id == $grade ? " selected" : "") . ">$opt</option>";
							} ?>
						</select>        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="board_weight">Board Weight</label>
        			</th>
        			<td>
						<input type="number" name="board_weight" id="board_weight" step="0.0001" value="<?php echo $board_weight; ?>"> kg per m<sup>2</sup>			
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="finished_weight">Finished Weight</label>
        			</th>
        			<td>
						<input type="number" name="finished_weight" id="finished_weight" step="0.0001" value="<?php echo $finished_weight; ?>"> kg per m<sup>2</sup>			
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($action); ?> Weight" class="button-primary" name="submit"></p>
        </form>

	<?php
}

function qbcb_process_add_weight(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['board_weight'])) $missing_fields .= "the board_weight, ";
	if(empty($_POST['finished_weight'])) $missing_fields .= "the finished weight, ";

	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_board_weight";

	// Check for duplicate box code
	$weights = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE {$table}.flute_id = '" . $_POST['flute'] . "' AND {$table}.grade_id = '" . $_POST['grade'] . "'" . ($_GET['action'] == 'edit' ? " AND NOT board_weight_id = " . $_GET['id'] : ""));
	if($weights > 0){
		echo "<div class='notice notice-warning'><p>Board weight already in the system.</p></div>";
		return;
	}	


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['flute_id'] = $_POST['flute'];
	$data['grade_id'] = $_POST['grade'];
	$data['board_weight'] = $_POST['board_weight'];
	$data['finished_weight'] = $_POST['finished_weight'];

	if($_GET['action'] != 'edit'){

		$success = $wpdb->insert($table, $data);

	}else{

		$success = $wpdb->update($table, $data, array("board_weight_id" => $_GET['id']));

	}

	if($success == false){
		echo "<div class='notice notice-error'><p>Board price could not be added - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	// Redirect to list box types
	wp_redirect(get_admin_url() . "admin.php?page=costabox-weight");

}


function qbcb_board_weight_import(){
	?>
	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Import Board Weights</h1>
        <hr class="wp-header-end">

        <?php qbcb_board_weight_import_process(); ?>

        <form method="POST" enctype="multipart/form-data">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="csv-file">Upload CSV</label>
        			</th>
        			<td>
						<input type="file" name="csv" id="csv-file" >        				
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="Import Prices" class="button-primary" name="submit"></p>
        </form>
    </div>
    <?php

}

function qbcb_board_weight_import_process(){

	if(!isset($_POST['submit'])) return;

	if(!isset($_FILES['csv'])){
		echo "No CSV selected";
		return; 
	}

	if($_FILES['csv']['error'] > 0){
		echo "There was an error uploading the file. Please try again.";
		return;
	}

	// Convert file to array
	$tmpName = $_FILES['csv']['tmp_name'];
	$csvAsArray = array_map('str_getcsv', file($tmpName));

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_board_weight";

	for($i = 0; $i < count($csvAsArray); $i++){

		$flute = Costabox_Board::getFluteId($csvAsArray[$i][0]);

		$grade = Costabox_Board::getGradeId($csvAsArray[$i][1]);

		$board_weight = $csvAsArray[$i][2];
		$finished_weight = $csvAsArray[$i][3];

		// Remove Weights For This Flute & Grade Already in the database
		$wpdb->delete($table, array(
			"flute_id" => $flute,
			"grade_id" => $grade,
		));

		// Insert new weights
		$wpdb->insert($table, array(
			"flute_id" => $flute,
			"grade_id" => $grade,
			"board_weight" => $board_weight,
			"finished_weight" => $finished_weight,
		));

	}

	wp_redirect(get_admin_url() . "admin.php?page=costabox-weight");

}