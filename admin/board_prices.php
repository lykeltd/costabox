<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_board_prices_menu");
 
function qbcb_board_prices_menu(){
        add_submenu_page('costabox', 'Board Prices', 'Board Prices', 'manage_options', 'costabox-prices', 'qbcb_board_prices_callback' );
}

function qbcb_board_prices_callback(){

	if(isset($_GET['action']) && $_GET['action'] == 'import'){
		qbcb_board_prices_import();
		return;
	}

	qbcb_board_prices_list();
}

add_action('admin_head', 'board_prices_list_width');

function board_prices_list_width() {
    echo '<style type="text/css">';
    echo '.costabox-box .column-flute_id { width:5% !important; }';
    echo '</style>';
}
 
function qbcb_board_prices_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Board Prices</h1><a href="?page=costabox-prices&action=import" class="page-title-action">Import Prices</a>&nbsp;<a href="?page=costabox-prices&download=csv" class="page-title-action">Export Prices</a>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">
        	<div id="col-container" class="wp-clearfix">
        		<div id="col-left">
        			<div class="col-wrap">
        				<?php qbcb_board_prices_add(); ?>
        			</div>
        		</div>
        		<div id="col-right">
        			<div class="col-wrap">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">

					        <?php
					        	include_once 'board_price_list.php';
					        	$list = new Price_List();
					        	$list->prepare_items(isset($_POST['s']) ? $_POST['s'] : null );
					        	$list->search_box( 'Search', 'search_id' ); 
					        	$list->display();
					        ?>

			        		</form>
			        	</div>
			        </div>
			    </div>
			</div>
        </div>

    </div>

	<?php
}

function qbcb_board_prices_form_values(){

	$values = array();

	$db = null;

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_board_prices WHERE board_price_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

	}

	$values['flute'] = isset($_POST['flute']) ? $_POST['flute'] : (is_null($db) ? "" : $db->flute_id);
	$values['grade'] = isset($_POST['grade']) ? $_POST['grade'] : (is_null($db) ? "" : $db->grade_id);
	$values['min_qty'] = isset($_POST['min_qty']) ? $_POST['min_qty'] : (is_null($db) ? "" : $db->min_qty);
	$values['max_qty'] = isset($_POST['max_qty']) ? $_POST['max_qty'] : (is_null($db) ? "" : $db->max_qty);
	$values['price'] = isset($_POST['price']) ? $_POST['price'] : (is_null($db) ? "" : $db->price);
	$values['in_stock'] = isset($_POST['in_stock']) ? $_POST['in_stock'] : (is_null($db) ? "" : $db->stock);

	return $values;

}

add_action('init', function (){

    if(!is_user_logged_in()) return;

    if(isset($_GET['page']) && $_GET['page'] == 'costabox-prices' && isset($_GET['download']) && $_GET['download'] == 'csv')
        qbcb_board_prices_export_process();

});
 
function qbcb_board_prices_add(){

	qbcb_process_add_price();

	extract(qbcb_board_prices_form_values());

	$action = isset($_GET['action']) ? $_GET['action'] : "add";

	?>
        <form method="POST">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="flutes">Flute</label>
        			</th>
        			<td>
						<select name="flute" id="flutes" >
							<?php foreach(Costabox_Board::getAllFlutes(true) as $id => $opt){
								echo "<option value='{$id}'" . ($id == $flute ? " selected" : "") . ">{$opt['flute']}</option>";
							} ?>
						</select>        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="grades">Grade</label>
        			</th>
        			<td>
						<select name="grade" id="grades" >
							<?php foreach(Costabox_Board::getAllGrades() as $o_id => $opt){
								echo "<option value='{$o_id}'" . ($o_id == $grade ? " selected" : "") . ">$opt</option>";
							} ?>
						</select>        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="min_qty">Break Point</label>
        			</th>
        			<td>
						<input type="number" name="min_qty" id="min_qty" step="1" placeholder="No Min Qty" value="<?php echo $min_qty; ?>">&nbsp;To	<input type="number" name="max_qty" id="max_qty" step="1" placeholder="No Max Qty" value="<?php echo $max_qty; ?>">			
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="price">Price</label>
        			</th>
        			<td>
						<strong>£</strong><input type="number" name="price" id="price" step="0.01" placeholder="Price Per Board" value="<?php echo $price; ?>">
        			</td>
        		</tr>
        		<tr>
        			<th scope="row" colspan="2">
        				<input type="checkbox" name="in_stock" id='in_stock' value="yes" <?php if($in_stock == 1) echo "checked";?>><label for="in_stock">This board is in stock.</label>
        			</th>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($action); ?> Price" class="button-primary" name="submit"></p>
        </form>

	<?php
}

function qbcb_process_add_price(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['price'])) $missing_fields .= "the price per board, ";
	if(empty($_POST['min_qty']) && empty($_POST['max_qty'])) $missing_fields .= "and a minimum quantity and/or a maximum quantity, ";

	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	if(!empty($_POST['min_qty']) && !empty($_POST['max_qty'])){
		if($_POST['min_qty'] >= $_POST['max_qty']){
			echo "<div class='notice notice-warning'><p>Maximum board quantity should be greater than minimum.</p></div>";
			return;
		}
	}

	$in_stock = empty($_POST['in_stock']) ? 0 : 1;

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_board_prices";

	// Check for duplicate box code
	$prices = $wpdb->get_results("SELECT min_qty, max_qty FROM {$table} WHERE {$table}.stock = {$in_stock} AND {$table}.flute_id = '" . $_POST['flute'] . "' AND {$table}.grade_id = '" . $_POST['grade'] . "'" . ($_GET['action'] == 'edit' ? " AND NOT board_price_id = " . $_GET['id'] : ""), "ARRAY_A");
	foreach($prices as $p){

		if(!empty($_POST['min_qty']) && empty($_POST['max_qty'])){
			if(is_null($p['max_qty']) && $p['min_qty'] < $_POST['min_qty']) continue;
			if(!is_null($p['max_qty']) && $_POST['min_qty'] > $p['max_qty']) continue;
		}

		if(empty($_POST['min_qty']) && !empty($_POST['max_qty'])){
			if(is_null($p['min_qty']) && $p['min_qty'] < $_POST['max_qty']) continue;
			if(!is_null($p['min_qty']) && $_POST['max_qty'] > $p['min_qty']) continue;
		}

		if(!empty($_POST['min_qty']) && !empty($_POST['max_qty'])){
			if(is_null($p['min_qty']) && !is_null($p['max_qty']) && $_POST['min_qty'] > $p['max_qty']) continue;
			if(!is_null($p['min_qty']) && is_null($p['max_qty']) && $_POST['max_qty'] < $p['min_qty']) continue;
			if(!is_null($p['min_qty']) && !is_null($p['max_qty']) && (($_POST['min_qty'] < $p['min_qty'] && $_POST['max_qty'] < $p['min_qty']) || ($_POST['max_qty'] > $p['max_qty'] && $_POST['min_qty'] > $p['min_qty']))) continue;
		}

		echo "<div class='notice notice-warning'><p>This will overlap existing board prices. Please change break points.</p></div>";
		return;
	}


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['flute_id'] = $_POST['flute'];
	$data['grade_id'] = $_POST['grade'];
	$data['min_qty'] = empty($_POST['min_qty']) ? null : round($_POST['min_qty'], 0);
	$data['max_qty'] = empty($_POST['max_qty']) ? null : round($_POST['max_qty'], 0);
	$data['price'] = $_POST['price'];
	$data['stock'] = $in_stock;

	if($_GET['action'] != 'edit'){

		$success = $wpdb->insert($table, $data);

	}else{

		$success = $wpdb->update($table, $data, array("board_price_id" => $_GET['id']));

	}

	if($success == false){
		echo "<div class='notice notice-error'><p>Board price could not be added - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	// Redirect to list box types
	wp_redirect(get_admin_url() . "admin.php?page=costabox-prices");

}

function qbcb_board_prices_import(){
	?>
	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Import Board Prices</h1>
        <hr class="wp-header-end">

        <?php qbcb_board_prices_import_process(); ?>

        <form method="POST" enctype="multipart/form-data">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="csv-file">Upload CSV</label>
        			</th>
        			<td>
						<input type="file" name="csv" id="csv-file" >        				
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="Import Prices" class="button-primary" name="submit"></p>
        </form>
    </div>
    <?php
}

function qbcb_board_prices_import_process(){

	if(!isset($_POST['submit'])) return;

	if(!isset($_FILES['csv'])){
		echo "No CSV selected";
		return; 
	}

	if($_FILES['csv']['error'] > 0){
		echo "There was an error uploading the file. Please try again.";
		return;
	}

	// Convert file to array
	$tmpName = $_FILES['csv']['tmp_name'];
	$csvAsArray = array_map('str_getcsv', file($tmpName));

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_board_prices";

	$qtys = array();
	for($i = 2; $i < (count($csvAsArray[0]) - 1); $i++)
		$qtys[$i - 2] = $csvAsArray[0][$i];

	for($i = 1; $i < count($csvAsArray); $i++){

		$flute = Costabox_Board::getFluteId($csvAsArray[$i][0]);

		$grade = Costabox_Board::getGradeId($csvAsArray[$i][1]);

		$stock = $csvAsArray[$i][count($qtys) + 2];
		$stock = (strpos($stock, "stock") === false ? "0" : "1");

		// Remove Prices For This Flute & Grade Already in the database
		$wpdb->delete($table, array(
			"flute_id" => $flute,
			"grade_id" => $grade,
			"stock" => $stock
		));

		for($p = 2; $p < count($csvAsArray[$i]); $p++){

			if(($p - 1) > count($qtys)) continue;

			$min_max = explode("-", $qtys[$p - 2]);

			$min = empty($min_max[0]) ? NULL : $min_max[0];

			$max = empty($min_max[1]) ? NULL : $min_max[1];

			$price = $csvAsArray[$i][$p];

			if(!is_numeric($price[0]))
				$price = substr($price, 1);

			if($price == 0) continue;

			$wpdb->insert($table, array(
				"flute_id" => $flute,
				"grade_id" => $grade,
				"min_qty" => $min,
				"max_qty" => $max,
				"price" => $price,
				"stock" => $stock,
			));

		}

	}

	wp_redirect(get_admin_url() . "admin.php?page=costabox-prices");

}

function qbcb_board_prices_export_process(){

    $csv = array();

    // Headings
    $csv[] = [
            "Flute",
            "Grade",
            "-499",
            "500-999",
            "1000-2999",
            "3000-5999",
            "6000-9999",
            "10000",
            ""
    ];

    // Keep track of line numbers
    $line_numbers = array();
    $line = 0;

    // Loop through board prices in the system
    global $wpdb;
    $tbl = $wpdb->prefix . "qbcb_board_prices";

    $sql = "SELECT * FROM {$tbl}";

    $prices = $wpdb->get_results($sql);
    $flutes = Costabox_Board::getAllFlutes(true);

    foreach($prices as $p){

        $flute = $flutes[$p->flute_id]['flute'] ?? "Unknown";
        $grade = Costabox_Board::getGradeById($p->grade_id);

        $key = $flute . $grade . $p->stock;

        if(isset($line_numbers[$key]))
            $i = $line_numbers[$key];
        else {
            $line++;
            $i = $line;
        }

        $line_numbers[$key] = $i;

        if(!isset($csv[$i])) $csv[$i] = [];

        $csv[$i][0] = $flute;
        $csv[$i][1] = $grade;

        if($p->min_qty >= 10000 && empty($p->max_qty))
            $csv[$i][7] = $p->price;
        elseif(!is_null($p->max_qty) && $p->max_qty < 500)
            $csv[$i][2] = $p->price;
        elseif($p->min_qty >= 499 && $p->max_qty < 1000)
            $csv[$i][3] = $p->price;
        elseif($p->min_qty >= 1000 && $p->max_qty < 3000)
            $csv[$i][4] = $p->price;
        elseif($p->min_qty >= 3000 && $p->max_qty < 6000)
            $csv[$i][5] = $p->price;
        elseif($p->min_qty >= 6000 && $p->max_qty < 10000)
            $csv[$i][6] = $p->price;

        if($p->stock == 1)
            $csv[$i][8] = "stock";
    }

    // Sort the line so the colums are outputted in the right order
    foreach($csv as &$l) {

        for($i = 0; $i < 9; $i++){
            if(!isset($l[$i]))
                $l[$i] = "";
        }

        ksort($l);

    }

    // Convert array to csv
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=costabox_prices_' . date("Ymd-Hi") . ' .csv');

    $out = fopen("php://output", "w");
    foreach($csv as $l){
        fputcsv($out, $l);
    }
    fclose($out);

    die();
}


