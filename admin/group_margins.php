<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_groups_margins_menu");
 
function qbcb_groups_margins_menu(){
        add_submenu_page('costabox', 'Groups', 'Group Margins', 'manage_options', 'costabox-margins', 'qbcb_groups_margins_callback' );
}

function qbcb_groups_margins_callback(){
	qbcb_process_update_group_markups();
	qbcb_groups_margins_list();
}
 
function qbcb_groups_margins_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Group Margins</h1>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">       	
			<div class="col-wrap">
				<div class="meta-box-sortables ui-sortable">
					<form method="post">

			        <?php

			        $groups = Groups_Group::get_groups();

			        foreach($groups as $group){
			         	echo "<hr><h3>{$group->name}:</h3>";

			         	echo '<table class="form-table">';

						echo '<tr><th scope="row"><label for="markup_1">Markup under 200m<sup>2</sup></label></th><td>';

						$markup_1 = costabox_get_setting("{$group->group_id}_markup_1");

						echo "<input {$readonly} type='number' id='markup_1' name='{$group->group_id}_markup_1' min='0' max='1000' style='min-width: 200px' placeholder='Leave blank to use " . costabox_get_setting("markup_1") . "%' value='{$markup_1}'>%";

						echo "</td></tr>";

						echo '<tr><th scope="row"><label for="markup_2">Markup between 200m<sup>2</sup> and 500m<sup>2</sup></label></th><td>';

						$markup_2 = costabox_get_setting("{$group->group_id}_markup_2");

						echo "<input {$readonly} type='number' id='markup_2' name='{$group->group_id}_markup_2' min='0' max='1000' style='min-width: 200px' placeholder='Leave blank to use  " . costabox_get_setting("markup_2") . "%' value='{$markup_2}'>%";

						echo "</td></tr>";

						echo '<tr><th scope="row"><label for="markup_3">Markup over 500m<sup>2</sup></th><td>';

						$markup_3 = costabox_get_setting("{$group->group_id}_markup_3");

						echo "<input {$readonly} type='number' id='markup_3' name='{$group->group_id}_markup_3' min='0' max='1000' style='min-width: 200px' placeholder='Leave blank to use " . costabox_get_setting("markup_3") . "%' value='{$markup_3}'>%";

						echo "</td></tr>";

						echo "</table>";
			        }


			        ?>

			        <input type="submit" name="submit" value="Update Pricing" class="button button-primary">

	        		</form>
	        	</div>
	        </div>
        </div>

    </div>

	<?php
}

function qbcb_process_update_group_markups(){

	if(!isset($_POST['submit']) || $_POST['submit'] != "Update Pricing") return;

	$groups = Groups_Group::get_groups();

    foreach($groups as $group){

    	for($n = 1; $n < 4; $n++) {
	    	if(isset($_POST["{$group->group_id}_markup_{$n}"])){
	    		costabox_set_setting("{$group->group_id}_markup_{$n}", $_POST["{$group->group_id}_markup_{$n}"]);
	    	}
    	}

    }

}


