<?php
/**
 * Lets do this correctly and like its 2020
 */

if (!class_exists(TpfQuoteEmails::class)) {
    /**
     *
     */
    class TpfQuoteEmails
    {
        /**
         *
         */
        public function __construct()
        {
            add_action('admin_menu', [$this, 'add_settings_menu']);
            add_action('admin_init', [$this, 'register_settings']);
        }

        /**
         *
         */
        public function add_settings_menu()
        {
            add_submenu_page(
                'costabox',
                'Email Settings',
                'Email Settings',
                'manage_options',
                'costabox-quote-email-settings',
                [$this, 'callback']
            );
        }

        /**
         *
         */
        public function callback()
        {
            tpf_get_template_part('admin/templates/email-settings');
        }

        /**
         *
         */
        public function register_settings()
        {
            register_setting('costabox_quote_email_options', 'costabox_quote_email_options');
            add_settings_section('costabox_quote_email_settings', 'Email Content Settings', [$this, 'add_section_text'], 'costabox-quote-email-settings');

            add_settings_field('quote_email_settings_title', 'Email Title', [$this, 'settings_title'], 'costabox-quote-email-settings', 'costabox_quote_email_settings');
            add_settings_field('quote_email_settings_message', 'Email Content', [$this, 'settings_message'], 'costabox-quote-email-settings', 'costabox_quote_email_settings');
            add_settings_field('quote_email_settings_email_pdf', 'Attach PDF', [$this, 'settings_attach_pdf'], 'costabox-quote-email-settings', 'costabox_quote_email_settings');
            add_settings_field('quote_email_settings_pdf_to_hubspot', 'Attach PDF to Hubspot', [$this, 'settings_attach_pdf_to_deal'], 'costabox-quote-email-settings', 'costabox_quote_email_settings');
        }

        /**
         *
         */
        public function add_section_text()
        {
            echo '<p>Here are all the options</p>';
        }

        /**
         *
         */
        public function settings_title()
        {
            $options = get_option('costabox_quote_email_options');

            if (!empty($options['title']) || !$options) {
                $options['title'] = 'Here is your quote';
            }
            echo '<input id="quote_email_settings_title" name="costabox_quote_email_options[title]" type="text" value="' . esc_attr($options['title']) . '" />';
        }

        /**
         *
         */
        public function settings_message()
        {
            $options  = get_option('costabox_quote_email_options');

            if (empty($options['message']) || $options == false) {
                $options['message'] = "Hello<br><br>Please find your quote below. If you'd like to proceed with it, simply go to {cart_url} with the items in your cart or get in touch. <em>Please note prices are exclusive of shipping, correct at the time of this email and are subject to change.</em><br/>";
            }

            $class    = (!empty($class)) ? $class : '';
            $id       = 'quote_email_settings_message';
            $settings = [
                'textarea_name' => 'costabox_quote_email_options[message]',
                'editor_class'  => '',
            ];

            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
            wp_enqueue_script('editor');
            wp_editor($options['message'], $id, $settings);
            echo '<p>In order to use dynamic urls, please add {cart_url} in order to output the cart link.</p>';
        }

        /**
         *
         */
        public function settings_attach_pdf()
        {
            $options  = get_option('costabox_quote_email_options');
            $html     = '<input type="checkbox" id="costabox_quote_email_options[attach_pdf]" name="costabox_quote_email_options[attach_pdf]" value="1"' . checked(1, $options['attach_pdf'], false) . '/>';
            $html .= '<label for="checkbox_example">Attach and send PDF of quote in email</label>';

            echo $html;
        }

        /**
         *
         */
        public function settings_attach_pdf_to_deal()
        {
            $options  = get_option('costabox_quote_email_options');
            $html     = '<input type="checkbox" id="costabox_quote_email_options[attach_pdf_to_deal]" name="costabox_quote_email_options[attach_pdf]" value="1"' . checked(1, $options['attach_pdf_to_deal'], false) . '/>';
            $html .= '<label for="checkbox_example">Attach PDF of quote to deal in Hubspot</label>';

            echo $html;
        }
    }

    new TpfQuoteEmails();
}
