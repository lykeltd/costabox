<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_box_types_menu");
 
function qbcb_box_types_menu(){
        add_submenu_page('costabox', 'Box Types', 'Box Types', 'manage_options', 'costabox-box', 'qbcb_box_types_callback' );
}

function qbcb_image_upload_include() {
	/*
	 * I recommend to add additional conditions just to not to load the scipts on each page
	 * like:
	 * if ( !in_array('post-new.php','post.php') ) return;
	 */
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
 
 	wp_enqueue_script( 'qbcb_image_upload', plugin_dir_url(__FILE__) . '/js/image_upload.js', array('jquery'), null, false );
}
 
add_action( 'admin_enqueue_scripts', 'qbcb_image_upload_include' );

function qbcb_box_types_callback(){

	if(isset($_GET['action']) && ($_GET['action'] == "add" || $_GET['action'] == "edit")){
		qbcb_box_types_add();
		return;
	}

	qbcb_box_types_list();
}

add_action('admin_head', 'box_types_list_width');

function box_types_list_width() {
	if(!isset($_GET['page']) || $_GET['page'] != 'costabox-box') return;

    echo '<style type="text/css">';
    echo '.costabox-box .column-box_type_id { width:5% !important; }';
    echo '.costabox-box .column-image { width:20% !important; }';
    echo '</style>';
}
 
function qbcb_box_types_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Box Types</h1><a href="?page=costabox-box&action=add" class="page-title-action">Add New</a>
        <hr class="wp-header-end">
        <script type="text/csss">
        	.wp-list-table .column-box_type_id { width: 5% !important; }
        </script>
        <div id="post-body-content" class="costabox-box">
			<div class="meta-box-sortables ui-sortable">
				<form method="post">

		        <?php
		        	include_once 'box_types_list.php';
		        	$list = new Box_Type_List();
		        	$list->prepare_items();
		        	$list->display();
		        ?>

        		</form>
        	</div>
        </div>

    </div>

	<?php
}

function qbcb_box_types_form_values(){

	$values = array();

	$db = null;
	$length_restrictions = null;
	$breadth_restrictions = null;
	$height_restrictions = null;
	$box_length = null;
	$lid_length = null;
	$box_width = null;
	$lid_width = null;
	$times = array();

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_box_type WHERE box_type_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

		$sql = "SELECT min, max FROM {$wpdb->prefix}qbcb_box_type_restrictions WHERE dimension = 'length' AND box_type_id = " . $_GET['id'];
		$length_restrictions = $wpdb->get_row($sql);
		
		$sql = "SELECT min, max FROM {$wpdb->prefix}qbcb_box_type_restrictions WHERE dimension = 'breadth' AND box_type_id = " . $_GET['id'];
		$breadth_restrictions = $wpdb->get_row($sql);

		$sql = "SELECT min, max FROM {$wpdb->prefix}qbcb_box_type_restrictions WHERE dimension = 'height' AND box_type_id = " . $_GET['id'];
		$height_restrictions = $wpdb->get_row($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_length_calculations WHERE component = 'box' AND box_type_id = " . $_GET['id'];
		$box_length = $wpdb->get_row($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_length_calculations WHERE component = 'lid' AND box_type_id = " . $_GET['id'];
		$lid_length = $wpdb->get_row($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_width_calculations WHERE component = 'box' AND box_type_id = " . $_GET['id'];
		$box_width = $wpdb->get_row($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_width_calculations WHERE component = 'lid' AND box_type_id = " . $_GET['id'];
		$lid_width = $wpdb->get_row($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_length_calculations WHERE component = 'box_multiply' AND box_type_id = " . $_GET['id'];
		$box_length_multiply = $wpdb->get_var($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_length_calculations WHERE component = 'lid_multiply' AND box_type_id = " . $_GET['id'];
		$lid_length_multiply = $wpdb->get_var($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_width_calculations WHERE component = 'box_multiply' AND box_type_id = " . $_GET['id'];
		$box_width_multiply = $wpdb->get_var($sql);

		$sql = "SELECT formula FROM {$wpdb->prefix}qbcb_width_calculations WHERE component = 'lid_multiply' AND box_type_id = " . $_GET['id'];
		$lid_width_multiply = $wpdb->get_var($sql);

		$sql = "SELECT stage, qty, seconds FROM {$wpdb->prefix}qbcb_box_type_times WHERE box_type_id = " . $_GET['id'];
		$box_times = $wpdb->get_results($sql, "ARRAY_A");

		foreach($box_times as $t){
			if(!isset($times[$t['stage']])){
				$times[$t['stage']] = array();
			}

			$times[$t['stage']][$t['qty']] = $t['seconds'];
		}

	}

	$values['code'] = isset($_POST['code']) ? $_POST['code'] : (is_null($db) ? "" : $db->code);
	$values['name'] = isset($_POST['name']) ? $_POST['name'] : (is_null($db) ? "" : $db->name);
	$values['description'] = isset($_POST['description']) ? $_POST['description'] : (is_null($db) ? "" : $db->description);
	$values['image'] = isset($_POST['box_image']) ? $_POST['box_image'] : (is_null($db) ? "" : $db->image);
    $values['board_depth'] = isset($_POST['board_depth']) ? $_POST['board_depth'] : (is_null($db) ? "6" : $db->depth);

	$values['length_min'] = isset($_POST['length_min']) ? $_POST['length_min'] : (is_null($length_restrictions) ? "" : $length_restrictions->min);
	$values['length_max'] = isset($_POST['length_max']) ? $_POST['length_max'] : (is_null($length_restrictions) ? "" : $length_restrictions->max);

	$values['breadth_min'] = isset($_POST['breadth_min']) ? $_POST['breadth_min'] : (is_null($breadth_restrictions) ? "" : $breadth_restrictions->min);
	$values['breadth_max'] = isset($_POST['breadth_max']) ? $_POST['breadth_max'] : (is_null($breadth_restrictions) ? "" : $breadth_restrictions->max);

	$values['height_min'] = isset($_POST['height_min']) ? $_POST['height_min'] : (is_null($height_restrictions) ? "" : $height_restrictions->min);
	$values['height_max'] = isset($_POST['height_max']) ? $_POST['height_max'] : (is_null($height_restrictions) ? "" : $height_restrictions->max);

	$values['length_calc_box'] = isset($_POST['length_calc_box']) ? $_POST['length_calc_box'] : (is_null($box_length) ? "" : $box_length->formula);
	$values['length_calc_lid'] = isset($_POST['length_calc_lid']) ? $_POST['length_calc_lid'] : (is_null($lid_length) ? "" : $lid_length->formula);

	$values['width_calc_box'] = isset($_POST['width_calc_box']) ? $_POST['width_calc_box'] : (is_null($box_width) ? "" : $box_width->formula);
	$values['width_calc_lid'] = isset($_POST['width_calc_lid']) ? $_POST['width_calc_lid'] : (is_null($lid_width) ? "" : $lid_width->formula);

	$values['length_multiply_box'] = isset($_POST['length_multiply_box']) ? $_POST['length_multiply_box'] : (is_null($box_length_multiply) ? "" : $box_length_multiply);
	$values['length_multiply_lid'] = isset($_POST['length_multiply_lid']) ? $_POST['length_multiply_lid'] : (is_null($lid_length_multiply) ? "" : $lid_length_multiply);

	$values['width_multiply_box'] = isset($_POST['width_multiply_box']) ? $_POST['width_multiply_box'] : (is_null($box_width_multiply) ? "" : $box_width_multiply);
	$values['width_multiply_lid'] = isset($_POST['width_multiply_lid']) ? $_POST['width_multiply_lid'] : (is_null($lid_width_multiply) ? "" : $lid_width_multiply);

	$values['setup_time'] = isset($_POST['setup_time']) ? $_POST['setup_time'] : (is_null($times['setup']) ? "" : $times['setup'][0]);

	if(isset($_POST['slit_more_two'])) $times['slit'][3] = $_POST['slit_more_two'];
	if(isset($_POST['slit_one_two'])) $times['slit'][2] = $_POST['slit_one_two'];
	if(isset($_POST['slit_two_part'])) $times['slit'][1] = $_POST['slit_two_part'];

	if(isset($_POST['make_more_two'])) $times['make'][3] = $_POST['make_more_two'];
	if(isset($_POST['make_one_two'])) $times['make'][2] = $_POST['make_one_two'];
	if(isset($_POST['make_two_part'])) $times['make'][1] = $_POST['make_two_part'];

	if(isset($_POST['finish_more_two'])) $times['finish'][3] = $_POST['finish_more_two'];
	if(isset($_POST['finish_one_two'])) $times['finish'][2] = $_POST['finish_one_two'];
	if(isset($_POST['finish_two_part'])) $times['finish'][1] = $_POST['finish_two_part'];

	if(isset($_POST['print_more_two'])) $times['print'][3] = $_POST['print_more_two'];
	if(isset($_POST['print_one_two'])) $times['print'][2] = $_POST['print_one_two'];
	if(isset($_POST['print_two_part'])) $times['print'][1] = $_POST['print_two_part'];

	$values['times'] = $times;

	return $values;

}

 
function qbcb_box_types_add(){

	qbcb_process_add_box();

	extract(qbcb_box_types_form_values());

	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline"><?php echo ucfirst($_GET['action']); ?> Box Type</h1>
        <hr class="wp-header-end">

        <form method="POST">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="box-code">Box Code (FEFCO)</label>
        			</th>
        			<td>
						<input type="text" name="code" id="box-code" maxlength="45" value="<?php echo $code; ?>" >        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="box-name">Name</label>
        			</th>
        			<td>
						<input type="text" name="name" id="box-name" maxlength="45" value="<?php echo $name; ?>">        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="box-description">Description</label>
        			</th>
        			<td>
						<textarea name="description" id="box-description" placeholder="Describe this type of box (Optional)" maxlength="255"><?php echo $description; ?></textarea>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label>Image</label>
        			</th>
        			<td>
						<?php echo qbcb_image_uploader_field("box_image", $image); ?>
        			</td>
        		</tr>
        		<tr>
        			<td colspan="2" style="padding-left: 0px;"><h3>Dimensions</h3></td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="length_min">Length (mm) - Restrictions</label>
        			</th>
        			<td>
        				<input type="number" name="length_min" id="length_min" value="<?php echo $length_min; ?>">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="number" name="length_max" id="length_max" value="<?php echo $length_max; ?>">
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="breadth_min">Breadth (mm) - Restrictions</label>
        			</th>
        			<td>
        				<input type="number" name="breadth_min" id="breadth_min" value="<?php echo $breadth_min; ?>">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="number" name="breadth_max" id="breadth_max" value="<?php echo $breadth_max; ?>">
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="height_min">Height (mm) - Restrictions</label>
        			</th>
        			<td>
        				<input type="number" name="height_min" id="height_min" value="<?php echo $height_min; ?>">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="number" name="height_max" id="height_max" value="<?php echo $height_max; ?>">
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="length_calc_box">Length Calculation (Box)</label>
        			</th>
        			<td>
        				<input type="text" name="length_calc_box" id="length_calc_box" value="<?php echo $length_calc_box; ?>">&nbsp;&nbsp;<input type="text" name="length_multiply_box" id="length_multiply_box" value="<?php echo $length_multiply_box; ?>" placeholder="Allowance Multiplier"><br>
        				<span class="description">Use [height], [breadth], [length] and +, -, /, *</span>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="length_calc_lid">Length Calculation (lid)</label>
        			</th>
        			<td>
        				<input type="text" name="length_calc_lid" placeholder="Leave blank for no lid" id="length_calc_lid" value="<?php echo $length_calc_lid; ?>">&nbsp;&nbsp;<input type="text" name="length_multiply_lid" id="length_multiply_lid" value="<?php echo $length_multiply_lid; ?>" placeholder="Allowance Multiplier"><br>
        				<span class="description">Use [height], [breadth], [length] and +, -, /, *</span>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="width_calc_box">Width Calculation (Box)</label>
        			</th>
        			<td>
        				<input type="text" name="width_calc_box" id="width_calc_box" value="<?php echo $width_calc_box; ?>">&nbsp;&nbsp;<input type="text" name="width_multiply_box" id="width_multiply_box" value="<?php echo $width_multiply_box; ?>" placeholder="Allowance Multiplier"><br>
        				<span class="description">Use [height], [breadth], [length] and +, -, /, *</span>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="width_calc_lid">Width Calculation (Lid)</label>
        			</th>
        			<td>
        				<input type="text" name="width_calc_lid" id="width_calc_lid" placeholder="Leave blank for no lid" value="<?php echo $width_calc_lid; ?>">&nbsp;&nbsp;<input type="text" name="width_multiply_lid" id="width_multiply_lid" value="<?php echo $width_multiply_lid; ?>" placeholder="Allowance Multiplier"><br>
        				<span class="description">Use [height], [breadth], [length] and +, -, /, *</span>
        			</td>
        		</tr>
                <tr>
                    <th scope="row">
                        <label for="board_depth">Board Depth (mm)</label>
                    </th>
                    <td>
                        <input type="text" name="board_depth" id="board_depth"  value="<?php echo $board_depth ?>">
                    </td>
                </tr>
        		<tr>
        			<td style="padding-left: 0px;" colspan="2"><h3>Times</h3></td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="setup_time">Setup Time</label>
        			</th>
        			<td>
        				<input type="number" name="setup_time" id="setup_time" value="<?php echo $setup_time; ?>">
        			</td>
        		</tr>
        		<tr>
        			<td colspan="2" style="margin-left: 0px">
        				<table>
        					<tr>
        						<td></td><td>> 2 out</td><td>1 or 2 out</td><td>2-part</td>
        					</tr>
        					<tr>
        						<td><strong>Slit</strong></td><td><input name="slit_more_two" type="number" min="0" step="0.1" value="<?php echo $times['slit'][3]; ?>"></td><td><input name="slit_one_two" type="number" min="0" step="0.1" value="<?php echo $times['slit'][2]; ?>"></td><td><input name="slit_two_part" type="number" min="0" step="0.1" value="<?php echo $times['slit'][1]; ?>"></td>
        					</tr>
        					<tr>
        						<td><strong>Make</strong></td><td><input name="make_more_two" type="number" min="0" step="0.1" value="<?php echo $times['make'][3]; ?>"></td><td><input name="make_one_two" type="number" min="0" step="0.1" value="<?php echo $times['make'][2]; ?>"></td><td><input name="make_two_part" type="number" min="0" step="0.1" value="<?php echo $times['make'][1]; ?>"></td>
        					</tr>
        					<tr>
        						<td><strong>Finish</strong></td><td><input name="finish_more_two" type="number" min="0" step="0.1" value="<?php echo $times['finish'][3]; ?>"></td><td><input name="finish_one_two" type="number" min="0" step="0.1" value="<?php echo $times['finish'][2]; ?>"></td><td><input name="finish_two_part" type="number" min="0" step="0.1" value="<?php echo $times['finish'][1]; ?>"></td>
        					</tr>
        					<tr>
        						<td><strong>Print</strong></td><td><input name="print_more_two" type="number" min="0" step="0.1" value="<?php echo $times['print'][3]; ?>"></td><td><input name="print_one_two" type="number" min="0" step="0.1" value="<?php echo $times['print'][2]; ?>"></td><td><input name="print_two_part" type="number" min="0" step="0.1" value="<?php echo $times['print'][1]; ?>"></td>
        					</tr>
        				</table>
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($_GET['action']); ?> Box Type" class="button-primary" name="submit"></p>
        </form>

    </div>

	<?php
}

function qbcb_process_add_box(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['code'])) $missing_fields .= "Box code, ";
	if(empty($_POST['name'])) $missing_fields .= (empty($missing_fields) ? "" : "and ") .  "Name, ";

	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_box_type";

	// Check for duplicate box code
	$codes = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE {$table}.code = '" . $_POST['code'] . "'" . ($_GET['action'] == 'edit' ? " AND NOT box_type_id = " . $_GET['id'] : ""));
	if($codes > 0){
		echo "<div class='notice notice-warning'><p>There is already a box type with the same code.</p></div>";
		return;
	}


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['code'] = $_POST['code'];
	$data['name'] = $_POST['name'];
	if(!empty($_POST['description'])) $data['description'] = $_POST['description']; else $data['description'] = NULL;
    if(!empty($_POST['board_depth'])) $data['depth'] = $_POST['board_depth']; else $data['depth'] = NULL;
	if(!empty($_POST['box_image'])) $data['image'] = $_POST['box_image']; else $data['image'] = NULL;

	if($_GET['action'] == 'add'){

		$success = $wpdb->insert($table, $data);

	}else{
		
		$wpdb->update($table, $data, array("box_type_id" => $_GET['id']));
		$success = true;

	}

	if($success == false){
		echo "<div class='notice notice-error'><p>Box type could not be added - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	// Add or edit restrictions for box
	if($_GET['action'] == 'add')
		$box_type_id = $wpdb->insert_id;
	else
		$box_type_id = $_GET['id'];

	// Add the dimension restrictions
	qbcb_add_box_type_restriction($box_type_id, 'length', $_POST['length_min'], $_POST['length_max']);
	qbcb_add_box_type_restriction($box_type_id, 'breadth', $_POST['breadth_min'], $_POST['breadth_max']);
	qbcb_add_box_type_restriction($box_type_id, 'height', $_POST['height_min'], $_POST['height_max']);

	// Add the calculations for each dimension
	qbcb_add_box_type_calculation($box_type_id, "length", "box", $_POST['length_calc_box']);
	qbcb_add_box_type_calculation($box_type_id, "length", "lid", $_POST['length_calc_lid']);
	qbcb_add_box_type_calculation($box_type_id, "width", "box", $_POST['width_calc_box']);
	qbcb_add_box_type_calculation($box_type_id, "width", "lid", $_POST['width_calc_lid']);

	// Add the multipliers for each dimension
	qbcb_add_box_type_calculation($box_type_id, "length", "box_multiply", $_POST['length_multiply_box']);
	qbcb_add_box_type_calculation($box_type_id, "length", "lid_multiply", $_POST['length_multiply_lid']);
	qbcb_add_box_type_calculation($box_type_id, "width", "box_multiply", $_POST['width_multiply_box']);
	qbcb_add_box_type_calculation($box_type_id, "width", "lid_multiply", $_POST['width_multiply_lid']);

	// Add the times for the production of the box 
	$wpdb->delete($wpdb->prefix . "qbcb_box_type_times", array("box_type_id" => $box_type_id, "stage" => "setup"));

	$wpdb->insert($wpdb->prefix . "qbcb_box_type_times", array(
		"box_type_id" => $box_type_id,
		"stage" => "setup",
		"qty" => 0,
		"seconds" => $_POST['setup_time']
	));

	qbcb_add_box_type_time($box_type_id, "slit", $_POST['slit_more_two'], $_POST['slit_one_two'], $_POST['slit_two_part']);
	qbcb_add_box_type_time($box_type_id, "make", $_POST['make_more_two'], $_POST['make_one_two'], $_POST['make_two_part']);
	qbcb_add_box_type_time($box_type_id, "finish", $_POST['finish_more_two'], $_POST['finish_one_two'], $_POST['finish_two_part']);
	qbcb_add_box_type_time($box_type_id, "print", $_POST['print_more_two'], $_POST['print_one_two'], $_POST['print_two_part']);

	// Redirect to list box types
	wp_redirect(get_admin_url() . "admin.php?page=costabox-box");

}

function qbcb_add_box_type_restriction( $box_type, $dimension, $min = "", $max = "" ){

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_box_type_restrictions";

	$data = array();
	$data['box_type_id'] = $box_type;
	$data['dimension'] = $dimension;

	if(!empty($min) || $min == 0) $data['min'] = $min;
	if(!empty($max) || $max == 0) $data['max'] = $max;

	// Check to see if a restriction for this box and dimension already exists
	$count = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE box_type_id = {$box_type} AND dimension = '{$dimension}'");

	if($count > 0){
		$wpdb->update($table, $data, array(
			"box_type_id" => $box_type,
			"dimension" => $dimension,
		));
	}else{
		$wpdb->insert($table, $data);
	}

}

function qbcb_add_box_type_calculation( $box_type, $dimension, $component, $formula ){

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_";

	if($dimension == 'length')
		$table .= "length_calculations";
	else
		$table .= "width_calculations";

	$data = array();
	$data['box_type_id'] = $box_type;
	$data['component'] = $component;
	$data['formula'] = $formula;


	// Check to see if a formula already exists for the component of this box
	$count = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE box_type_id = {$box_type} AND component = '{$component}'");

	if($count > 0){

		if(!empty($formula))
			$wpdb->update($table, $data, array(
				"box_type_id" => $box_type,
				"component" => $component,
			));
		else
			$wpdb->delete($table, array(
				"box_type_id" => $box_type,
				"component" => $component,
			));
	}else{
		if(!empty($formula) || $formula == 0) $wpdb->insert($table, $data);
	}

}

function qbcb_add_box_type_time( $box_type_id, $stage, $two_more, $one_two, $two_part ){

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_box_type_times";

	$wpdb->delete($table, array("box_type_id" => $box_type_id, "stage" => $stage));

	$data = array();
	$data['box_type_id'] = $box_type_id;
	$data['stage'] = $stage;
	$data['qty'] = 3;
	$data['seconds'] = $two_more;

	$wpdb->insert($table, $data);

	$data['qty'] = 2;
	$data['seconds'] = $one_two;
	$wpdb->insert($table, $data);

	$data['qty'] = 1;
	$data['seconds'] = $two_part;
	$wpdb->insert($table, $data);

}
