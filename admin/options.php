<?php

/*
* Contains the code to manage the settings for costabox
*/

add_action('admin_menu', 'qbcb_settings_menu');

function qbcb_settings_menu()
{
    add_submenu_page('costabox', 'Settings', 'Settings', 'manage_options', 'costabox-settings', 'qbcb_settings_callback');
}

function qbcb_settings_callback()
{
    if (isset($_GET['import']) && $_GET['import'] == 'contacts') {
        //qbcb_settings_import_contacts();
        return;
    }

    if (isset($_GET['import']) && $_GET['import'] == 'company_groups') {
        qbcb_settings_import_company_groups();

        return;
    }

    if (isset($_GET['sync']) && $_GET['sync'] == 'companies') {
        qbcb_settings_sync_companies();

        return;
    }

    qbcb_process_save_settings();

    extract(qbcb_setting_values()); ?>

<div class="wrap">
    <div id="icon-options-general" class="icon32"><br></div>
    <h1 class="wp-heading-inline">Settings</h1>
    <hr class="wp-header-end" />
    <p>This page contains the global settings for Costabox.</p>
    <form method="POST">
        <table class="form-table">
            <tr>
                <th scope="row">
                    <label for="product-id">Custom Box Product</label>
                </th>
                <td>
                    <?php

                            if (function_exists('wc_get_products')) {
                                echo "<select name='product_id' id='product-id'>";

                                foreach (wc_get_products(['visibility' => 'hidden']) as $product) {
                                    $selected = ($product_id == $product->get_id()) ? 'selected' : '';

                                    echo "<option value='" . $product->get_id() . "' {$selected}>" . $product->get_name() . '</option>';
                                }

                                echo '</select>';
                            } else {
                                echo 'Please install and activate Woocommerce';
                            } ?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="plate_product-id">Plate Product</label>
                </th>
                <td>
                    <?php

                            if (function_exists('wc_get_products')) {
                                echo "<select name='plate_product_id' id='plate_product-id'>";

                                foreach (wc_get_products(['visibility' => 'hidden']) as $product) {
                                    $selected = ($plate_product_id == $product->get_id()) ? 'selected' : '';

                                    echo "<option value='" . $product->get_id() . "' {$selected}>" . $product->get_name() . '</option>';
                                }

                                echo '</select>';
                            } else {
                                echo 'Please install and activate Woocommerce';
                            } ?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="forme_product-id">Forme Product</label>
                </th>
                <td>
                    <?php

                            if (function_exists('wc_get_products')) {
                                echo "<select name='forme_product_id' id='plate_product-id'>";

                                foreach (wc_get_products(['visibility' => 'hidden']) as $product) {
                                    $selected = ($forme_product_id == $product->get_id()) ? 'selected' : '';

                                    echo "<option value='" . $product->get_id() . "' {$selected}>" . $product->get_name() . '</option>';
                                }

                                echo '</select>';
                            } else {
                                echo 'Please install and activate Woocommerce';
                            } ?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="page-id">Custom Box Page</label>
                </th>
                <td>
                    <?php
                            echo "<select name='page_id' id='page-id'>";

    foreach (get_pages() as $page) {
        $selected = ($page_id == $page->ID) ? 'selected' : '';

        echo "<option value='" . $page->ID . "' {$selected}>" . $page->post_title . '</option>';
    }

    echo '</select>'; ?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="markup_1">Markup under 200m<sup>2</sup></label>
                </th>
                <td>
                    <input type="number" name="markup_1"
                        value="<?php echo $markup_1; ?>"
                        style="min-width: 100px;" id="markup_1" min="0" max="1000"> %
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="markup_2">Markup between 200m<sup>2</sup> and 500m<sup>2</sup></label>
                </th>
                <td>
                    <input type="number" name="markup_2"
                        value="<?php echo $markup_2; ?>"
                        style="min-width: 100px;" id="markup_2" min="0" max="1000"> %
                </td>
            </tr>
			 <tr>
                    <th scope="row">
                        <label for="handhole_cost">Handhole Cost</label>
                    </th>
                    <td>
                        £<input type="number" name="handhole_cost" value="<?php echo $handhole_cost; ?>" style="min-width: 100px;" id="handhole_cost" min="0" max="1000" step="0.01">
                    </td>
                </tr>
            <tr>
                <th scope="row">
                    <label for="markup_3">Markup over 500m<sup>2</sup></label>
                </th>
                <td>
                    <input type="number" name="markup_3"
                        value="<?php echo $markup_3; ?>"
                        style="min-width: 100px;" id="markup_3" min="0" max="1000"> %
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="labour_cost">Labour Cost (Per Hour)</label>
                </th>
                <td>
                    £<input type="number" name="labour_cost"
                        value="<?php echo $labour_cost; ?>"
                        style="min-width: 100px;" id="labour_cost" min="0" max="1000" step="0.01">
                </td>
            </tr>
			 <tr>
                    <th scope="row">
                        <label for="labour_cost">Printing Cost (Percentage)</label>
                    </th>
                    <td>
                        <input type="number" name="printing_cost" value="<?php echo $printing_cost; ?>" step="any" style="min-width: 100px;" id="printing_cost"> %
                    </td>
                </tr>
            <tr>
                <th scope="row">
                    <label for="minimum_order_fee">Minimum Order Fee</label>
                </th>
                <td>
                    £<input type="number" name="minimum_order_fee"
                        value="<?php echo $minimum_order_fee; ?>"
                        style="min-width: 100px;" id="minimum_order_fee" min="0" step="0.01"
                        placeholder="Order fee">&nbsp;<input placeholder="Applies to orders under" type="number"
                        name="minimum_order_amount"
                        value="<?php echo $minimum_order_amount; ?>"
                        style="min-width: 100px;" id="minimum_order_amount" min="0" step="0.01">m<sup>2</sup>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="lead_time">Current Lead Time</label>
                </th>
                <td>
                    <input type="number" name="lead_time"
                        value="<?php echo $lead_time; ?>"
                        style="min-width: 100px;" id="lead_time" min="0" max="1000" step="0.01"> working days
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="stock_lead_time">Stock Lead Time</label>
                </th>
                <td>
                    <input type="number" name="stock_lead_time"
                        value="<?php echo $stock_lead_time; ?>"
                        style="min-width: 100px;" id="stock_lead_time" min="0" max="1000" step="0.01"> working days
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="excluded_delivery_dates">Excluded delivery dates</label>
                </th>
                <td>
                    <input type="text" name="excluded_delivery_dates"
                        value="<?php echo $excluded_delivery_dates; ?>"
                        style="min-width: 100px;" id="excluded_delivery_dates" >
                    <p class="description">Enter in the format YYYY-mm-dd, seperated by a comma.</p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="stock_board_chop">Stock Board Chop</label>
                </th>
                <td>
                    <input type="number" name="stock_board_chop"
                        value="<?php echo $stock_board_chop; ?>"
                        style="min-width: 100px;" id="stock_board_chop" min="0" max="100000" step="1"> mm
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="stock_board_deckle">Stock Board Deckle</label>
                </th>
                <td>
                    <input type="number" name="stock_board_deckle"
                        value="<?php echo $stock_board_deckle; ?>"
                        style="min-width: 100px;" id="stock_board_deckle" min="0" max="100000" step="1"> mm
                </td>
            </tr>

        </table>
        <p class="submit"><input type="submit" value="Save Settings" class="button-primary" name="submit"></p>
    </form>

</div>

<?php
}

// Copy relevant user information to the company
function qbcb_settings_sync_companies()
{
    global $wpdb;

    $users = get_users(['fields' => ['ID', 'user_email']]);

    foreach ($users as $user) {
        $company_id = get_user_meta($user->ID, 'wcb2brp_company', true);

        if (empty($company_id)) {
            continue;
        }

        // Determine company domain

        $company_domain = substr($user->user_email, strrpos($user->user_email, '@') + 1);

        // This list isn't exhaustive enough.

        $public_domains = [
            /* Default domains included */
            'aol.com', 'att.net', 'comcast.net', 'facebook.com', 'gmail.com', 'gmx.com', 'googlemail.com',
            'google.com', 'hotmail.com', 'hotmail.co.uk', 'mac.com', 'me.com', 'mail.com', 'msn.com',
            'live.com', 'sbcglobal.net', 'verizon.net', 'yahoo.com', 'yahoo.co.uk',

            /* Other global domains */
            'email.com', 'fastmail.fm', 'games.com' /* AOL */, 'gmx.net', 'hush.com', 'hushmail.com', 'icloud.com',
            'iname.com', 'inbox.com', 'lavabit.com', 'love.com' /* AOL */, 'outlook.com', 'pobox.com', 'protonmail.ch', 'protonmail.com', 'tutanota.de', 'tutanota.com', 'tutamail.com', 'tuta.io',
            'keemail.me', 'rocketmail.com' /* Yahoo */, 'safe-mail.net', 'wow.com' /* AOL */, 'ygm.com' /* AOL */,
            'ymail.com' /* Yahoo */, 'zoho.com', 'yandex.com',

            /* United States ISP domains */
            'bellsouth.net', 'charter.net', 'cox.net', 'earthlink.net', 'juno.com',

            /* British ISP domains */
            'btinternet.com', 'virginmedia.com', 'blueyonder.co.uk', 'freeserve.co.uk', 'live.co.uk',
            'ntlworld.com', 'o2.co.uk', 'orange.net', 'sky.com', 'talktalk.co.uk', 'tiscali.co.uk',
            'virgin.net', 'wanadoo.co.uk', 'bt.com',

            /* Domains used in Asia */
            'sina.com', 'sina.cn', 'qq.com', 'naver.com', 'hanmail.net', 'daum.net', 'nate.com', 'yahoo.co.jp', 'yahoo.co.kr', 'yahoo.co.id', 'yahoo.co.in', 'yahoo.com.sg', 'yahoo.com.ph', '163.com', 'yeah.net', '126.com', '21cn.com', 'aliyun.com', 'foxmail.com',

            /* French ISP domains */
            'hotmail.fr', 'live.fr', 'laposte.net', 'yahoo.fr', 'wanadoo.fr', 'orange.fr', 'gmx.fr', 'sfr.fr', 'neuf.fr', 'free.fr',

            /* German ISP domains */
            'gmx.de', 'hotmail.de', 'live.de', 'online.de', 't-online.de' /* T-Mobile */, 'web.de', 'yahoo.de',

            /* Italian ISP domains */
            'libero.it', 'virgilio.it', 'hotmail.it', 'aol.it', 'tiscali.it', 'alice.it', 'live.it', 'yahoo.it', 'email.it', 'tin.it', 'poste.it', 'teletu.it',

            /* Russian ISP domains */
            'mail.ru', 'rambler.ru', 'yandex.ru', 'ya.ru', 'list.ru',

            /* Belgian ISP domains */
            'hotmail.be', 'live.be', 'skynet.be', 'voo.be', 'tvcablenet.be', 'telenet.be',

            /* Argentinian ISP domains */
            'hotmail.com.ar', 'live.com.ar', 'yahoo.com.ar', 'fibertel.com.ar', 'speedy.com.ar', 'arnet.com.ar',

            /* Domains used in Mexico */
            'yahoo.com.mx', 'live.com.mx', 'hotmail.es', 'hotmail.com.mx', 'prodigy.net.mx',

            /* Domains used in Canada */
            'yahoo.ca', 'hotmail.ca', 'bell.net', 'shaw.ca', 'sympatico.ca', 'rogers.com',

            /* Domains used in Brazil */
            'yahoo.com.br', 'hotmail.com.br', 'outlook.com.br', 'uol.com.br', 'bol.com.br', 'terra.com.br', 'ig.com.br', 'itelefonica.com.br', 'r7.com', 'zipmail.com.br', 'globo.com', 'globomail.com', 'oi.com.br',
        ];

        if (!(in_array( $company_domain, $public_domains ))) {
            update_post_meta($company_id, 'qbcb_domain', $company_domain);
        }

        // Copy billing address

        $addresses = ['shipping', 'billing'];

        $address_fields = ['address_1', 'address_2', 'city', 'state', 'postcode', 'phone'];

        foreach ($addresses as $type) {
            foreach ($address_fields as $field) {
                $value = get_user_meta($user->ID, "{$type}_{$field}", true);

                if (empty($value)) {
                    continue;
                }

                update_post_meta($company_id, "qbcb_{$type}_{$field}", $value);
            }
        }
    }
}

function qbcb_settings_import_company_groups()
{
    global $wpdb;

    $csv_file = plugin_dir_path(__FILE__) . '../company_groups.csv';

    if (!file_exists($csv_file)) {
        echo 'File not found';

        return;
    }

    $csvAsArray = array_map('str_getcsv', file($csv_file));

    for ($r = 1; $r < count($csvAsArray); $r++) {
        print_r($csvAsArray[$r]);

        $user_data = [];

        for ($c = 0; $c < count($csvAsArray[$r]); $c++) {
            $column = $csvAsArray[0][$c];

            $user_data[$column] = $csvAsArray[$r][$c];
        }

        $companyID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_type = 'company' AND post_title = '" . $user_data['Contact'] . "'");

        if (empty($companyID)) {
            continue;
        }

        update_post_meta($companyID, 'qbcb_groups', $user_data['ID']);

        $sql = "SELECT user_id FROM {$wpdb->prefix}usermeta WHERE meta_key = 'wcb2brp_company' AND meta_value = '{$companyID}'";

        $results = $wpdb->get_results($sql, 'ARRAY_A');

        $groups = Groups_Group::get_groups();

        foreach ($results as $res) {
            foreach ($groups as $grp) {
                Groups_User_Group::delete($res['user_id'], $grp->group_id);
            }

            Groups_User_Group::create(['user_id' => $res['user_id'], 'group_id' => $user_data['ID']]);
        }
    }
}

function qbcb_settings_import_contacts()
{
    global $wpdb;

    $csv_file = plugin_dir_path(__FILE__) . '../contacts.csv';

    if (!file_exists($csv_file)) {
        echo 'File not found';

        return;
    }

    $csvAsArray = array_map('str_getcsv', file($csv_file));

    for ($r = 1; $r < count($csvAsArray); $r++) {
        $user_data = [];

        for ($c = 0; $c < count($csvAsArray[$r]); $c++) {
            $column = $csvAsArray[0][$c];

            $user_data[$column] = $csvAsArray[$r][$c];
        }

        // Create company if it doesn't exist
        $companyID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_type = 'company' AND post_title = '" . $user_data['*ContactName'] . "'");

        if (empty($companyID)) {
            $companyID = wp_insert_post([
                'post_title'  => $user_data['*ContactName'],
                'post_type'   => 'company',
                'post_status' => 'publish',
            ]);
        }

        $user = get_user_by('email', $user_data['EmailAddress']);

        if (!$user) {
            $username = $user_data['FirstName'] . '.' . $user_data['LastName'];
            if (strlen($username) < 5) {
                $username = $user_data['*ContactName'];
            }

            // $userId = wp_insert_user(array(
            //     "user_pass" => wp_generate_password(),
            //     "user_login" => $username,
            //     "user_email" => $user_data['EmailAddress'],
            //     "first_name" => $user_data['FirstName'],
            //     "last_name" => $user_data['LastName']
            // ));

            //wp_new_user_notification($userId, 'user');
        } else {
            $userId = $user->id;
        }

        $user_meta = get_userdata($userId);

        if (!is_null($user_meta->roles) && !in_array('administrator', $user_meta->roles)) {
            wp_update_user(['ID' => $userId, 'role' => 'customer']);
            $user = new WP_User($userId);
            $user->add_cap('wcb2brp_view_users');
            $user->add_cap('wcb2brp_add_user');
            $user->add_cap('wcb2brp_edit_user');
            $user->add_cap('wcb2brp_set_rules');
            $user->add_cap('wcb2brp_delete_user');
            echo 'Added';
        }

        //wp_update_user( array ('ID' => 1, 'role' => 'administrator') );

        update_user_meta($userId, 'wcb2brp_company', $companyID);
        update_user_meta($userId, 'wcb2brp_company_user_role', 'company_admin');
        update_user_meta($userId, 'wcb2brp_company_owner', 'on');
        update_user_meta($userId, 'wcb2brp_company_user_status', 'active');

        update_user_meta($userId, 'billing_address_1', $user_data['POAddressLine1']);
        update_user_meta($userId, 'billing_address_2', $user_data['POAddressLine2']);
        update_user_meta($userId, 'billing_city', $user_data['POCity']);
        update_user_meta($userId, 'billing_state', $user_data['PORegion']);
        update_user_meta($userId, 'billing_postcode', $user_data['POPostalCode']);
        update_user_meta($userId, 'billing_phone', $user_data['PhoneNumber']);
        update_user_meta($userId, 'billing_country', 'GB');

        if (!empty($user_data['SAAddressLine1'])) {
            update_user_meta($userId, 'shipping_company', $user_data['SAAttenntionTo']);
            update_user_meta($userId, 'shipping_address_1', $user_data['SAAddressLine1']);
            update_user_meta($userId, 'shipping_address_2', $user_data['SAAddressLine2']);
            update_user_meta($userId, 'shipping_city', $user_data['SACity']);
            update_user_meta($userId, 'shipping_state', $user_data['SARegion']);
            update_user_meta($userId, 'shipping_postcode', $user_data['SAPostalCode']);
            update_user_meta($userId, 'shipping_phone', $user_data['PhoneNumber']);
            update_user_meta($userId, 'shipping_country', 'GB');
        }
    }
}

function qbcb_setting_values()
{
    $values = [];

    $values['product_id']                 = costabox_get_setting('product_id');
    $values['plate_product_id']           = costabox_get_setting('plate_product_id');
    $values['forme_product_id']           = costabox_get_setting('forme_product_id');
    $values['page_id']                    = costabox_get_setting('page_id');
    $values['profit_margin']              = costabox_get_setting('profit_margin');
    $values['markup_1']                   = costabox_get_setting('markup_1');
    $values['markup_2']                   = costabox_get_setting('markup_2');
    $values['markup_3']                   = costabox_get_setting('markup_3');
    $values['labour_cost']                = costabox_get_setting('labour_cost');
    $values['printing_cost']                = costabox_get_setting('printing_cost');
    $values['handhole_cost']                = costabox_get_setting('handhole_cost');
	$values['minimum_order_fee']          = costabox_get_setting('minimum_order_fee');
    $values['minimum_order_amount']       = costabox_get_setting('minimum_order_amount');
    $values['carraige_cost']              = costabox_get_setting('carraige_cost');
    $values['stock_board_chop']           = costabox_get_setting('stock_board_chop');
    $values['stock_board_deckle']         = costabox_get_setting('stock_board_deckle');
    $values['lead_time']                  = costabox_get_setting('lead_time');
    $values['stock_lead_time']            = costabox_get_setting('stock_lead_time');
    $values['excluded_delivery_dates']    = costabox_get_setting('excluded_delivery_dates');

    return $values;
}

function qbcb_process_save_settings()
{
	if(!isset($_POST['submit']))return;
    if ($_POST['submit'] != 'Save Settings') {
        return;
    }

    if(isset($_POST['product_id']))costabox_set_setting('product_id', $_POST['product_id']);
    if(isset($_POST['plate_product_id']))costabox_set_setting('plate_product_id', $_POST['plate_product_id']);
    if(isset($_POST['forme_product_id']))costabox_set_setting('forme_product_id', $_POST['forme_product_id']);
    if(isset($_POST['page_id']))costabox_set_setting('page_id', $_POST['page_id']);
    if(isset($_POST['profit_margin']))costabox_set_setting('profit_margin', $_POST['profit_margin']);
    if(isset($_POST['markup_1']))costabox_set_setting('markup_1', $_POST['markup_1']);
    if(isset($_POST['markup_2']))costabox_set_setting('markup_2', $_POST['markup_2']);
    if(isset($_POST['markup_3']))costabox_set_setting('markup_3', $_POST['markup_3']);
    if(isset($_POST['labour_cost']))costabox_set_setting('labour_cost', $_POST['labour_cost']);
    if(isset($_POST['minimum_order_fee']))costabox_set_setting('minimum_order_fee', $_POST['minimum_order_fee']);
    if(isset($_POST['minimum_order_amount']))costabox_set_setting('minimum_order_amount', $_POST['minimum_order_amount']);
    if(isset($_POST['carraige_cost']))costabox_set_setting('carraige_cost', $_POST['carraige_cost']);
    if(isset($_POST['stock_board_chop']))costabox_set_setting('stock_board_chop', $_POST['stock_board_chop']);
    if(isset($_POST['stock_board_deckle']))costabox_set_setting('stock_board_deckle', $_POST['stock_board_deckle']);
    if(isset($_POST['lead_time']))costabox_set_setting('lead_time', $_POST['lead_time']);
    if(isset($_POST['stock_lead_time']))costabox_set_setting('stock_lead_time', $_POST['stock_lead_time']);
    if(isset($_POST['excluded_delivery_dates']))costabox_set_setting('excluded_delivery_dates', $_POST['excluded_delivery_dates']);
    if(isset($_POST['hubspot_api']))costabox_set_setting('hubspot_api', $_POST['hubspot_api']);
    if(isset($_POST['hubspot_list']))costabox_set_setting('hubspot_list', $_POST['hubspot_list']);
    if(isset($_POST['hubspot_followup']))costabox_set_setting('hubspot_followup', $_POST['hubspot_followup']);
    if(isset($_POST['smartsheet_api']))costabox_set_setting('smartsheet_api', $_POST['smartsheet_api']);
    if(isset($_POST['smartsheet_sheet']))costabox_set_setting('smartsheet_sheet', $_POST['smartsheet_sheet']);

    if(isset($_POST['smartsheet_auto']) && $_POST['smartsheet_auto'] == 'auto')
        costabox_set_setting('smartsheet_auto', $_POST['smartsheet_auto']);
    else
        costabox_set_setting('smartsheet_auto', "");
    
    if(isset($_POST['xero_account_code']))costabox_set_setting('xero_account_code', $_POST['xero_account_code']);
    if(isset($_POST['xero_other_account_code']))costabox_set_setting('xero_other_account_code', $_POST['xero_other_account_code']);
    if(isset($_POST['xero_shipping_account_code']))costabox_set_setting('xero_shipping_account_code', $_POST['xero_shipping_account_code']);
    if(isset($_POST['xero_tax_type']))costabox_set_setting('xero_tax_type', $_POST['xero_tax_type']);
    if(isset($_POST['xero_invoice_status']))costabox_set_setting('xero_invoice_status', $_POST['xero_invoice_status']);

    if(isset($_POST['xero_auto']) && $_POST['xero_auto'] == 'auto')
        costabox_set_setting('xero_auto', $_POST['xero_auto']);
    else
        costabox_set_setting('xero_auto', "");
	
	
	
	// Custom
     if(isset($_POST['printing_cost']))costabox_set_setting('printing_cost', $_POST['printing_cost']);
     if(isset($_POST['handhole_cost']))costabox_set_setting('handhole_cost', $_POST['handhole_cost']);
}
