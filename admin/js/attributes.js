jQuery(function($){
	
	$('body').on('click', '#select_bc', function(){
		$("#box_codes option").prop("selected", true);
	});

	$('body').on('click', '#deselect_bc', function(){
		$("#box_codes option").prop("selected", false);
	});
	
	$('body').on('keyup', '#field_name', function(){
		var slug = $('#field_name').val();

		slug = slug.replace(' ', '_');

		slug = slug.replace(/[^a-zA-Z_]+/g, '');

		$('#slug').val(slug.toLowerCase());
	});
 
});
