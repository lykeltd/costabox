<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_quotations_menu");
 
function qbcb_quotations_menu(){
        add_menu_page( 'Costabox Quotes', 'Costabox', 'manage_options', 'costabox', 'qbcb_quotations_callback' );
        add_submenu_page('costabox', 'Box Specifications', 'Saved Boxes', 'manage_options', 'costabox', 'qbcb_quotations_callback' );
}

add_action('admin_head', 'quotes_list_width');

function quotes_list_width() {

	if(!isset($_GET['page']) || $_GET['page'] != 'costabox') return;

    echo '<style type="text/css">';
    echo '.column-quote_id { width:5% !important; }';
    echo '.column-quoted_price, .column-qty, .column-box_type_id { width:10% !important; }';
    echo '</style>';
}
 
function qbcb_quotations_callback(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h2>Saved Box Specifications</h2></div>

        <div id="post-body-content" class="costabox-box">
			<div class="meta-box-sortables ui-sortable">
				<form method="post">

		        <?php
		        	include_once 'quotes_list.php';
		        	$list = new Quotes_List();
		        	$list->prepare_items();
		        	$list->display();
		        ?>

        		</form>
        	</div>
        </div>

	<?php
}
 