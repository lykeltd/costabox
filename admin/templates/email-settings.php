<div class="wrap">
    <h1>Send a Quote Email Settings</h1>
    <p>Here you can add your options for sending the quote email</p>
    <hr>

    <form action="options.php" method="post">
        <?php settings_fields('costabox_quote_email_options'); ?>
        <?php do_settings_sections('costabox-quote-email-settings'); ?>
        <input name="submit" class="button button-primary" type="submit"
            value="<?php esc_attr_e('Save'); ?>">
    </form>
</div>