<?php

class Costabox_Admin_New_User {

    private bool $is_new = false;

    public function __construct() {
        add_action( 'admin_menu', array($this, 'add_admin_menu') );
        add_action( 'admin_bar_menu', array($this, 'toolbar_items'), 999 );
    }

    public function add_admin_menu(): void {
        add_submenu_page( 'costabox', 'New User', 'New User', 'manage_options', 'qbcb-add-user', array( $this, 'new_user' ) );
    }

    public function toolbar_items($wp_admin_bar): void {
        $wp_admin_bar->add_node( array(
            'id'    => 'qbcb-new-user',
            'title' => 'New Costabox User',
            'href'  => admin_url('admin.php?page=qbcb-add-user'),
        ) );
    }

    private function get_fields(): array{
        return array(
            'first_name' => array(
                'label' => 'First Name',
                'type' => 'text',
                'required' => true,
            ),
            'last_name' => array(
                'label' => 'Last Name',
                'type' => 'text',
                'required' => true,
            ),
            'company' => array(
                'label' => 'Company',
                'type' => 'text',
                'required' => true,
            ),
            'email' => array(
                'label' => 'Email',
                'type' => 'email',
                'required' => true,
            ),
            'phone' => array(
                'label' => 'Phone',
                'type' => 'tel',
                'required' => true,
            ),
            'address_1' => array(
                'label' => 'Address 1',
                'type' => 'text',
                'required' => true,
            ),
            'address_2' => array(
                'label' => 'Address 2',
                'type' => 'text',
                'required' => false,
            ),
            'city' => array(
                'label' => 'City',
                'type' => 'text',
                'required' => true,
            ),
            'state' => array(
                'label' => 'County',
                'type' => 'text',
                'required' => false,
            ),
            'postcode' => array(
                'label' => 'Postcode',
                'type' => 'text',
                'required' => true,
                'type' => 'tel',
                'required' => true,
            ),

        );
    }

    private function get_values(): array{
        $fields = $this->get_fields();
        $values = array();

        foreach($fields as $name => $field){
            $values[$name] = isset($_POST[$name]) && !$this->is_new ? sanitize_text_field($_POST[$name]) : '';
        }

        return $values;
    }

    private function handle_submit(): void {
        if(!isset($_POST['submit']))
            return; // Form not submitted

        if(!check_admin_referer("costabox_add_user"))
            return; // Invalid nonce

        // Create the user
        $form_data = $this->get_values();

        $user_id = wc_create_new_customer($form_data['email'], $form_data['email'], wp_generate_password(), [
            'first_name' => $form_data['first_name'],
            'last_name' => $form_data['last_name'],
        ]);

        if(is_wp_error($user_id)) {
            echo "<div class='notice notice-error'><p>Error creating user: " . $user_id->get_error_message() . "</p></div>";
            return; // Error creating user
        }

        foreach (["first_name", "last_name", "email", "address_1", "address_2", "city", "state", "postcode", "phone", "company"] as $field) {
            update_user_meta($user_id, 'billing_' . $field, $form_data[$field]);
            update_user_meta($user_id, 'shipping_' . $field, $form_data[$field]);
        }

        // Set country to UK
        update_user_meta($user_id, 'billing_country', 'GB');
        update_user_meta($user_id, 'shipping_country', 'GB');

        require_once WP_PLUGIN_DIR . '/costabox/classes/Costabox_New_User.php';
        Costabox_New_User::process($user_id);
        Costabox_New_User::add_telephone_prefix($user_id);

        $loginas_url = wp_nonce_url(add_query_arg(array(
            'action'  => 'login_as_user',
            'user_id' => $user_id,
            'back_url' => home_url(),
        ), wp_login_url()), "login_as_user_{$user_id}");

         echo "<div class='notice notice-success'><p>User created successfully. <a href='{$loginas_url}'>Click here to login as them.</a></p></div>";

            $this->is_new = true;

    }

    public function new_user(): void {

        $this->handle_submit();

        $fields = $this->get_fields();
        $values = $this->get_values();

        ?>
        <div class="wrap">
            <h2>Add a new user</h2>
            <p>Use this form to easily add a new user to the website and automatically link them to the relevant company.</p>
            <form method="POST">

                <?php wp_nonce_field( 'costabox_add_user',  ); ?>

                <table class="form-table">
                    <tbody>
                        <?php

                        foreach($fields as $name => $field){
                            echo '<tr>';
                            echo '<th scope="row"><label for="'.$name.'">'.$field['label']. ($field['required'] ? "*" : "") . '</label></th>';
                            echo '<td><input type="'.$field['type'].'" name="'.$name.'" id="'.$name.'"' . ($field['required'] ? 'required="required"' : '') . ' value="' . $values[$name] .'"></td>';
                            echo '</tr>';
                        }

                        ?>
                    </tbody>
                </table>


                <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response">
                <p class="submit">
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Add User">
                </p>

            </form>
        </div>
        <?php
    }
}

new Costabox_Admin_New_User();