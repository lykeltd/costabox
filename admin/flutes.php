<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_flutes_menu");
 
function qbcb_flutes_menu(){
        add_submenu_page('costabox', 'Flutes', 'Flutes', 'manage_options', 'costabox-flutes', 'qbcb_flutes_callback' );
}

function qbcb_flutes_callback(){

	if(isset($_GET['action']) && ($_GET['action'] == "add" || $_GET['action'] == "edit")){
		qbcb_flutes_add();
		return;
	}

	qbcb_flutes_list();
}

add_action('admin_head', 'flutes_list_width');

function flutes_list_width() {
    echo '<style type="text/css">';
    echo '.costabox-box .column-flute_id { width:5% !important; }';
    echo '</style>';
}
 
function qbcb_flutes_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Flutes</h1><a href="?page=costabox-flutes&action=add" class="page-title-action">Add New</a>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">
			<div class="meta-box-sortables ui-sortable">
				<form method="post">

		        <?php
		        	include_once 'flutes_list.php';
		        	$list = new Flutes_List();
		        	$list->prepare_items();
		        	$list->display();
		        ?>

        		</form>
        	</div>
        </div>

    </div>

	<?php
}

function qbcb_flutes_form_values(){

	$values = array();

	$db = null;
	$size = null;

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_flutes WHERE flute_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

		$size = $wpdb->get_var("SELECT length FROM {$wpdb->prefix}qbcb_board_lengths WHERE flute_id = " . $_GET['id']);

	}

	$values['flute'] = isset($_POST['flute']) ? $_POST['flute'] : (is_null($db) ? "" : $db->flute);
	$values['paper'] = isset($_POST['paper']) ? $_POST['paper'] : (is_null($db) ? "" : $db->paper);
	$values['description'] = isset($_POST['description']) ? $_POST['description'] : (is_null($db) ? "" : $db->description);
	$values['colour'] = isset($_POST['colour']) ? $_POST['colour'] : (is_null($db) ? "" : $db->colour);
	$values['walls'] = isset($_POST['wall']) ? $_POST['wall'] : (is_null($db) ? "" : $db->wall);
	$values['length'] = isset($_POST['length']) ? $_POST['length'] : (is_null($size) ? "" : $size);
	$values['staff_only'] = isset($_POST["staff_only"]) ? ($_POST['staff_only'] == "yes" ? "checked" : "") : (is_null($db) ? "" : ($db->staff_only == 1 ? "checked" : ""));
	$values['image'] = isset($_POST['flute_image']) ? $_POST['flute_image'] : (is_null($db) ? "" : $db->image);
	$values['equivalent_to'] = isset($_POST['equivalent']) ? $_POST['equivalent'] : (is_null($db) ? "" : $db->equivalent);
	$values['moq'] = isset($_POST['moq']) ? $_POST['moq'] : (is_null($db) ? "" : $db->moq);

	return $values;

}

 
function qbcb_flutes_add(){

	qbcb_process_add_flute();

	extract(qbcb_flutes_form_values());

	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline"><?php echo ucfirst($_GET['action']); ?>  Flute</h1>
        <hr class="wp-header-end">

        <form method="POST">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="flute">Flute</label>
        			</th>
        			<td>
						<input type="text" name="flute" id="flute" maxlength="45" value="<?php echo $flute; ?>" >        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="flute-paper">Paper</label>
        			</th>
        			<td>
						<input type="text" name="paper" id="flute-paper" maxlength="45" value="<?php echo $paper; ?>">        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="flute-colour">Colour</label>
        			</th>
        			<td>
						<input type="text" name="colour" id="flute-colour" maxlength="45" value="<?php echo $colour; ?>">        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="flute-wall">Number of Walls</label>
        			</th>
        			<td>
						<select name="wall" id="flute-wall" >
							<option <?php if($walls == 1) echo "selected"; ?>>1</option>
							<option <?php if($walls == 2) echo "selected"; ?>>2</option>
							<option <?php if($walls == 3) echo "selected"; ?>>3</option>
						</select>        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="box-description">Description</label>
        			</th>
        			<td>
						<textarea name="description" id="box-description" placeholder="Describe this flute (Optional)" maxlength="255"><?php echo $description; ?></textarea>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="board-length">Length</label>
        			</th>
        			<td>
						<input type="number" name="length" id="board-length" value="<?php echo $length; ?>">
						<br><span class="description">The size of the board in mm</span>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="board-moq">Minimum Order Quantity</label>
        			</th>
        			<td>
						<input type="number" name="moq" id="board-moq" value="<?php echo $moq; ?>">
						<br><span class="description">The minimum amount of board per box order in m<sup>2</sup></span>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="staff-only">Staff Only?</label>
        			</th>
        			<td>
						<input type="checkbox" name="staff_only" id="staff-only" value="yes" <?php echo $staff_only; ?>>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label>Image</label>
        			</th>
        			<td>
						<?php echo qbcb_image_uploader_field("flute_image", $image); ?>
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for='equivalent'>Equivalent to</label>
        			</th>
        			<td>
						<select id='equivalent' name="equivalent">
							<option value="0">None</option>
							<?php foreach(Costabox_Board::getAllFlutes(false) as $flute_id => $flute){
								if(isset($_GET['id']) && $flute_id == $_GET['id']) continue;

								$selected = $flute_id == $equivalent_to ? "selected" : "";

								echo "<option value='{$flute_id}' {$selected} >{$flute['flute']}</option>";
							} ?>
						</select>
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($_GET['action']); ?> Flute" class="button-primary" name="submit"></p>
        </form>

    </div>

	<?php
}

function qbcb_process_add_flute(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['flute'])) $missing_fields .= "Flute, ";
	if(empty($_POST['paper'])) $missing_fields .= (empty($missing_fields) ? "" : "and ") .  "Paper, ";
	if(empty($_POST['colour'])) $missing_fields .= (empty($missing_fields) ? "" : "and ") .  "Colour, ";
	if(empty($_POST['length'])) $missing_fields .= (empty($missing_fields) ? "" : "and ") .  "Board Size/Length, ";

	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_flutes";

	// Check for duplicate box code
	$codes = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE {$table}.flute = '" . $_POST['flute'] . "'" . ($_GET['action'] == 'edit' ? " AND NOT flute_id = " . $_GET['id'] : ""));
	if($codes > 0){
		echo "<div class='notice notice-warning'><p>This flute is already in the system.</p></div>";
		return;
	}


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['flute'] = $_POST['flute'];
	$data['paper'] = $_POST['paper'];
	$data['colour'] = $_POST['colour'];
	$data['wall'] = $_POST['wall'];
	$data['equivalent'] = $_POST['equivalent'];
	if(!empty($_POST['description'])) $data['description'] = $_POST['description']; else $data['description'] = NULL;
	$data['staff_only'] = ($_POST['staff_only'] == 'yes') ? 1 : 0;
	if(!empty($_POST['flute_image'])) $data['image'] = $_POST['flute_image']; else $data['image'] = NULL;
	if(!empty($_POST['moq'])) $data['moq'] = $_POST['moq']; else $data['moq'] = NULL;

	if($_GET['action'] == 'add'){

		$success = $wpdb->insert($table, $data);

	}else{

		$success = $wpdb->update($table, $data, array("flute_id" => $_GET['id']));

	}

	if($success == false && $_GET['action'] == 'add'){
		echo "<div class='notice notice-error'><p>Flute could not be added/updated - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	// Store the board length in a seperate table
	$table = $wpdb->prefix . "qbcb_board_lengths";
	$data = array();
	$data['length'] = $_POST['length'];

	if($_GET['action'] == 'add'){

		$data['flute_id'] = $wpdb->insert_id;

		$success = $wpdb->insert($table, $data);

	}else{

		$success = $wpdb->update($table, $data, array("flute_id" => $_GET['id']));

	}

	// Redirect to list box types
	wp_redirect(get_admin_url() . "admin.php?page=costabox-flutes");

}

