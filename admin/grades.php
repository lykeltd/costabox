<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_grades_menu");
 
function qbcb_grades_menu(){
        add_submenu_page('costabox', 'Grades', 'Board Grades', 'manage_options', 'costabox-grades', 'qbcb_grades_callback' );
}

function qbcb_grades_callback(){
	qbcb_grades_list();
}

add_action('admin_head', 'grades_list_width');

function grades_list_width() {
    echo '<style type="text/css">';
    echo '.costabox-box .column-flute_id { width:5% !important; }';
    echo '</style>';
}
 
function qbcb_grades_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Board Grades</h1>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">
        	<div id="col-container" class="wp-clearfix">
        		<div id="col-left">
        			<div class="col-wrap">
        				<?php qbcb_grades_add(); ?>
        			</div>
        		</div>
        		<div id="col-right">
        			<div class="col-wrap">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">

					        <?php
					        	include_once 'grades_list.php';
					        	$list = new Grades_List();
					        	$list->prepare_items();
					        	$list->display();
					        ?>

			        		</form>
			        	</div>
			        </div>
			    </div>
			</div>
        </div>

    </div>

	<?php
}

function qbcb_grades_form_values(){

	$values = array();

	$db = null;

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_grades WHERE grade_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

	}

	$values['grade'] = isset($_POST['grade']) ? $_POST['grade'] : (is_null($db) ? "" : $db->grade);
	$values['staff_only'] = isset($_POST["staff_only"]) ? ($_POST['staff_only'] == "yes" ? "checked" : "") : (is_null($db) ? "" : ($db->staff_only == 1 ? "checked" : ""));

	return $values;

}

 
function qbcb_grades_add(){

	qbcb_process_add_grade();

	extract(qbcb_grades_form_values());

	$action = isset($_GET['action']) ? $_GET['action'] : "add";

	?>
        <form method="POST">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="grade">Grade</label>
        			</th>
        			<td>
						<input type="number" name="grade" id="grade" maxlength="45" value="<?php echo $grade; ?>" >        				
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="staff-only">Staff Only?</label>
        			</th>
        			<td>
						<input type="checkbox" name="staff_only" id="staff-only" value="yes" <?php echo $staff_only; ?>>
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($action); ?> Grade" class="button-primary" name="submit"></p>
        </form>

	<?php
}

function qbcb_process_add_grade(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['grade'])) $missing_fields .= "board grade";

	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_grades";

	// Check for duplicate box code
	$codes = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE {$table}.grade = '" . $_POST['grade'] . "'" . ($_GET['action'] == 'edit' ? " AND NOT flute_id = " . $_GET['id'] : ""));
	if($codes > 0){
		echo "<div class='notice notice-warning'><p>The board grade is already in the system.</p></div>";
		return;
	}


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['grade'] = $_POST['grade'];
	$data['staff_only'] = ($_POST['staff_only'] == 'yes') ? 1 : 0;

	if($_GET['action'] != 'edit'){

		$success = $wpdb->insert($table, $data);

	}else{

		$success = $wpdb->update($table, $data, array("grade_id" => $_GET['id']));

	}

	if($success == false){
		echo "<div class='notice notice-error'><p>Board grade could not be added - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	// Redirect to list box types
	wp_redirect(get_admin_url() . "admin.php?page=costabox-grades");

}

