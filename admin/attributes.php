<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_attributes_menu");
 
function qbcb_attributes_menu(){
        add_submenu_page('costabox', 'Attributes', 'Attributes', 'manage_options', 'costabox-attributes', 'qbcb_attributes_callback' );
}

function qbcb_attributes_callback(){
	qbcb_attributes_list();
}

add_action('admin_head', 'attribute_list_width');

function attribute_list_width() {
    echo '<style type="text/css">';
    echo '.costabox-box .column-flute_id { width:5% !important; }';
    echo '</style>';
}

function qbcb_attributes_include() {

	if($_GET['page'] != 'costabox-attributes') return;
 
 	wp_enqueue_script( 'qbcb_image_upload', plugin_dir_url(__FILE__) . '/js/attributes.js', array('jquery'), null, false );
}
 
add_action( 'admin_enqueue_scripts', 'qbcb_attributes_include' );
 
function qbcb_attributes_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Attributes</h1>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">
        	<div id="col-container" class="wp-clearfix">
        		<div id="col-left">
        			<div class="col-wrap">
        				<?php qbcb_attributes_add(); ?>
        			</div>
        		</div>
        		<div id="col-right">
        			<div class="col-wrap">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">

					        <?php
					        	include_once 'attributes_list.php';
					        	$list = new Attributes_List();
					        	$list->prepare_items();
					        	$list->display();
					        ?>

			        		</form>
			        	</div>
			        </div>
			    </div>
			</div>
        </div>

    </div>

	<?php
}

function qbcb_attributes_form_values(){

	$values = array();

	$db = null;
	$codes = null; 

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_attributes WHERE attribute_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

		$sql = "SELECT box_type_id FROM {$wpdb->prefix}qbcb_box_type_attributes WHERE attribute_id = " . $_GET['id'];
		$codes = $wpdb->get_results($sql, "ARRAY_A");

	}

	$values['name'] = isset($_POST['name']) ? $_POST['name'] : (is_null($db) ? "" : $db->name);
	$values['slug'] = isset($_POST['slug']) ? $_POST['slug'] : (is_null($db) ? "" : $db->slug);
	$values['type'] = isset($_POST['type']) ? $_POST['type'] : (is_null($db) ? "" : $db->type);
	$values['box_codes'] = array();

	if(!is_null($codes)){
		foreach($codes as $box_code){
			$values['box_codes'][] = $box_code['box_type_id'];
		}
	}


	return $values;

}

 
function qbcb_attributes_add(){

	qbcb_process_add_attribute();

	extract(qbcb_attributes_form_values());

	$action = isset($_GET['action']) ? $_GET['action'] : "add";

	?>
        <form method="POST">
        	<table class="form-table">
        		<tr>
        			<th scope="row">
        				<label for="field_name">Name</label>
        			</th>
        			<td>
						<input type="text" name="name" id="field_name" placeholder="What information is this attribute collecting?" value="<?php echo $name; ?>">	
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="slug">Slug</label>
        			</th>
        			<td>
        				<input type="text" name="slug" id="slug" placeholder="Used in formulas" value="<?php echo $slug; ?>">			
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="type">Type</label>
        			</th>
        			<td>
						<select name="type" id="type">
							<option value="text" <?php if($type == "text") echo 'selected'; ?>>Text Field</option>
							<option value="number" <?php if($type == "number") echo 'selected'; ?>>Number Field</option>
							<option value="select" <?php if($type == "select") echo 'selected'; ?>>Drop down</option>
						</select>			
        			</td>
        		</tr>
        		<tr>
        			<th scope="row">
        				<label for="box_codes">Used For</label>
        			</th>
        			<td>
        				<a id="select_bc" href="#">Select All</a> / <a href="#" id='deselect_bc'>Deselect All</a><br><br>
						<select name="box_codes[]" id="box_codes" multiple>
							<?php
								foreach(Costabox_Box::getAllTypes() as $id => $opt)
									echo "<option value='{$id}' " . (in_array($id, $box_codes) ? "selected" : "") . ">{$opt}</option>" ;
							?>
						</select>			
        			</td>
        		</tr>
        	</table>
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($action); ?> Attribute" class="button-primary" name="submit"></p>
        </form>

	<?php
}

function qbcb_process_add_attribute(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['name'])) $missing_fields .= "the name, ";
	if(empty($_POST['slug'])) $missing_fields .= "a slug, ";

	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_attributes";

	// Check for attribute with the same slug
	$attributes = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE {$table}.slug = '" . $_POST['slug'] . "'" . ($_GET['action'] == 'edit' ? " AND NOT attribute_id = " . $_GET['id'] : ""));
	if($attributes > 0){
		echo "<div class='notice notice-warning'><p>There is already an attribute with the same slug.</p></div>";
		return;
	}	


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['name'] = $_POST['name'];
	$data['slug'] = $_POST['slug'];
	$data['type'] = $_POST['type'];

	$new_id = null;

	if($_GET['action'] != 'edit'){

		$success = $wpdb->insert($table, $data);
		$new_id = $wpdb->insert_id;

	}else{

		$success = $wpdb->update($table, $data, array("attribute_id" => $_GET['id']));
		$new_id = $_GET['id'];

		$success = true;

	}

	if($success == false){
		echo "<div class='notice notice-error'><p>Attribute could not be added - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	if(!is_null($new_id)){

		$wpdb->delete($wpdb->prefix . "qbcb_box_type_attributes", array("attribute_id" => $new_id));

		foreach ($_POST['box_codes'] as $box_type_id) {
			$wpdb->insert($wpdb->prefix . "qbcb_box_type_attributes", array(
				"attribute_id" => $new_id,
				"box_type_id" => $box_type_id
			));
		}

	}

	// Redirect to list of attributes
	wp_redirect(get_admin_url() . "admin.php?page=costabox-attributes");

}

