<?php

// Creates the page in WP Admin that displays quotations 

add_action('admin_menu', "qbcb_allowances_menu");
 
function qbcb_allowances_menu(){
        add_submenu_page('costabox', 'Allowances', 'Allowances', 'manage_options', 'costabox-allowances', 'qbcb_allowances_callback' );
}

function qbcb_allowances_callback(){
	qbcb_allowances_list();
}

add_action('admin_head', 'allowances_list_width');

function allowances_list_width() {
    echo '<style type="text/css">';
    echo '.costabox-box .column-flute_id { width:5% !important; }';
    echo '</style>';
}
 
function qbcb_allowances_list(){
	?>

	<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
        <h1 class="wp-heading-inline">Allowances</h1>
        <hr class="wp-header-end">
        
        <div id="post-body-content" class="costabox-box">
        	<div id="col-container" class="wp-clearfix">

        				<?php qbcb_allowances_add(); ?>

                <hr>
						<div class="meta-box-sortables ui-sortable">
							<form method="post">

					        <?php
					        	include_once 'allowances_list.php';
					        	$list = new Allowances_List();

					        	$list->prepare_items(isset($_POST['s']) ? $_POST['s'] : null );
					        	$list->search_box( 'Search', 'search_id' ); 
					        	$list->display();
					        ?>

			        		</form>
			        	</div>
			        </div>

        </div>

    </div>

	<?php
}

function qbcb_allowances_form_values(){

	$values = array();

	$db = null;

	if(isset($_GET['id'])){

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}qbcb_allowances WHERE allowance_id = " . $_GET['id'];
		$db = $wpdb->get_row($sql);

	}

	$values['flute'] = isset($_POST['flute']) ? $_POST['flute'] : (is_null($db) ? "" : $db->flute_id);
	$values['component'] = isset($_POST['component']) ? $_POST['component'] : (is_null($db) ? "" : $db->component);
	$values['dimension'] = isset($_POST['dimension']) ? $_POST['dimension'] : (is_null($db) ? "" : $db->dimension);
	$values['box_codes'] = isset($_POST['box_codes']) ? $_POST['box_codes'] : (is_null($db) ? "" : $db->box_type_ids);
	$values['allowance'] = isset($_POST['allowance']) ? $_POST['allowance'] : (is_null($db) ? "" : $db->value);

	return $values;

}

 
function qbcb_allowances_add(){

	qbcb_process_add_allowance();

	extract(qbcb_allowances_form_values());

	$action = isset($_GET['action']) ? $_GET['action'] : "add";

	?>
        <form method="POST">
            <div style="display: inline-block; width: 30%; vertical-align: top">
                <label for="flutes">Flute</label>
                <br><select name="flute" id="flutes" >
                    <?php foreach(Costabox_Board::getAllFlutes(true) as $id => $opt){
                        echo "<option value='{$id}'" . ($id == $flute ? " selected" : "") . ">{$opt['flute']}</option>";
                    } ?>
                </select>
            </div>
            <div style="display: inline-block; width: 30%; vertical-align: top;">
                <label for="allowance">Allowance (Value)</label>
                <br><input type="number" name="allowance" id="allowance" value="<?php echo $allowance; ?>">
            </div>
            <div style="display: inline-block; width: 30%; vertical-align: top">

                <label for="box_codes">Specific Box Types</label>
                <br>
                    <input type="text" name="box_codes" id="box_codes" value="<?php echo $box_codes; ?>"><br>
                    <span class="description">Enter box types the allowance applies to. <br>Blank means all.</span>
            </div>

            <input type="hidden" name="component" value="Box">
            <input type="hidden" name="dimension" value="Length">
        	<p class="submit"><input type="submit" value="<?php echo ucfirst($action); ?> Allowance" class="button-primary" name="submit"></p>
        </form>

	<?php
}

function qbcb_process_add_allowance(){

	if(!isset($_POST['submit'])) return;

	// Check all required information is present

	$missing_fields = "";

	if(empty($_POST['allowance'])) $missing_fields .= "the allowance, ";
 
	if(!empty($missing_fields)){
		echo "<div class='notice notice-warning'><p>Please enter " . rtrim($missing_fields, ", ") . "</p></div>";
		return;
	}

	global $wpdb;
	$table = $wpdb->prefix . "qbcb_allowances";

	// Check for duplicate box code
	$codes = $wpdb->get_var("SELECT COUNT(*) FROM {$table} WHERE {$table}.flute_id = '" . $_POST['flute'] . "' AND component = '" . $_POST['component'] . "' AND dimension = '" . $_POST['dimension'] . "' " . (empty($_POST['box_codes']) ? "" : " AND box_type_ids LIKE '%'" . $_POST['box_codes'] . "'") . ($_GET['action'] == 'edit' ? " AND NOT allowance_id = " . $_GET['id'] : ""));
	echo $wpdb->last_query;
    if($codes > 0){
		echo "<div class='notice notice-warning'><p>The allowance is already in the system. Please edit it.</p></div>";
		return;
	}


	// Add or edit box in database
	$sucess = false;

	$data = array();
	$data['flute_id'] = $_POST['flute'];
	$data['component'] = $_POST['component'];
	$data['dimension'] = $_POST['dimension'];
	$data['box_type_ids'] = empty($_POST['box_codes']) ? null : $_POST['box_codes'];
	$data['value'] = $_POST['allowance'];

	if($_GET['action'] != 'edit'){

		$success = $wpdb->insert($table, $data);

	}else{
        
		$success = $wpdb->update($table, $data, array("allowance_id" => $_GET['id']));

	}

	if($success == false){
        echo $wpdb->last_query;
		echo "<div class='notice notice-error'><p>Allowance could not be added - please try again. If the problem persists, contact Lyke Ltd.</p></div>";
		return;
	}

	// Redirect to list box types
	wp_redirect(get_admin_url() . "admin.php?page=costabox-allowances");

}

