<?php
/**
 * Plugin Name: Costabox
 * Plugin URI: https://quickbox.co
 * Description: Allows quotes to be produced, saved, and purchased (through WooCommerce) for custom boxes
 * Version: 1.1
 * Author: Andy Green (Lyke Ltd.)
 * Author URI: https://lykeltd.com
 */


include_once 'api.php';
include_once 'admin/quotes.php';
include_once 'admin/box_types.php';
include_once 'admin/flutes.php';
include_once 'admin/grades.php';
include_once 'admin/board_prices.php';
include_once 'admin/board_weight.php';
include_once 'admin/allowances.php';
include_once 'admin/options.php';
include_once 'admin/admin.php';

include_once plugin_dir_path(__FILE__) . '/frontend/frontend.php';

include_once plugin_dir_path(__FILE__) . '/helpers.php';
include_once plugin_dir_path(__FILE__) . '/classes/quote-to-pdf.php';
include_once plugin_dir_path(__FILE__) . '/admin/quote-email.php';


include_once plugin_dir_path(__FILE__) . '/classes/Costabox_DB.php';
include_once plugin_dir_path(__FILE__) . '/classes/Costabox_Shortcode_Contact.php';
include_once plugin_dir_path(__FILE__) . '/classes/Costabox_Shortcode_Calculator.php';
require_once plugin_dir_path(__FILE__) . '/classes/Costabox_New_User.php';
require_once plugin_dir_path(__FILE__) . '/classes/Costabox_User.php';
require_once plugin_dir_path(__FILE__) . '/classes/Costabox_Cart_Functions.php';
require_once plugin_dir_path(__FILE__) . '/classes/Costabox_Order_Functions.php';
require_once plugin_dir_path(__FILE__) . '/classes/Costabox_Quotes.php';
require_once plugin_dir_path(__FILE__) . '/classes/Costabox_Companies.php';

class Costabox {

    public function __construct()
    {
        register_activation_hook(__FILE__, ["Costabox_DB", "initialise"]);


        add_action('plugins_loaded', [$this, 'plugins_loaded']);

        // Change From email settings
        add_filter('wp_mail_from', function($email) {
            return "sales@quickbox.co";
        });

        add_filter('wp_mail_from_name', function($name) {
            return "Quickbox.co";
        });

        add_action('init', [$this, 'init']);
        add_action('woocommerce_init', [$this, 'woocommerce_init']);

        add_filter("rest_pre_serve_request", [$this, "rest_pre_serve_request"], 999, 4);

        add_action("wp_head", [$this, "enqueue_mouseflow"]);

        // Disable all comments
        add_action('admin_init', [$this, 'disable_comments_post_types_support']);
        add_filter('comments_open', '__return_false', 20, 2);
        add_filter('pings_open', '__return_false', 20, 2);
        add_filter('comments_array', '__return_empty_array', 10, 2);
        add_action('admin_menu', function () {
            remove_menu_page('edit-comments.php');
        });

        add_filter("woocommerce_shipping_methods", [$this, "add_shipping_method"]);


        // Allow the API to access the WC session
        add_filter('woocommerce_is_rest_api_request', function($is_rest_api_request)
        {
            return false;
        });
    }

    public function plugins_loaded(): void
    {

        if(is_admin()) {
            remove_filter( 'registration_errors', 'advanced_google_recaptcha_process_register_form');
        }
    }

    public function init(): void
    {
        new Costabox_Shortcode_Contact();
        new Costabox_Shortcode_Calculator();
        new Costabox_New_User();
        new Costabox_User();
        new Costabox_Cart_Functions();
        new Costabox_Order_Functions();
        new Costabox_Quotes();
        new Costabox_Companies();

        if(!is_admin()) {
            if (Costabox_User::is_staff(get_current_user_id()) && isset($_GET['supplier_download'])) {
                qbcb_download_supplier_quote($_GET['supplier_download']);
            }

            if (isset($_GET['artwork_download'])) {
                qbcb_download_artwork($_GET['artwork_download']);
            }

            if (isset($_GET['download'])) {
                qbcb_download_quote($_GET['download']);
            }
        }

        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }

        if(is_admin() && function_exists('advanced_google_recaptcha_load_frontend_scripts')){
            add_action('admin_enqueue_scripts', 'advanced_google_recaptcha_load_frontend_scripts');
            remove_action( 'woocommerce_register_post', 'advanced_google_recaptcha_check_woo_register_form', 10, 3 );
        }
    }

    public function woocommerce_init(): void
    {

        remove_action( 'wp_logout', array( WC()->session, 'destroy_session' ) );

        require_once plugin_dir_path(__FILE__) . '/classes/QBCB_Courier_Shipping_Method.php';
    }

    public function rest_pre_serve_request($served, $result, $request, $server)
    {
        $is_image   = false;
        $image_data = null;

        // Check the "Content-Type" header to confirm that we really want to return
        // binary image data.
        foreach ( $result->get_headers() as $header => $value ) {
            if ( 'content-type' === strtolower( $header ) ) {
                $is_image   = str_contains( $value, 'pdf' );
                $image_data = $result->get_data();
                break;
            }
        }

        // Output the binary data and tell the REST server to not send any other
        // details (via "return true").
        if ( $is_image && is_string( $image_data ) ) {
            echo $image_data;

            return true;
        }

        return $served;
    }

    public function disable_comments_post_types_support(): void
    {

        // Redirect any user trying to access comments page
        global $pagenow;

        if ($pagenow === 'edit-comments.php') {
            wp_safe_redirect(admin_url());
            exit;
        }

        // Remove comments metabox from dashboard
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

        $post_types = get_post_types();
        foreach ($post_types as $post_type) {
            if (post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }

    public function enqueue_mouseflow(): void
    {
        $user = Costabox_User::get_user();
        if($user && Costabox_User::is_staff($user->ID)) {
            return;
        }

        ?>

        <script type="text/javascript">
            window._mfq = window._mfq || [];
            (function() {
                var mf = document.createElement("script"); mf.type = "text/javascript"; mf.defer = true;
                mf.src = "//cdn.mouseflow.com/projects/4d20fb04-8b87-44ff-903d-033f29e9811a.js";
                document.getElementsByTagName("head")[0].appendChild(mf);
            })();
        </script>

        <?php

        remove_action( 'woocommerce_available_payment_gateways', array( "WCB2BRP_Company", 'unset_credit_payment_gateway' ) );
    }

    public function add_shipping_method($methods)
    {
        $methods['qbcb_courier'] = 'QBCB_Courier_Shipping_Method';
        return $methods;
    }

}

new Costabox();


function costabox_get_box_code($box_type_id, $include_name = false)
{
    if (!is_numeric($box_type_id) || str_starts_with($box_type_id, '0')) {
        return $box_type_id;
    }

    global $wpdb;

    $table = $wpdb->prefix . 'qbcb_box_type';

    $sql      = 'SELECT code' . ($include_name ? ', name' : '') . " FROM {$table} WHERE box_type_id = {$box_type_id}";
    $box_code = $wpdb->get_row($sql, 'ARRAY_A');

    return is_null($box_code) ? 'Unknown' : ($box_code['code'] . ($include_name ? ' - ' . $box_code['name'] : ''));
}


function costabox_get_setting( string $key): ?string
{
    global $wpdb;
    $table = $wpdb->prefix . 'qbcb_options';

    $sql = "SELECT meta_value FROM {$table} WHERE meta_key = '{$key}' ORDER BY option_id DESC";

    return $wpdb->get_var($sql);
}

function costabox_set_setting($key, $value): void
{
    $update = !is_null(costabox_get_setting($key));

    global $wpdb;
    $table = $wpdb->prefix . 'qbcb_options';

    if ($update) {
        $wpdb->update($table, ['meta_value' => $value], ['meta_key' => $key]);
    } else {
        $wpdb->insert($table, ['meta_value' => $value, 'meta_key' => $key]);
    }
}

function costabox_get_profit_margin($area, $user_id = null, $stock = false): ?float
{
    $margin = null;

    // Workout which markup level to use
    $margin_level = 'markup_1';

    if ($area >= 200 && $area <= 500) {
        $margin_level = 'markup_2';
    }

    if ($area > 500) {
        $margin_level = 'markup_3';
    }

//    var_dump($user_id); die();

    if ($user_id > 0) {
        // Get best margin from groups user is in
        if (is_null($margin) && function_exists('wcb2brp_is_b2b_user') && wcb2brp_is_b2b_user($user_id)) {
            // Get margin for company if it isn't set at user level
            $company_id = get_user_meta($user_id, 'wcb2brp_company', true);


            $company_margin     = get_post_meta($company_id, '_qbcb_' . $margin_level, true);

            if (!empty($company_margin)) {
                $margin = $company_margin;
            }else {
                $company_groups = get_post_meta($company_id, 'qbcb_groups', true);
                $company_groups = explode(',', $company_groups);

                foreach ($company_groups as $grp) {
                    $grp_margin = costabox_get_setting("{$grp}_{$margin_level}");

                    if (empty($grp_margin)) {
                        continue;
                    }

                    if (is_null($margin) || $grp_margin < $margin) {
                        $margin = $grp_margin;
                    }
                }


                if (empty($margin)) {
                    $margin = null;
                }
            }

//            var_dump($margin); die;
        }

        if(is_null($margin) && class_exists('Groups_User')){

            $groups_user = new Groups_User($user_id);

            $user_groups = $groups_user->groups;

            $best_group = null;

            if (is_array($user_groups)) {
                foreach ($user_groups as $grp) {
                    $grp_margin = costabox_get_setting("{$grp->group_id}_{$margin_level}");

                    if (empty($grp_margin)) {
                        continue;
                    }

                    if (is_null($best_group) || $grp_margin < $best_group) {
                        $best_group = $grp_margin;
                    }
                }
            }

            $margin = $best_group;

            if (is_null($margin)) {
                $margin = get_user_meta($user_id, $margin_level, true);
                if (empty($margin)) {
                    $margin = null;
                }
            }
    }

    }

    if (is_null($margin)) {
        $margin = costabox_get_setting($margin_level);
    }

    if ($stock) {
        $margin = costabox_get_setting("markup_1");
    }

    return 1 + (strval($margin) / 100) + (!empty($user_id) ? 0 : 0.05);
}

function qbcb_image_uploader_field($name, $value = ''): string
{
    $image      = ' button">Upload image';
    $image_size = 'thumbnail'; // it would be better to use thumbnail size here (150x150 or so)
  $display      = 'none'; // display state ot the "Remove image" button

  if ($image_attributes = wp_get_attachment_image_src($value, $image_size)) {
      // $image_attributes[0] - image URL
      // $image_attributes[1] - image width
      // $image_attributes[2] - image height

      $image   = '"><img src="' . $image_attributes[0] . '" style="max-width:95%;display:block;" />';
      $display = 'inline-block';
  }

    return '
  <div>
    <a href="#" class="qbcb_upload_image_button' . $image . '</a>
    <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . esc_attr($value) . '" />
    <a href="#" class="qbcb_remove_image_button" style="display:inline-block;display:' . $display . '">Remove image</a>
  </div>';
}

// Function to determine whether somebody has an Account Number in Xero
function costabox_can_pay_on_account(string $email_address): bool
{

    $user = get_user_by('email', $email_address);

    if(!$user) $user = get_user_by('login', $email_address);

    if(!$user) return false;

        // Get margin for company if it isn't set at user level
    $company_id = get_user_meta($user->ID, 'wcb2brp_company', true);
    
    if(!empty($company_id)){

        $account_number = get_post_meta($company_id, "qbcb_account_number", true);

        return !empty($account_number);

    }

    return  false;

}



function qbcb_assign_user_company($user_id, $company)
{
    // Assign user to the company
    $user = new WP_User($user_id);
    $user->add_cap('wcb2brp_view_users');

    update_user_meta($user_id, 'wcb2brp_company', $company);
    update_user_meta($user_id, 'wcb2brp_company_user_role', 'company_user_role');
    update_user_meta($user_id, 'wcb2brp_company_user_status', 'active');

    // die();
}

function qbcb_supplier_quotes_upload_path()
{
    $root_path = substr(ABSPATH, 0, strrpos(ABSPATH, DIRECTORY_SEPARATOR, -2));

    return $root_path . DIRECTORY_SEPARATOR . 'qbcb_supplier_quotes';
}

function qbcb_artwork_upload_path()
{
    if(str_starts_with(ABSPATH, '/var/www/wordpress'))
        $root_path = substr(ABSPATH, 0, -1);
    else
        $root_path = substr(ABSPATH, 0, strrpos(ABSPATH, DIRECTORY_SEPARATOR, -2));


    return $root_path . DIRECTORY_SEPARATOR . 'qbcb_artwork';
}


function qbcb_download_supplier_quote($bought_in_id)
{
    global $wpdb;

    $sql = "SELECT file FROM {$wpdb->prefix}qbcb_purchased_box WHERE purchased_box_id = {$bought_in_id}";

    $details = $wpdb->get_row($sql);

    if (empty($details)) {
        echo 'No file found';

        return;
    }

    $file_name = $details->file;
    $file_name_parts = explode($file_name, '.');
    $ext       = end($file_name_parts);

    $content_type = $ext == 'text' ? 'text/plain' : "application/{$ext}";

    $file = qbcb_supplier_quotes_upload_path() . DIRECTORY_SEPARATOR . $file_name;

    header('Content-Disposition: attachment; filename=' . substr($file_name, 10) . '');
    header('Content-Length: ' . filesize($file));
    header("Content-Type: {$content_type};");
    readfile($file);

    exit;
}



function qbcb_download_artwork($artwork_id)
{
    global $wpdb;

    $sql = "SELECT file FROM {$wpdb->prefix}qbcb_artwork WHERE artwork_id = {$artwork_id}";

    $details = $wpdb->get_row($sql);

    if (empty($details)) {
        echo 'No file found';

        return;
    }

    $file_name = $details->file;
    $file_name_parts = explode($file_name, '.');
    $ext       = end($file_name_parts);

    $content_type = $ext == 'text' ? 'text/plain' : "application/{$ext}";

    $file = qbcb_artwork_upload_path() . DIRECTORY_SEPARATOR . $file_name;

    header('Content-Disposition: attachment; filename=' . substr($file_name, 10) . '');
    header('Content-Length: ' . filesize($file));
    header("Content-Type: {$content_type};");
    readfile($file);

    exit;
}

function qbcb_download_quote($cart_id)
{
    $uploads_dir         = wp_upload_dir();
    $quotesFolder = $uploads_dir['basedir'] . '/costabox/';

    $file = $quotesFolder . DIRECTORY_SEPARATOR . $cart_id . '.pdf';

    header('Content-Disposition: attachment; filename=' . $cart_id . '.pdf');
    header('Content-Length: ' . filesize($file));
    header("Content-Type: application/pdf;");
    readfile($file);

    exit;
}


